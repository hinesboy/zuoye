//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 Tetris.rc 使用
//
#define IDC_MYICON                      2
#define IDD_TETRIS_DIALOG               102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_TETRIS                      107
#define IDI_SMALL                       108
#define IDC_TETRIS                      109
#define IDR_MAINFRAME                   128
#define IDM_DIFF                        129
#define ID_32778                        32778
#define ID_32779                        32779
#define ID_LAYOUT1                      32780
#define ID_LAYOUT2                      32781
#define ID_32785                        32785
#define ID_32786                        32786
#define ID_32787                        32787
#define ID_dif1                         32788
#define ID_dif2                         32789
#define ID_dif3                         32790
#define ID_Menu                         32791
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32792
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           111
#endif
#endif
