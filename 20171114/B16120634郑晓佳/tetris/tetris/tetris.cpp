// Tetris.cpp : 定义应用程序的入口点。
//

/*
记录于2016-5-29 星期日    由于闪屏过于显眼，采用双缓冲

*/

#include "stdafx.h"
#include "Tetris.h"
#include "windows.h"//定义了Windows的所有资料型态、函数调用、资料结构和常数识别字
#include <mmsystem.h>//包含windows中与多媒体有关的大多数接口
#pragma comment(lib, "WINMM.LIB")//链接一些WINMM.LIB库，支持对windows的变成
#define MAX_LOADSTRING 100 //定义字符串最大长度

// 此代码模块中包含的函数的前向声明:
ATOM                MyRegisterClass(HINSTANCE hInstance);   //注册窗口类
BOOL                InitInstance(HINSTANCE, int); //保存实例句柄并创建主窗口
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);//处理主窗口的消息。
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM); // “关于”框的消息处理程序。

// 全局变量:
HINSTANCE hInst;                                // 当前实例
TCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本
TCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名

int APIENTRY _tWinMain(HINSTANCE hInstance,  //当前实例句柄 
	HINSTANCE hPrevInstance, //其他实例句柄
	LPTSTR    lpCmdLine,  //指向程序命令行参数的指针
	int       nCmdShow) //应用程序开始执行时窗口显示方式的整数值标识 
{
	init_game();  //初始化游戏
	UNREFERENCED_PARAMETER(hPrevInstance);  //避免编译器关于未引用参数的警告
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: 在此放置代码。
	MSG msg;  //定义消息类
	HACCEL hAccelTable;  //加速键表句柄

	// 初始化全局字符串
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING); //从资源加载字符串资源
	LoadString(hInstance, IDC_TETRIS, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);//注册该窗口应用程序

	// 执行应用程序初始化:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;//初始化失败
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TETRIS)); //调入指定的加速键表。

	// 主消息循环:
	while (1)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))//检查线程消息队列，并将msg（如果存在返回非0）放于指定的结构
		{
			TranslateMessage(&msg);//将虚拟键消息转化为字符消息
			DispatchMessage(&msg);//在窗体的过程函数处理消息
			if (msg.message == WM_QUIT)//WM_OUIT：关系消息
			{
				break;//退出游戏
			}
		}
		else
		{
			if ((GAME_STATE & 2) != 0)
			{
				tCur = GetTickCount(); //返回（retrieve）从操作系统启动所经过（elapsed）的毫秒数，它的返回值是DWORD。
				if (tCur - tPre>g_speed)//启动时间超过g_speed
				{

					int flag = CheckValide(curPosX, curPosY + 1, bCurTeris);//检查有效性
					if (flag == 1)
					{
						curPosY++;
						tPre = tCur;//重新标定起始时间
						HWND hWnd = GetActiveWindow();//获得与调用线程的消息队列相关的活动窗口的窗口句柄
						InvalidateRect(hWnd, &rc_left, FALSE);//向指定的窗体更新区域添加一个矩形，然后窗口客户区域的这一部分将被重新绘制
						InvalidateRect(hWnd, &rc_right_top, FALSE);
					}
					else if (flag == -2)//下一行无法下降
					{
						g_speed = t_speed;//更新速度
						fillBlock();//到底最底部填充矩阵
						checkComplite();//查找可消除行
						setRandomT();//随机获取一个值
						curPosX = (NUM_X - 4) >> 1;//更新当前坐标
						curPosY = 0;
						HWND hWnd = GetActiveWindow();
						InvalidateRect(hWnd, &rc_main, FALSE);
					}
					else if (flag == -3)//无下一行
					{
						HWND hWnd = GetActiveWindow();
						if (MessageBox(hWnd, L"胜败乃兵家常事，菜鸡请重新来过", L":时光机", MB_YESNO) == IDYES)
						{
							init_game();//重新初始化游戏
						}
						else
						{
							break;//退出游戏
						}
					}
				}
			}
		}
	}

	return (int)msg.wParam;
}


//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
//  注释:
//
//    仅当希望
//    此代码与添加到 Windows 95 中的“RegisterClassEx”
//    函数之前的 Win32 系统兼容时，才需要此函数及其用法。调用此函数十分重要，
//    这样应用程序就可以获得关联的
//    “格式正确的”小图标。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

		wcex.cbSize = sizeof(WNDCLASSEX);//结构体WNDCLASSEX大小

	wcex.style = CS_HREDRAW | CS_VREDRAW;//定义窗口样式
	wcex.style = CS_HREDRAW | CS_VREDRAW;//定义窗体样式
	wcex.lpfnWndProc = WndProc;//回调函数
	wcex.cbClsExtra = 0;//窗口类额外字节数，通常为0
	wcex.cbWndExtra = 0;//窗口实例额外字节数，通常为0
	wcex.hInstance = hInstance;//进程句柄
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TETRIS));//图标
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);//鼠标样式
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);//背景
	wcex.lpszMenuName = MAKEINTRESOURCE(IDC_TETRIS);//菜单指针
	wcex.lpszClassName = szWindowClass;//窗体类名
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));//窗体小图标

	return RegisterClassEx(&wcex);
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释:
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // 将实例句柄存储在全局变量中

	hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);//创建窗口
	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);//设置窗口显示状态
	UpdateWindow(hWnd);//更新制定窗口的客户区

	return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的: 处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;//绘制它所拥有的窗口客户区所需要的信息的结构体
	HDC hdc;//标示设备环境句柄
	int nWinx, nWiny, nClientX, nClientY;
	int posX, posY;
	RECT rect;//存储成对出现的参数
	HMENU hSysmenu;//菜单的句柄或是子窗口的标识符
	switch (message)
	{
	case WM_CREATE://CreateWindow函数请求创建窗口时发送此消息


		GetWindowRect(hWnd, &rect);//获得指定窗口的边框矩形的尺寸
		nWinx = 530;//窗口宽度
		nWiny = 680;//窗口高度
		posX = GetSystemMetrics(SM_CXSCREEN);//获取屏幕宽度
		posY = GetSystemMetrics(SM_CYSCREEN);//获取屏幕高度
		posX = (posX - nWinx) >> 1;//居中时X位置
		posY = (posY - nWiny) >> 1;//居中时Y位置
		GetClientRect(hWnd, &rect);//获取窗口客户区尺寸
		nClientX = rect.right - rect.left;//窗口客户区X值
		nClientY = rect.bottom - rect.top;//窗口客户区Y值

		MoveWindow(hWnd, posX, posY, 530, 680, TRUE);//设置窗口位置和大小
		hSysmenu = GetSystemMenu(hWnd, false);//允许应用程序为复制或修改而访问窗口菜单
		AppendMenu(hSysmenu, MF_SEPARATOR, 0, NULL);//在hSysmenu后追加一个新菜单项
													//AppendMenu(hSysmenu, 0, IDM_DIFF, L"难度选择");此处缺少资源ID，故被注释
		selectDiffculty(hWnd, 1);//新增行，直接设定难度值
		selectLayOut(hWnd, 2);
		break;
	case WM_COMMAND://用户点击菜单、按钮、下拉列表框等控件发送此消息
		wmId = LOWORD(wParam);//取wParam的低位
		wmEvent = HIWORD(wParam);//取wParam的高位
								 // 分析菜单选择:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);////弹出“关于”消息框
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);//退出
			break;
			/*case ID_dif1:
			selectDiffculty(hWnd, 1);
			break;
			case ID_dif2:
			selectDiffculty(hWnd, 2);
			break;
			case ID_dif3:
			selectDiffculty(hWnd, 3);
			break;
			case ID_LAYOUT1:
			selectLayOut(hWnd, 1);
			break;
			case ID_LAYOUT2:
			selectLayOut(hWnd, 2);
			break;*/  //无参数  注释
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_KEYDOWN://当一个非系统键被按下时该消息发送给具有键盘焦点的窗口 
		hdc = GetDC(hWnd);//检索一指定窗口的客户区域或整个屏幕的显示设备上下文环境的句柄
		InvalidateRect(hWnd, NULL, false);
		switch (wParam)
		{
		case VK_LEFT://键盘方向键
			curPosX--;//当前X值减一
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)
			{
				curPosX++;//若已超过左边界，X值+1
			}
			break;
		case VK_RIGHT:
			curPosX++;
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)
			{
				curPosX--;
			}
			break;
		case VK_UP:
			RotateTeris(bCurTeris);//旋转矩阵
			break;
		case VK_DOWN://加速下降
			if (g_speed == t_speed)
				g_speed = 10;
			else
				g_speed = t_speed;
			//outPutBoxInt(g_speed);
			break;
		case 'W'://旋转
			RotateTeris(bCurTeris);
			break;
		case 'A'://左移动
			curPosX--;
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)
			{
				curPosX++;
			}
			break;
		case 'D'://右移动
			curPosX++;
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)
			{
				curPosX--;
			}
			break;

		case 'S'://快速下降
			if (g_speed == t_speed)
				g_speed = 10;
			else
				g_speed = t_speed;
			//outPutBoxInt(g_speed);
			break;
		default:
			break;
		}

	case WM_PAINT://当窗口显示区域的一部分显示内容或者全部变为“无效”，以致于必须“更新画面”时，将由这个消息通知程序
		hdc = BeginPaint(hWnd, &ps);//为指定窗口进行绘图工作的准备
									// TODO: 在此添加任意绘图代码...
		DrawBackGround(hdc);//绘制背景
		EndPaint(hWnd, &ps);//指定窗口进行绘图的结束
		break;
	case WM_DESTROY://窗口销毁后(调用DestroyWindow()后)，消息队列得到的消息
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG://对话框和所有控件创建完毕
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


void drawBlocked(HDC mdc)//绘制砖块

{
	int i, j;

	HBRUSH hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 255, 0));//画刷句柄

	SelectObject(mdc, hBrush);//用新对象代替先前对象

	for (i = 0; i<NUM_Y; i++)
	{
		for (j = 0; j<NUM_X; j++)
		{
			if (g_hasBlocked[i][j])
			{
				Rectangle(mdc, BORDER_X + j*BLOCK_SIZE, BORDER_Y + i*BLOCK_SIZE,
					BORDER_X + (j + 1)*BLOCK_SIZE, BORDER_Y + (i + 1)*BLOCK_SIZE
				);//绘制砖块
			}
		}
	}
	DeleteObject(hBrush);//删除对象
}

int CheckValide(int startX, int startY, BOOL bTemp[4][4])//查看砖块的情况
{
	int i, j;
	for (i = 3; i >= 0; i--)
	{
		for (j = 3; j >= 0; j--)
		{
			if (bTemp[i][j])
			{
				if (j + startX<0 || j + startX >= NUM_X)
				{
					return -1;
				}
				if (i + startY >= NUM_Y)
				{
					return -2;
				}
				if (g_hasBlocked[i + startY][j + startX])
				{
					//outPutBoxInt(j+startY);
					if (curPosY == 0)
					{
						return -3;
					}
					return -2;
				}
			}
		}
	}
	//MessageBox(NULL,L"这里",L"as",MB_OK);
	//outPutBoxInt(curPosY);
	return 1;
}

void checkComplite()//查看下一行是否能删除
{
	int i, j, k, count = 0;
	for (i = 0; i<NUM_Y; i++)
	{
		bool flag = true;
		for (j = 0; j<NUM_X; j++)
		{
			if (!g_hasBlocked[i][j])
			{
				flag = false;
			}
		}
		if (flag)
		{
			count++;
			for (j = i; j >= 1; j--)
			{
				for (k = 0; k<NUM_X; k++)
				{
					g_hasBlocked[j][k] = g_hasBlocked[j - 1][k];
				}

			}
			drawCompleteParticle(i);
			Sleep(300);

			PlaySound(_T("coin.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}
	}
	GAME_SCORE += int(count*1.5);
}



VOID outPutBoxInt(int num)
{
	TCHAR szBuf[1024];
	LPCTSTR str = TEXT("%d");
	wsprintf(szBuf, str, num);
	MessageBox(NULL, szBuf, L"aasa", MB_OK);
}

VOID outPutBoxString(TCHAR str[1024])
{
	TCHAR szBuf[1024];
	LPCTSTR cstr = TEXT("%s");
	wsprintf(szBuf, cstr, str);
	MessageBox(NULL, szBuf, L"aasa", MB_OK);
}


void setRandomT()//随机生成方块降落
{
	int rd_start = RandomInt(0, sizeof(state_teris) / sizeof(state_teris[0]));
	int rd_next = RandomInt(0, sizeof(state_teris) / sizeof(state_teris[0]));
	//outPutBoxInt(rd_start);
	//outPutBoxInt(rd_next);
	//outPutBoxInt(rd_start);
	if (GAME_STATE == 0)//游戏刚开始
	{
		GAME_STATE = GAME_STATE | 0x0001;//标记程序已经运行
		//outPutBoxInt(GAME_STATE);
		memcpy(bCurTeris, state_teris[rd_start], sizeof(state_teris[rd_start]));
		memcpy(bNextCurTeris, state_teris[rd_next], sizeof(state_teris[rd_next]));
	}
	else//程序已经有下落砖块
	{
		memcpy(bCurTeris, bNextCurTeris, sizeof(bNextCurTeris));//令当前下落方块=下一下落方块
		memcpy(bNextCurTeris, state_teris[rd_next], sizeof(state_teris[rd_next]));//记录下一个落方块
	}

}
void init_game()//初始化游戏
{
	GAME_SCORE = 0;
	setRandomT();
	curPosX = (NUM_X - 4) >> 1;
	curPosY = 0;


	memset(g_hasBlocked, 0, sizeof(g_hasBlocked));
	rc_left.left = 0;
	rc_left.right = SCREEN_LEFT_X;
	rc_left.top = 0;
	rc_left.bottom = SCREEN_Y;

	rc_right.left = rc_left.right + BORDER_X;
	rc_right.right = 180 + rc_right.left;
	rc_right.top = 0;
	rc_right.bottom = SCREEN_Y;

	rc_main.left = 0;
	rc_main.right = SCREEN_X;
	rc_main.top = 0;
	rc_main.bottom = SCREEN_Y;

	rc_right_top.left = rc_right.left;
	rc_right_top.top = rc_right.top;
	rc_right_top.right = rc_right.right;
	rc_right_top.bottom = (rc_right.bottom) / 2;

	rc_right_bottom.left = rc_right.left;
	rc_right_bottom.top = rc_right_top.bottom + BORDER_Y;
	rc_right_bottom.right = rc_right.right;
	rc_right_bottom.bottom = rc_right.bottom;

	g_speed = t_speed = 1000 - GAME_DIFF * 280;//下落速度
}

void fillBlock()//到达最底部填充矩阵
{
	int i, j;
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bCurTeris[i][j])
			{
				g_hasBlocked[curPosY + i][curPosX + j] = 1;
			}
		}
	}
}

void RotateTeris(BOOL bTeris[4][4])//旋转矩阵
{
	BOOL bNewTeris[4][4];
	int x, y;
	for (x = 0; x<4; x++)
	{
		for (y = 0; y<4; y++)
		{
			bNewTeris[x][y] = bTeris[3 - y][x];
			//逆时针：
			//bNewTeris[x][y] = bTeris[y][3-x];
		}
	}
	if (CheckValide(curPosX, curPosY, bNewTeris) == 1)
	{
		memcpy(bTeris, bNewTeris, sizeof(bNewTeris));//更新形状
	}

}

int RandomInt(int _min, int _max)//随机生成一个数
{
	srand((rd_seed++) % 65532 + GetTickCount());
	return _min + rand() % (_max - _min);
}

VOID DrawTeris(HDC mdc)//绘制背景
{

	int i, j;
	HPEN hPen = (HPEN)GetStockObject(BLACK_PEN);
	HBRUSH hBrush = (HBRUSH)GetStockObject(WHITE_BRUSH);
	SelectObject(mdc, hPen);
	SelectObject(mdc, hBrush);
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bCurTeris[i][j])
			{
				Rectangle(mdc, (j + curPosX)*BLOCK_SIZE + BORDER_X, (i + curPosY)*BLOCK_SIZE + BORDER_Y, (j + 1 + curPosX)*BLOCK_SIZE + BORDER_X, (i + 1 + curPosY)*BLOCK_SIZE + BORDER_Y);
			}
		}
	}
	drawBlocked(mdc);
	DeleteObject(hPen);
	DeleteObject(hBrush);
}

VOID DrawBackGround(HDC hdc)
{

	HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);
	HDC mdc = CreateCompatibleDC(hdc);
	HBITMAP hBitmap = CreateCompatibleBitmap(hdc, SCREEN_X, SCREEN_Y);

	SelectObject(mdc, hBrush);
	SelectObject(mdc, hBitmap);

	HBRUSH hBrush2 = (HBRUSH)GetStockObject(WHITE_BRUSH);
	FillRect(mdc, &rc_main, hBrush2);//填充方块颜色
	Rectangle(mdc, rc_left.left + BORDER_X, rc_left.top + BORDER_Y, rc_left.right, rc_left.bottom);
	Rectangle(mdc, rc_right.left + BORDER_X, rc_right.top + BORDER_Y, rc_right.right, rc_right.bottom);
	DrawTeris(mdc);//绘制正在下落的方块
	drawNext(mdc);//绘制下一个下落的方块
	drawScore(mdc);//绘制得分
	::BitBlt(hdc, 0, 0, SCREEN_X, SCREEN_Y, mdc, 0, 0, SRCCOPY);
	DeleteObject(hBrush);
	DeleteDC(mdc);
	DeleteObject(hBitmap);
	DeleteObject(hBrush2);


	//  int x,y;
	//  HPEN hPen = (HPEN)GetStockObject(NULL_PEN);
	//  HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);
	//  SelectObject(hdc,hPen);
	//  SelectObject(hdc,hBrush);
	//  for (x = 0;x<NUM_X;x++)
	//  {
	//      for(y=0;y<NUM_Y;y++)
	//      {
	//          Rectangle(hdc,BORDER_X+x*BLOCK_SIZE,BORDER_Y+y*BLOCK_SIZE,
	//              BORDER_X+(x+1)*BLOCK_SIZE,
	//              BORDER_Y+(y+1)*BLOCK_SIZE);
	//      }
	//  }
}

void drawNext(HDC hdc)//绘制下一个的砖块
{
	int i, j;
	HBRUSH hBrush = (HBRUSH)CreateSolidBrush(RGB(0, 188, 0));
	SelectObject(hdc, hBrush);
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bNextCurTeris[i][j])
			{
				Rectangle(hdc, rc_right_top.left + BLOCK_SIZE*(j + 1), rc_right_top.top + BLOCK_SIZE*(i + 1), rc_right_top.left + BLOCK_SIZE*(j + 2), rc_right_top.top + BLOCK_SIZE*(i + 2));
			}
		}
	}
	HFONT hFont = CreateFont(30, 0, 0, 0, FW_THIN, 0, 0, 0, UNICODE, 0, 0, 0, 0, L"微软雅黑");
	SelectObject(hdc, hFont);
	SetBkMode(hdc, TRANSPARENT);//设置指定DC的背景混合模式，背景混合模式用于与文本，填充画刷和当画笔不是实线时
	SetBkColor(hdc, RGB(255, 255, 0));//用白色来设置当前的背景色
	RECT rect;//绘制右部面板框
	rect.left = rc_right_top.left + 40;
	rect.top = rc_right_top.bottom - 150;
	rect.right = rc_right_top.right;
	rect.bottom = rc_right_top.bottom;
	DrawTextW(hdc, TEXT("下一个"), _tcslen(TEXT("下一个")), &rect, 0);
	DeleteObject(hFont);
	DeleteObject(hBrush);
}

void drawScore(HDC hdc)//绘制分数和难度
{
	HFONT hFont = CreateFont(30, 0, 0, 0, FW_THIN, 0, 0, 0, UNICODE, 0, 0, 0, 0, L"微软雅黑");
	SelectObject(hdc, hFont);
	SetBkMode(hdc, TRANSPARENT);
	SetBkColor(hdc, RGB(255, 255, 0));
	RECT rect;
	rect.left = rc_right_bottom.left;
	rect.top = rc_right_bottom.top;
	rect.right = rc_right_bottom.right;
	rect.bottom = rc_right_bottom.bottom;
	TCHAR szBuf[30];
	LPCTSTR cstr = TEXT("当前难度：%d");
	wsprintf(szBuf, cstr, GAME_DIFF);
	DrawTextW(hdc, szBuf, _tcslen(szBuf), &rect, DT_CENTER | DT_VCENTER);

	RECT rect2;
	rect2.left = rc_right_bottom.left;
	rect2.top = rc_right_bottom.bottom / 2 + 100;
	rect2.right = rc_right_bottom.right;
	rect2.bottom = rc_right_bottom.bottom;
	TCHAR szBuf2[30];
	LPCTSTR cstr2 = TEXT("得分：%d");
	wsprintf(szBuf2, cstr2, GAME_SCORE);
	//outPutBoxInt(sizeof(szBuf));
	DrawTextW(hdc, szBuf2, _tcslen(szBuf2), &rect2, DT_CENTER | DT_VCENTER);

	DeleteObject(hFont);
}

int selectDiffculty(HWND hWnd, int diff)//选择难度
{
	TCHAR szBuf2[30];
	LPCTSTR cstr2 = TEXT("确认选择难度 %d 吗？");
	wsprintf(szBuf2, cstr2, diff);
	if (MessageBox(hWnd, szBuf2, L"难度选择", MB_YESNO) == IDYES)
	{
		GAME_DIFF = diff;
		InvalidateRect(hWnd, &rc_right_bottom, false);
		GAME_STATE |= 2;
		init_game();
		return GAME_DIFF;
	}
	return -1;
}

int selectLayOut(HWND hWnd, int layout)//选择一行的格子数
{
	NUM_X = 10 * layout;
	NUM_Y = 20 * layout;
	BLOCK_SIZE = 30 / layout;
	GAME_STATE |= 2;
	InvalidateRect(hWnd, &rc_right_bottom, false);
	init_game();
	return layout;
}

void drawCompleteParticle(int line)//在消去一行画出消去效果
{
	HWND hWnd = GetActiveWindow();
	HDC hdc = GetDC(hWnd);
	HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);
	HPEN hPen = (HPEN)CreatePen(PS_DOT, 1, RGB(255, 255, 0));
	SelectObject(hdc, hBrush);
	SelectObject(hdc, hPen);
	Rectangle(hdc, BORDER_X,
		BORDER_Y + line*BLOCK_SIZE,
		BORDER_X + NUM_X*BLOCK_SIZE,
		BORDER_Y + BLOCK_SIZE*(1 + line));
	DeleteObject(hBrush);
	DeleteObject(hPen);
}