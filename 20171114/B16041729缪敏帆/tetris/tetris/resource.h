//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 tetris.rc 使用
//
#define IDC_MYICON                      2
#define IDD_TETRIS_DIALOG               102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_TETRIS                      107
#define IDI_SMALL                       108
#define IDC_TETRIS                      109
#define IDR_MAINFRAME                   128
#define IDC_                            32771
#define IDM_DIFF                        32772
#define ID_dif1                         32773
#define ID_32774                        32774
#define ID_32775                        32775
#define ID_dif2                         32776
#define ID_dif3                         32777
#define ID_LAYOUT1                      32780
#define ID_LAYOUT2                      32781
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32782
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
