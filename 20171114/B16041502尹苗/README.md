## 运行结果说明：
### 运行界面（如图1所示）
![运行结果图1](http://a1.qpic.cn/psb?/V141RwUG3Hgo3P/g2ZEytXAT4L3SRi5fBgxlCJBRQ3PV1ZD.0qPpK6tUiE!/b/dPMAAAAAAAAA&bo=BAKjAgAAAAADAII!&rf=viewer_4)
### 难度选择为难度3、布局选择为布局1（如图2所示）
![运行结果图2]
(http://a1.qpic.cn/psb?/V141RwUG3Hgo3P/n6VY07Bdniox4bxQioEeI9vSY7032CywJASVPFgv9bU!/b/dD4BAAAAAAAA&bo=BwKkAgAAAAADAIY!&rf=viewer_4)
### 难度选择为难度2、布局选择为布局2（如图3所示）
![运行结果图3]
(http://a1.qpic.cn/psb?/V141RwUG3Hgo3P/7WocEoo1XlVdu1oNsEX7Jn9OkdGrq9C7a2GJh6FV1lg!/b/dPMAAAAAAAAA&bo=DgKmAgAAAAADAI0!&rf=viewer_4)