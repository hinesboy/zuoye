# 一、结果
## 1、选择难度1
![选择难度1](https://gitee.com/AYuKKNi/images/raw/a84b527a302b278d47930a5763305a3e8350d80b/1.PNG)
## 2、正在运行
![正在运行]
(https://gitee.com/AYuKKNi/images/raw/a84b527a302b278d47930a5763305a3e8350d80b/2.PNG)
## 3、选择难度2
![选择难度2]
(https://gitee.com/AYuKKNi/images/raw/a84b527a302b278d47930a5763305a3e8350d80b/3.PNG)
## 4、选择布局1
![选择布局1]
(https://gitee.com/AYuKKNi/images/raw/a84b527a302b278d47930a5763305a3e8350d80b/5.PNG)
## 5、选择布局2
![选择布局2]
(https://gitee.com/AYuKKNi/images/raw/a84b527a302b278d47930a5763305a3e8350d80b/4.PNG)
# 二、实验感想
## 1、对于俄罗斯方块的代码有了基本了解。
## 2、对于自己写俄罗斯方块也有了一定信心。