#include "stdafx.h"
#include "Tetris.h"
#include "windows.h"
#include <mmsystem.h>//为了使用Play Sound函数
#pragma comment(lib, "WINMM.LIB")//为了使用Play Sound函数
#define MAX_LOADSTRING 100

// 此代码模块中包含的函数的前向声明:
ATOM                MyRegisterClass(HINSTANCE hInstance);//注册窗口类
BOOL                InitInstance(HINSTANCE, int);//初始化应用程序
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);//窗口过程处理函数
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

// 全局变量:
HINSTANCE hInst;                                // 当前实例
TCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本
TCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名
HMENU diff;//难度菜单句柄的定义
HMENU lay;//布局菜单句柄的定义

int APIENTRY _tWinMain(HINSTANCE hInstance,//应用程序当前实例句柄
	HINSTANCE hPrevInstance,//应用程序其他实例句柄
	LPTSTR    lpCmdLine,//指向程序命令行参数的指针
	int       nCmdShow)//应用程序开始执行时窗口显示方式的整数值标识
{
	init_game();//初始化游戏
	UNREFERENCED_PARAMETER(hPrevInstance);//展开传递的参数，避免编译器关于未引用参数的警告
	UNREFERENCED_PARAMETER(lpCmdLine);////展开传递的参数，避免编译器关于未引用参数的警告

	// TODO: 在此放置代码。
	MSG msg;
	HACCEL hAccelTable;

	// 初始化全局字符串
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);//导入字符串资源
	LoadString(hInstance, IDC_TETRIS, szWindowClass, MAX_LOADSTRING);//导入字符串资源
	MyRegisterClass(hInstance);//注册窗口类

	// 执行应用程序初始化:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TETRIS));//加载加速器

	// 主消息循环:
	while (1)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))//收到消息
		{
			TranslateMessage(&msg);//将消息的虚拟键转化为字符信息
			DispatchMessage(&msg);//将参数指定的消息传送到指定的窗口函数
			if (msg.message == WM_QUIT)//如果消息是退出应用程序
			{
				break;//终止程序
			}
		}
		else
		{
			if ((GAME_STATE & 2) != 0)//如果游戏开始时
			{
				tCur = GetTickCount();//返回时间
				if (tCur - tPre>g_speed)//如果超过返回时间
				{

					int flag = CheckValide(curPosX, curPosY + 1, bCurTeris);//判断下一行的情况，返回给flag
					if (flag == 1)//下降一行
					{
						curPosY++;//方块坐标y加1
						tPre = tCur;//重新计算反应时间
						HWND hWnd = GetActiveWindow();
						InvalidateRect(hWnd, &rc_left, FALSE);//向窗口添加无效区域，并发送WM_PAINT消息要求系统重绘这个无效区域
						InvalidateRect(hWnd, &rc_right_top, FALSE);//向窗口添加无效区域，并发送WM_PAINT消息要求系统重绘这个无效区域
					}
					else if (flag == -2)//方块到底时
					{
						g_speed = t_speed;//重置速度
						fillBlock();//将方块填充给矩形
						checkComplite(); //查看能否消去这行
						setRandomT();//重新产生随机方块
						curPosX = (NUM_X - 4) >> 1;//重置方块坐标x
						curPosY = 0;//重置方块坐标y
						HWND hWnd = GetActiveWindow();//获得与调用线程的消息队列相关的活动窗口的窗口句柄
						InvalidateRect(hWnd, &rc_main, FALSE);//向窗口添加无效区域，并发送WM_PAINT消息要求系统重绘这个无效区域
					}
					else if (flag == -3)//如果游戏输了
					{
						HWND hWnd = GetActiveWindow();//获得与调用线程的消息队列相关的活动窗口的窗口句柄
						if (MessageBox(hWnd, L"胜败乃兵家常事，菜鸡请重新来过", L":时光机", MB_YESNO) == IDYES)
						{
							init_game();//重新开始游戏
						}
						else
						{
							break;//退出消息循环，结束程序
						}
					}
				}
			}
		}
	}

	return (int)msg.wParam;//程序终止时将信息返回系统
}


//
//  函数: MyRegisterClass()3
//
//  目的: 注册窗口类。
//
//  注释:
//
//    仅当希望
//    此代码与添加到 Windows 95 中的“RegisterClassEx”
//    函数之前的 Win32 系统兼容时，才需要此函数及其用法。调用此函数十分重要，
//    这样应用程序就可以获得关联的
//    “格式正确的”小图标。
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;//定义窗口类对象

	wcex.cbSize = sizeof(WNDCLASSEX);//窗口类的大小
	wcex.style = CS_HREDRAW | CS_VREDRAW;//窗口类的类型
	wcex.lpfnWndProc = WndProc;//窗口处理函数为WndProc
	wcex.cbClsExtra = 0;//窗口类无例句柄扩展
	wcex.cbWndExtra = 0;//窗口实例无扩展
	wcex.hInstance = hInstance;//当前实例句柄
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TETRIS));//窗口的图标为默认图标
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);//窗口采用箭头光标
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);//窗口背景为白色
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_TETRIS);//使用系统自定义的菜单资源
	wcex.lpszClassName = szWindowClass;//窗口类名为“窗口示例”
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));//窗口的小图标为默认图标

	return RegisterClassExW(&wcex);//返回注册窗口
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释:
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;//当前实例句柄

	hInst = hInstance; // 将实例句柄存储在全局变量中

	hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);//创建窗口
	if (!hWnd)//如果创建窗口失败则发出警告
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);//显示窗口
	UpdateWindow(hWnd);//绘制用户区
	return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的: 处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;//WIndows系统提供的标识无效区域的结构
	HDC hdc;//设置环境句柄
	int nWinx, nWiny, nClientX, nClientY;//设置坐标
	int posX, posY;//设置坐标
	RECT rect;//矩形标识
	HMENU hSysmenu;//菜单窗口的句柄
	switch (message)
	{
	case WM_CREATE://当一个应用程序通过CreateWindowEx函数或者
		//CreateWindow函数请求创建窗口时发送此消息，(此消息在函数返回之前发送)。

		GetWindowRect(hWnd, &rect);//就是获取窗体的边界矩形赋值给rect
		nWinx = 530;
		nWiny = 680;
		posX = GetSystemMetrics(SM_CXSCREEN);//获取屏幕的宽度
		posY = GetSystemMetrics(SM_CYSCREEN);//获取屏幕的高度
		posX = (posX - nWinx) >> 1;//变成二进制
		posY = (posY - nWiny) >> 1;//变成二进制
		GetClientRect(hWnd, &rect);//得到窗口句柄的客户区大小
		nClientX = rect.right - rect.left;//客户区的宽度
		nClientY = rect.bottom - rect.top;//客户区的高度

		MoveWindow(hWnd, posX, posY, 530, 680, TRUE);//设置位置及分辨率
		hSysmenu = GetMenu(hWnd);//获取系统菜单句柄
		AppendMenu(hSysmenu, MF_SEPARATOR, 0, NULL);//添加菜单
		diff = CreatePopupMenu();//创建一个难度菜单句柄
		//AppendMenu(hSysmenu, 0, IDM_DIFF, L"难度选择");
		AppendMenu(hSysmenu, MF_POPUP, (UINT_PTR)diff, L"难度选择");//添加菜单diff
		AppendMenu(diff, MF_STRING, ID_dif1, L"难度1");//在菜单diff下创建菜单项难度1
		AppendMenu(diff, MF_STRING, ID_dif2, L"难度2");//在菜单diff下创建菜单项难度2
		AppendMenu(diff, MF_STRING, ID_dif3, L"难度3");//在菜单diff下创建菜单项难度3
		lay = CreatePopupMenu();//创建一个布局菜单句柄
		AppendMenu(hSysmenu, MF_POPUP, (UINT_PTR)lay, L"布局选择");//添加菜单lay
		AppendMenu(lay, MF_STRING, ID_LAYOUT1, L"布局1");//在菜单lay下创建菜单项布局1
		AppendMenu(lay, MF_STRING, ID_LAYOUT2, L"布局2");//在菜单lay下创建菜单项布局2
		SetMenu(hWnd, hSysmenu);//设置系统自定义菜单
		SetMenu(hWnd, diff);//设置难度菜单
		SetMenu(hWnd, lay);//设置布局菜单
		break;
	case WM_COMMAND://当用户从菜单选中一个命令项目
		//、当一个控件发送通知消息给去父窗口或者按下一个快捷键将发送 WM_COMMAND 消息
		wmId = LOWORD(wParam);//取低位
		wmEvent = HIWORD(wParam);//取高位
		// 分析菜单选择:
		switch (wmId)
		{
		case IDM_ABOUT: //关于按键
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:  //退出按键
			DestroyWindow(hWnd);//退出程序
			break;
		case ID_dif1: //难度1按键
			selectDiffculty(hWnd, 1);//选择难度1
			break;
		case ID_dif2: //难度2按键
			selectDiffculty(hWnd, 2);//选择难度2
			break;
		case ID_dif3: //难度3按键
			selectDiffculty(hWnd, 3);//选择难度3
			break;
		case ID_LAYOUT1: //布局1按键
			selectLayOut(hWnd, 1);//选择布局1
			break;
		case ID_LAYOUT2: //布局2按键
			selectLayOut(hWnd, 2);//选择布局1
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);//返回缺省的窗口过程来为应用程序没有处理的任何窗口消息提供缺省的处理
		}
		break;
	case WM_KEYDOWN://当一个非系统键被按下时该消息发送给具有键盘焦点的窗口。
		hdc = GetDC(hWnd);//该函数检索一指定窗口的客户区域或整个屏幕的显示设备上下文环境的句柄
		InvalidateRect(hWnd, NULL, false);//该函数向指定的窗体更新区域添加一个矩形，然后窗口客户区域的这一部分将被重新绘制
		switch (wParam)
		{
		case VK_LEFT://键盘上的左箭头
			curPosX--;//坐标x-1
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)//检查是否超过边缘
			{
				curPosX++;//坐标x+1，返回原来的坐标x
			}
			break;
		case VK_RIGHT://键盘上的右箭头
			curPosX++;//坐标x+1
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)//检查是否超过边缘
			{
				curPosX--;//坐标x-1，返回原来的坐标x
			}
			break;
		case VK_UP://键盘上的上箭头
			RotateTeris(bCurTeris);//变换图形方向
			break;
		case VK_DOWN://键盘上的下箭头
			if (g_speed == t_speed)//如果速度相等
				g_speed = 10;
			else
				g_speed = t_speed;//加快速度
			//outPutBoxInt(g_speed);
			break;
		case 'W':
			RotateTeris(bCurTeris);//变换图形方向
			break;
		case 'A':
			curPosX--;//坐标x-1
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)//检查是否超过边缘
			{
				curPosX++;//坐标x+1，返回原来的坐标x
			}
			break;
		case 'D':
			curPosX++;//坐标x+1
			if (CheckValide(curPosX, curPosY, bCurTeris) != 1)//检查是否超过边缘
			{
				curPosX--;//坐标x-1，返回原来坐标
			}
			break;

		case 'S':
			if (g_speed == t_speed)//如果速度相等
				g_speed = 10;
			else
				g_speed = t_speed;//加快速度
			//outPutBoxInt(g_speed);
			break;
		default:
			break;
		}

	case WM_PAINT://更新画面
		hdc = BeginPaint(hWnd, &ps);
		// TODO: 在此添加任意绘图代码...
		DrawBackGround(hdc);
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY://窗口销毁后(调用DestroyWindow()后)，消息队列得到的消息。
		PostQuitMessage(0);//退出程序
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);//返回缺省的窗口过程来为
		//应用程序没有处理的任何窗口消息提供缺省的处理。该函数确保每一个消息得到处理。 
	}
	return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);//告诉编译器,已经使用了该变量,不必检测警告
	switch (message)
	{
	case WM_INITDIALOG://表明对话框及其所有子控件都创建完毕了
		return (INT_PTR)TRUE;

	case WM_COMMAND://当用户从菜单选中一个命令项目、当一个控件发送通知消息给去父窗口或者按下一个快捷键将发送 WM_COMMAND 消息
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


void drawBlocked(HDC mdc)//绘制当前已经存在砖块的区域
{
	int i, j;

	HBRUSH hBrush = (HBRUSH)CreateSolidBrush(RGB(255, 255, 0));

	SelectObject(mdc, hBrush);

	for (i = 0; i<NUM_Y; i++)
	{
		for (j = 0; j<NUM_X; j++)
		{
			if (g_hasBlocked[i][j])
			{
				Rectangle(mdc, BORDER_X + j*BLOCK_SIZE, BORDER_Y + i*BLOCK_SIZE,
					BORDER_X + (j + 1)*BLOCK_SIZE, BORDER_Y + (i + 1)*BLOCK_SIZE
				);
			}
		}
	}
	DeleteObject(hBrush);
}

int CheckValide(int startX, int startY, BOOL bTemp[4][4])////给定一个矩阵，查看是否合法
{
	int i, j;
	for (i = 3; i >= 0; i--)
	{
		for (j = 3; j >= 0; j--)
		{
			if (bTemp[i][j])
			{
				if (j + startX<0 || j + startX >= NUM_X)
				{
					return -1;
				}
				if (i + startY >= NUM_Y)
				{
					return -2;
				}
				if (g_hasBlocked[i + startY][j + startX])
				{
					//outPutBoxInt(j+startY);
					if (curPosY == 0)
					{
						return -3;
					}
					return -2;
				}
			}
		}
	}
	//MessageBox(NULL,L"这里",L"as",MB_OK);
	//outPutBoxInt(curPosY);
	return 1;
}

void checkComplite()//查看一行是否能消去  采用从上往下的消法，消去一行后把上面的每行都往下移
{
	int i, j, k, count = 0;
	for (i = 0; i<NUM_Y; i++)
	{
		bool flag = true;
		for (j = 0; j<NUM_X; j++)
		{
			if (!g_hasBlocked[i][j])
			{
				flag = false;
			}
		}
		if (flag)
		{
			count++;
			for (j = i; j >= 1; j--)
			{
				for (k = 0; k<NUM_X; k++)
				{
					g_hasBlocked[j][k] = g_hasBlocked[j - 1][k];
				}

			}
			drawCompleteParticle(i);
			Sleep(300);

			PlaySound(_T("coin.wav"), NULL, SND_FILENAME | SND_ASYNC);
		}
	}
	GAME_SCORE += int(count*1.5);
}



VOID outPutBoxInt(int num) //自定义的弹窗函数  用于调试
{
	TCHAR szBuf[1024];
	LPCTSTR str = TEXT("%d");
	wsprintf(szBuf, str, num);//将一系列的字符和数值输入到缓冲区。
	MessageBox(NULL, szBuf, L"aasa", MB_OK);
}

VOID outPutBoxString(TCHAR str[1024])
{
	TCHAR szBuf[1024];
	LPCTSTR cstr = TEXT("%s");
	wsprintf(szBuf, cstr, str);//将一系列的字符和数值输入到缓冲区。
	MessageBox(NULL, szBuf, L"aasa", MB_OK);
}




void setRandomT()//随机生成一个方块用作下一次掉落
{
	int rd_start = RandomInt(0, sizeof(state_teris) / sizeof(state_teris[0]));
	int rd_next = RandomInt(0, sizeof(state_teris) / sizeof(state_teris[0]));
	//outPutBoxInt(rd_start);
	//outPutBoxInt(rd_next);
	//outPutBoxInt(rd_start);
	if (GAME_STATE == 0)
	{
		GAME_STATE = GAME_STATE | 0x0001;
		//outPutBoxInt(GAME_STATE);
		memcpy(bCurTeris, state_teris[rd_start], sizeof(state_teris[rd_start]));
		memcpy(bNextCurTeris, state_teris[rd_next], sizeof(state_teris[rd_next]));
	}
	else
	{
		memcpy(bCurTeris, bNextCurTeris, sizeof(bNextCurTeris));
		memcpy(bNextCurTeris, state_teris[rd_next], sizeof(state_teris[rd_next]));
	}

}
void init_game()//初始化游戏
{
	GAME_SCORE = 0;
	setRandomT();//随机生成一个方块用作下一次掉落
	curPosX = (NUM_X - 4) >> 1;//设置初始坐标
	curPosY = 0;


	memset(g_hasBlocked, 0, sizeof(g_hasBlocked));
	rc_left.left = 0;
	rc_left.right = SCREEN_LEFT_X;
	rc_left.top = 0;
	rc_left.bottom = SCREEN_Y;

	rc_right.left = rc_left.right + BORDER_X;
	rc_right.right = 180 + rc_right.left;
	rc_right.top = 0;
	rc_right.bottom = SCREEN_Y;

	rc_main.left = 0;
	rc_main.right = SCREEN_X;
	rc_main.top = 0;
	rc_main.bottom = SCREEN_Y;

	rc_right_top.left = rc_right.left;
	rc_right_top.top = rc_right.top;
	rc_right_top.right = rc_right.right;
	rc_right_top.bottom = (rc_right.bottom) / 2;

	rc_right_bottom.left = rc_right.left;
	rc_right_bottom.top = rc_right_top.bottom + BORDER_Y;
	rc_right_bottom.right = rc_right.right;
	rc_right_bottom.bottom = rc_right.bottom;

	g_speed = t_speed = 1000 - GAME_DIFF * 280;
}

void fillBlock() //到达底部后填充矩阵
{
	int i, j;
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bCurTeris[i][j])
			{
				g_hasBlocked[curPosY + i][curPosX + j] = 1;
			}
		}
	}
}

void RotateTeris(BOOL bTeris[4][4])//  //旋转矩阵
{
	BOOL bNewTeris[4][4];
	int x, y;
	for (x = 0; x<4; x++)
	{
		for (y = 0; y<4; y++)
		{
			bNewTeris[x][y] = bTeris[3 - y][x];//旋转角度
			//逆时针：
			//bNewTeris[x][y] = bTeris[y][3-x];
		}
	}
	if (CheckValide(curPosX, curPosY, bNewTeris) == 1)
	{
		memcpy(bTeris, bNewTeris, sizeof(bNewTeris));
	}

}

int RandomInt(int _min, int _max) //获取一个随机int
{
	srand((rd_seed++) % 65532 + GetTickCount());
	return _min + rand() % (_max - _min);
}

VOID DrawTeris(HDC mdc)  //绘制正在下落的方块
{

	int i, j;
	HPEN hPen = (HPEN)GetStockObject(BLACK_PEN);
	HBRUSH hBrush = (HBRUSH)GetStockObject(WHITE_BRUSH);
	SelectObject(mdc, hPen);//electObject是计算机编程语言函数，
	//该函数选择一对象到指定的设备上下文环境中，新对象替换先前的相同类型的对象
	SelectObject(mdc, hBrush);//electObject是计算机编程语言函数，
	//该函数选择一对象到指定的设备上下文环境中，新对象替换先前的相同类型的对象
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bCurTeris[i][j])
			{
				Rectangle(mdc, (j + curPosX)*BLOCK_SIZE + BORDER_X, (i + curPosY)*BLOCK_SIZE + BORDER_Y, (j + 1 + curPosX)*BLOCK_SIZE + BORDER_X, (i + 1 + curPosY)*BLOCK_SIZE + BORDER_Y);
			}
		}
	}
	drawBlocked(mdc);
	DeleteObject(hPen);
	DeleteObject(hBrush);
}

VOID DrawBackGround(HDC hdc)   //绘制背景
{

	HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);
	//应用程序可以通过调用GetDeviceCaps函数来确定一个设备是否支持这些操作
	HDC mdc = CreateCompatibleDC(hdc);
	HBITMAP hBitmap = CreateCompatibleBitmap(hdc, SCREEN_X, SCREEN_Y);

	SelectObject(mdc, hBrush);
	SelectObject(mdc, hBitmap);

	HBRUSH hBrush2 = (HBRUSH)GetStockObject(WHITE_BRUSH);
	FillRect(mdc, &rc_main, hBrush2);
	Rectangle(mdc, rc_left.left + BORDER_X, rc_left.top + BORDER_Y, rc_left.right, rc_left.bottom);
	Rectangle(mdc, rc_right.left + BORDER_X, rc_right.top + BORDER_Y, rc_right.right, rc_right.bottom);
	DrawTeris(mdc);
	drawNext(mdc);
	drawScore(mdc);
	::BitBlt(hdc, 0, 0, SCREEN_X, SCREEN_Y, mdc, 0, 0, SRCCOPY);
	DeleteObject(hBrush);
	DeleteDC(mdc);
	DeleteObject(hBitmap);
	DeleteObject(hBrush2);


	//  int x,y;
	//  HPEN hPen = (HPEN)GetStockObject(NULL_PEN);
	//  HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);
	//  SelectObject(hdc,hPen);
	//  SelectObject(hdc,hBrush);
	//  for (x = 0;x<NUM_X;x++)
	//  {
	//      for(y=0;y<NUM_Y;y++)
	//      {
	//          Rectangle(hdc,BORDER_X+x*BLOCK_SIZE,BORDER_Y+y*BLOCK_SIZE,
	//              BORDER_X+(x+1)*BLOCK_SIZE,
	//              BORDER_Y+(y+1)*BLOCK_SIZE);
	//      }
	//  }
}

void drawNext(HDC hdc)
{
	int i, j;
	HBRUSH hBrush = (HBRUSH)CreateSolidBrush(RGB(0, 188, 0));
	SelectObject(hdc, hBrush);
	for (i = 0; i<4; i++)
	{
		for (j = 0; j<4; j++)
		{
			if (bNextCurTeris[i][j])
			{
				Rectangle(hdc, rc_right_top.left + BLOCK_SIZE*(j + 1), rc_right_top.top + BLOCK_SIZE*(i + 1), rc_right_top.left + BLOCK_SIZE*(j + 2), rc_right_top.top + BLOCK_SIZE*(i + 2));
			}
		}
	}
	HFONT hFont = CreateFont(30, 0, 0, 0, FW_THIN, 0, 0, 0, UNICODE, 0, 0, 0, 0, L"微软雅黑");
	SelectObject(hdc, hFont);
	SetBkMode(hdc, TRANSPARENT);
	SetBkColor(hdc, RGB(255, 255, 0));
	RECT rect;
	rect.left = rc_right_top.left + 40;
	rect.top = rc_right_top.bottom - 150;
	rect.right = rc_right_top.right;
	rect.bottom = rc_right_top.bottom;
	DrawTextW(hdc, TEXT("下一个"), _tcslen(TEXT("下一个")), &rect, 0);
	DeleteObject(hFont);
	DeleteObject(hBrush);
}

void drawScore(HDC hdc) //绘制分数
{
	HFONT hFont = CreateFont(30, 0, 0, 0, FW_THIN, 0, 0, 0, UNICODE, 0, 0, 0, 0, L"微软雅黑");
	SelectObject(hdc, hFont);
	SetBkMode(hdc, TRANSPARENT);
	SetBkColor(hdc, RGB(255, 255, 0));
	RECT rect;
	rect.left = rc_right_bottom.left;
	rect.top = rc_right_bottom.top;
	rect.right = rc_right_bottom.right;
	rect.bottom = rc_right_bottom.bottom;
	TCHAR szBuf[30];
	LPCTSTR cstr = TEXT("当前难度：%d");
	wsprintf(szBuf, cstr, GAME_DIFF);
	DrawTextW(hdc, szBuf, _tcslen(szBuf), &rect, DT_CENTER | DT_VCENTER);

	RECT rect2;
	rect2.left = rc_right_bottom.left;
	rect2.top = rc_right_bottom.bottom / 2 + 100;
	rect2.right = rc_right_bottom.right;
	rect2.bottom = rc_right_bottom.bottom;
	TCHAR szBuf2[30];
	LPCTSTR cstr2 = TEXT("得分：%d");
	wsprintf(szBuf2, cstr2, GAME_SCORE);
	//outPutBoxInt(sizeof(szBuf));
	DrawTextW(hdc, szBuf2, _tcslen(szBuf2), &rect2, DT_CENTER | DT_VCENTER);

	DeleteObject(hFont);
}

int selectDiffculty(HWND hWnd, int diff)//选择难度
{
	TCHAR szBuf2[30];
	LPCTSTR cstr2 = TEXT("确认选择难度 %d 吗？");
	wsprintf(szBuf2, cstr2, diff);
	if (MessageBox(hWnd, szBuf2, L"难度选择", MB_YESNO) == IDYES)
	{
		GAME_DIFF = diff;
		InvalidateRect(hWnd, &rc_right_bottom, false);
		GAME_STATE |= 2;
		init_game();
		return GAME_DIFF;
	}
	return -1;
}

int selectLayOut(HWND hWnd, int layout)//选择布局
{
	NUM_X = 10 * layout;
	NUM_Y = 20 * layout;
	BLOCK_SIZE = 30 / layout;
	GAME_STATE |= 2;
	InvalidateRect(hWnd, &rc_right_bottom, false);
	init_game();
	return layout;
}

void drawCompleteParticle(int line)
{
	HWND hWnd = GetActiveWindow();
	HDC hdc = GetDC(hWnd);
	HBRUSH hBrush = (HBRUSH)GetStockObject(GRAY_BRUSH);
	HPEN hPen = (HPEN)CreatePen(PS_DOT, 1, RGB(255, 255, 0));
	SelectObject(hdc, hBrush);
	SelectObject(hdc, hPen);
	Rectangle(hdc, BORDER_X,
		BORDER_Y + line*BLOCK_SIZE,
		BORDER_X + NUM_X*BLOCK_SIZE,
		BORDER_Y + BLOCK_SIZE*(1 + line));
	DeleteObject(hBrush);
	DeleteObject(hPen);
}