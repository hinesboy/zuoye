#pragma once//宏  [p'ræɡmə]杂注; 编译指示;

/*
作用：
为了避免同一个头文件被包含（include）多次，C/C++中有两种宏实现方式：一种是#ifndef方式；另一种是#pragma once方式。
在能够支持这两种方式的编译器上，二者并没有太大的区别。但两者仍然有一些细微的区别。
方式一：
#ifndef __SOMEFILE_H__
#define __SOMEFILE_H__
... ... // 一些声明语句
#endif
方式二：
#pragma once
... ... // 一些声明语句
#ifndef的方式依赖于宏名字不能冲突，这不光可以保证同一个文件不会被包含多次，也能保证内容完全相同的两个文件不会被不小心同时包含。当然，缺点就是如果不同头文件的宏名不小心“撞车”，可能就会导致头文件明明存在，编译器却硬说找不到声明的状况
#pragma once则由编译器提供保证：同一个文件不会被包含多次。注意这里所说的“同一个文件”是指物理上的一个文件，而不是指内容相同的两个文件。带来的好处是，你不必再费劲想个宏名了，当然也就不会出现宏名碰撞引发的奇怪问题。对应的缺点就是如果某个头文件有多份拷贝，本方法不能保证他们不被重复包含。当然，相比宏名碰撞引发的“找不到声明”的问题，重复包含更容易被发现并修正。
方式一由语言支持所以移植性好，方式二 可以避免名字冲突

这是一个比较常用的指令,只要在头文件的最开始加入这条指令就能够保证头文件被编译一次
#pragma once用来防止某个头文件被多次include，#ifndef，#define，#endif用来防止某个宏被多次定义。
#pragma once是编译相关，就是说这个编译系统上能用，但在其他编译系统不一定可以，也就是说移植性差，不过现在基本上已经是每个编译器都有这个定义了。
#ifndef，#define，#endif这个是C++语言相关，这是C++语言中的宏定义，通过宏定义避免文件多次编译。所以在所有支持C++语言的编译器上都是有效的，如果写的程序要跨平台，最好使用这种方式
*/

#include "resource.h"
/*
VS会根据你在可视化界面的设计,会自动管理该文件.包括.rc文件。
但是，有时候VS也会出点小问题就需要自己动手进去修改，可以增加灵活性。
两种手段，各有优劣，优势互补。

这个就是你在资源视图中添加资源的时候VC给你资源定义的ID，就是一系列宏而已

resource.h一般不需要手动写
当你添加一个资源并保存时，VC会自动在resource.h文件中增加一个宏定义。该定义确定资源的ID。
但是当你删除一个资源时，VC并不会在resource.h中删除该ID的定义。不过这个并不会影响你对资源的使用。因为当你下次添加资源时以相同的ID来定位资源时，VC会自动搜索头文件，如果已经存在并且未被使用，则不重新定义该ID。如果已经存在且已经被使用，系统将会对你进行提示。

resource.h就是.rc文件的头文件
.rc文件里的常量全在resource.h定义
一般情况下不用你去写和修改
vc会帮你写和改
但是如果你想用以前的.rc中的资源比如你以前做的对话框
你只需复制粘贴就行了
就象下面这段

IDD_JK_DIALOG DIALOGEX 0, 0, 320, 200
STYLE DS_MODALFRAME | WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_SYSMENU
EXSTYLE WS_EX_APPWINDOW
CAPTION "jk"
FONT 9, "宋体"
BEGIN
PUSHBUTTON      "学号",IDC_BUTTON1,25,75,62,22
PUSHBUTTON      "姓名",IDC_BUTTON2,179,75,64,24
EDITTEXT        IDC_EDIT1,105,75,57,24,ES_AUTOHSCROLL
EDITTEXT        IDC_EDIT2,261,75,46,24,ES_AUTOHSCROLL
END
你可以复制到你新的工程里去。
方法是用记事本打开.rc文件然后粘贴进去
但是vc可能会提示IDD_JK_DIALOG IDC_BUTTON1，IDC_BUTTON2，IDC_EDIT1，IDC_EDIT2没有定义
这时你只需要用记事本打开resource.h
在里面加
#define IDD_JK_DIALOG                   102
#define IDC_BUTTON1                     1000
#define IDC_BUTTON2                     1001
#define IDC_EDIT1                       1002
#define IDC_EDIT2                       1003
如果你需要做一个有很多控件的对话框
但是以前做过一个一样的对话框
这时你就可以使用这种方法
*/


//函数声明
void checkComplite();  //查看一行是否能消去  采用从上往下的消法，消去一行后把上面的每行都往下移，但是我感觉效率有点低，以后看能不能有点改进//check:检查，核对complete:完整，完成
void drawBlocked(HDC hdc);      //绘制当前已经存在砖块的区域draw:绘画，拖拉block：块，障碍物
void DrawBackGround(HDC hdc);       //绘制背景
void outPutBoxInt(int num);     //自定义的弹窗函数  用于调试
void outPutBoxString(TCHAR str[1024]);
void setRandomT();      //随机生成一个方块用作下一次掉落
void init_game();       //初始化
void fillBlock();       //到达底部后填充矩阵
void RotateTeris(BOOL bTeris[4][4]);        //旋转矩阵
void DrawTeris(HDC mdc);    //绘制正在下落的方块
void drawNext(HDC hdc); //绘制下一个将要掉落的方块
void drawScore(HDC hdc);    //绘制分数
void drawCompleteParticle(int line);//[ˈpɑ:tɪkl]微粒，颗粒; [数，物] 粒子，质点; 极小量;

int RandomInt(int _min, int _max);       //获取一个随机int
int CheckValide(int curPosX, int curPosY, BOOL bCurTeris[4][4]);   //给定一个矩阵，查看是否合法
int selectDiffculty(HWND hWnd, int dif);
int selectLayOut(HWND hWnd, int layout);

//常量声明
const int BORDER_X = 10;//border:边框
const int BORDER_Y = 10;
const int SCREEN_LEFT_X = 300 + BORDER_X;；//screen：屏幕
const int SCREEN_Y = 600 + BORDER_Y;
const int SCREEN_RIGHT_X = 180 + BORDER_X * 2;
const int SCREEN_X = SCREEN_LEFT_X + SCREEN_RIGHT_X;
const BOOL state_teris[][4][4] =
{
	{ { 1,1,1,1 },{ 0,0,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,1,0 },{ 0,1,1,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,1,1 },{ 0,0,0,1 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,1,0 },{ 0,0,1,1 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,0,0 },{ 1,1,1,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,1,1 },{ 0,1,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } },
	{ { 0,1,1,0 },{ 1,1,0,0 },{ 0,0,0,0 },{ 0,0,0,0 } }
};



//全局变量声明
bool g_hasBlocked[50][50];
RECT rc_left, rc_right, rc_right_top, rc_right_bottom, rc_main;
int g_speed = 300;
int t_speed = 300;
BOOL bCurTeris[4][4];
BOOL bNextCurTeris[4][4];
int curPosX, curPosY;
int rd_seed = 1995421;
int tPre = 0, tCur;
int GAME_STATE = 0;
int GAME_SCORE = 0;
int GAME_DIFF = 1;
int NUM_X = 10;
int NUM_Y = 20;
int BLOCK_SIZE = 30;