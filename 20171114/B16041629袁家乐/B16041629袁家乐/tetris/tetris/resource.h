//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 tetris.rc 使用
//
#define IDC_MYICON                      2
#define IDD_TETRIS_DIALOG               102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_TETRIS                      107
#define IDI_SMALL                       108
#define IDC_TETRIS                      109
#define IDR_MAINFRAME                   128
#define IDM_DIFF                        32771
#define ID_32772                        32772
#define ID_DIFF                         32773
#define ID_32774                        32774
#define ID_Menu                         32775
#define ID_32776                        32776
#define ID_32777                        32777
#define ID_32778                        32778
#define ID_dif1                         32779
#define ID_dif2                         32780
#define ID_dif3                         32781
#define IDM_dif1                        32782
#define IDM_dif2                        32783
#define IDM_dif3                        32784
#define ID_32785                        32785
#define ID_32786                        32786
#define ID_32787                        32787
#define ID_32788                        32788
#define ID_32789                        32789
#define IDM_LAYOUT1                     32790
#define ID_32792                        32792
#define ID_32793                        32793
#define IDM_LAYOUT2                     32794
#define ID_32794                        32794
#define ID_Menu32795                    32795
#define ID_32796                        32796
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32797
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           112
#endif
#endif
