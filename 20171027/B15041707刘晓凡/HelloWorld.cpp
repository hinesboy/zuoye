// HelloWorld.cpp : Defines the entry point for the application.
//
//头文件
#include "stdafx.h"
#include "resource.h"

#define MAX_LOADSTRING 100//最大可显示长

// 全局变量:
HINSTANCE hInst;								                     //当前进程资源的句柄
TCHAR szTitle[MAX_LOADSTRING];							        	 // 窗口上方显示的标题
TCHAR szWindowClass[MAX_LOADSTRING];								// 窗口定义的名称
//实际上前面第一行的句柄就相当于ID号，每运行一个进程都需要系统分配一个编码来标示它。
//TCHAR是双字节的字符类型，char为单字节的字符类型。

// 此代码模块中包含的函数的前向声明:
ATOM				MyRegisterClass(HINSTANCE hInstance);//注册窗口函数的声明
BOOL				InitInstance(HINSTANCE, int);//初始化函数的函数的声明
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);//主窗口的回调函数
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);//“关于”对话框的回调函数

//win32程序的主程序相当于main
int APIENTRY WinMain(HINSTANCE hInstance,//当前实例的句柄（windows是多任务操作系统，一个程序可运行多个实例）
                     HINSTANCE hPrevInstance,//前一个实例的句柄
                     LPSTR     lpCmdLine,//长字符串指针数据类型，传给WinMain命令
                     int       nCmdShow)//指定窗口如何显示，一般没用
{
 	// TODO: 在此放置代码.
	MSG msg;//消息数据类型
	HACCEL hAccelTable;////存放键盘加速键（快捷键）表的句柄

	// 初始化全局字符串
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);//将IDS_APP_TITLE里的内容载入字符串szTitle
	LoadString(hInstance, IDC_HELLOWORLD, szWindowClass, MAX_LOADSTRING);//将IDC_TEST里的内容载入字符串szWindowClass中  
	MyRegisterClass(hInstance);//调用注册窗口函数，注册窗口  

	// 执行应用程序初始化:
	if (!InitInstance (hInstance, nCmdShow)) //调用初始化实例函数，启动窗口
	{
		return FALSE;//失败则程序退出 
	}

	//载入快捷键表
	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_HELLOWORLD);

	// 消息循环，如果得到退出消息返回0 
	while (GetMessage(&msg, NULL, 0, 0)) //从消息队列中取消息
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) //如果是不是快捷键则进入if，是则直接发给窗口过程  
		{
			TranslateMessage(&msg);//翻译消息  
			DispatchMessage(&msg);//发送消息给句柄
		}
	}

	return msg.wParam;//返回退出  
}



//
//  函数: MyRegisterClass()
//
//  目的：注册窗口类。
//
// 要点:
//此函数只有你想要代码能兼容添加到Windows 95与win32的“RegisterClassEx”兼容时才调用
//调用这个函数很重要，这样应用程序就会得到“格式良好”的小图标。

//注册窗口函数，实际调用的api是RegisterCl
ATOM MyRegisterClass(HINSTANCE hInstance)//ATOM为unsigned short类型 
{
	WNDCLASSEX wcex;//定义一个窗口类  

	wcex.cbSize = sizeof(WNDCLASSEX); //窗口大小 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;//窗口风格，用|符号能够同时选中
	wcex.lpfnWndProc	= (WNDPROC)WndProc;//窗口过程函数，下面有定义 
	wcex.cbClsExtra		= 0;//附加字节，一般为0 
	wcex.cbWndExtra		= 0;//附加字节，一般为0 
	wcex.hInstance		= hInstance;//实例句柄
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_HELLOWORLD);//窗口图标，可以在资源视图中改
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);//窗口中光标的风格  
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);//窗口画刷颜色 
	wcex.lpszMenuName	= (LPCSTR)IDC_HELLOWORLD;//菜单资源名称，可以在资源视图中改 
	wcex.lpszClassName	= szWindowClass;//窗口类名称  
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);//窗口小图标 

	return RegisterClassEx(&wcex);//注册  
}

//
//  函数: InitInstance(HANDLE, int)
//
//  目的: 保存实例句柄并创建主窗口 
//
//  要点:在这个函数中，我们将实例句柄保存在一个全局变量中，并创建和显示主程序窗口。

// 初始化实例函数,此函数功能是创建并显示窗口,实际使用的api 是CreateWindow,,ShowWindow
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)//启动窗口函数，第二个参数为启动类型
{
   HWND hWnd;//定义窗口句柄（标识）  

   hInst = hInstance; // 是将记录当前实例的句柄设置为该实例  

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,//创建窗口  
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)//判断是否创建成功
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);//显示窗口
   UpdateWindow(hWnd);//刷新窗口

   return TRUE;
}

//
//  函数: WndProc(HWND, unsigned, WORD, LONG)
//
//  目的:  处理主窗口的消息
//
//  WM_COMMAND	- 处理应用程序菜单
//  WM_PAINT	- 绘画主窗口
//  WM_DESTROY	- 发布一个退出消息并返回
//
//回掉系统API窗口过程函数，后面两个参数为消息  
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;//定义两个int变量
	PAINTSTRUCT ps;//保存了窗口绘制信息 
	HDC hdc;//窗口中元素的句柄  
	TCHAR szHello[MAX_LOADSTRING];//定义双字节字符串  
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);//将资源表里的字符给szHello变量

	switch (message) //选择消息  
	{
		case WM_COMMAND://菜单中选中一个命令时得到的消息 
			wmId    = LOWORD(wParam); //
			wmEvent = HIWORD(wParam); //
			// Parse the menu selections:
			switch (wmId)///
			{
				case IDM_ABOUT://关于  
				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);//跳出新图形窗口
				   break;
				case IDM_EXIT://退出  
				   DestroyWindow(hWnd);//销毁窗口
				   break;
				default:
				   return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_PAINT://绘制，该消息在窗口拉伸收缩、最小化后能接收到 
			hdc = BeginPaint(hWnd, &ps);//
			// TODO: 在这里添加任何绘图代码...
			RECT rt;//定义矩形变量  
			GetClientRect(hWnd, &rt);//将窗口尺寸赋给矩形变量  
			DrawText(hdc, szHello, strlen(szHello), &rt, DT_CENTER);//在窗口上打印szHello里的内容  
			EndPaint(hWnd, &ps);//结束绘制
			break;
		case WM_DESTROY:
			PostQuitMessage(0);//
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);//
   }
   return 0;
}

//关于窗口的Mesage处理程序
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)//菜单中点击关于弹出的小窗口
{
	switch (message)//选择消息  
	{
		case WM_INITDIALOG://此处添加初始化时进行的内容，默认没有添加
				return TRUE;//正常退出  

		case WM_COMMAND://选择了其中的按钮 
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) //如果点"确定"或者"取消"
			{
				EndDialog(hDlg, LOWORD(wParam));//结束 
				return TRUE;//正常退出
			}
			break;//
	}
    return FALSE;//异常退出
}
