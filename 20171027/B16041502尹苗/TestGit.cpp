//TestGit.cpp: 定义应用程序的入口点。
//

#include "stdafx.h"
#include "TestGit.h"

#define MAX_LOADSTRING 100

// 全局变量: 
HINSTANCE hInst;//宏定义 句柄型                                // 当前实例
WCHAR szTitle[MAX_LOADSTRING];//宏定义 char型                  // 标题栏文本
WCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名

// 此代码模块中包含的函数的前向声明: 
ATOM                MyRegisterClass(HINSTANCE hInstance);//ATOM:宏定义 word短整型
BOOL                InitInstance(HINSTANCE, int);//BOOL:布尔值
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);//LRESULT:宏定义 长整型  CALLBACK：回调函数WndProc
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);//LRESULT:宏定义 长整型  CALLBACK：回调函数About


	/*WinMain：应用程序入口点
	参数：
	hInstance是一个称为“实例句柄”或“模块句柄”的东西。操作系统在内存中加载时使用这个值来标识可执行文件(EXE)。实例句柄是某些窗口函数所必需的-例如，加载图标或位图。
	hPrevInstance毫无意义。它在16位的视窗中使用，但现在总是零。
	lpCmdLine将命令行参数作为unicode字符串包含。
	nCmdShow表示主应用程序窗口是否将最小化、最大化或正常显示的标志。

	函数返回一个int值。返回值不被操作系统使用，但可以使用返回值将状态代码传递给编写的其他程序。
	*/
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: 在此放置代码。

    // 初始化全局字符串

	/*函数LoadString
	函数原型及参数注释
	int WINAPI LoadString(
	_In_opt_ HINSTANCE hInstance,//应用程序实例句柄
	_In_     UINT      uID,//资源中的字符串编号
	_Out_    LPTSTR    lpBuffer,//接收从资源里拷贝字符串出来的缓冲区
	_In_     int       nBufferMax//指明缓冲区的大小
	);
	*/
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_TESTGIT, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // 执行应用程序初始化: 

	/*函数InitInstance
	功能：创建窗口
	如果初始化成功，则返回非零值；否则返回0
	*/
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

	/*函数LoadAccelerators
	功能：加载加速键资源
	参数：hInstance为当前程序实例句柄
	(LPCTSTR)IDC_WIN32为加速键表名
	*/
    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TESTGIT));//宏定义 加速键句柄

    MSG msg;//MSG:结构体

    // 主消息循环: 
    while (GetMessage(&msg, nullptr, 0, 0))
    {
		/*函数TranslateAccelerator
		功能：完成加速键消息到菜单消息的翻译
		参数：msg.hwnd为窗口句柄
		hAccelTable为加速键表句柄
		&msg为指向MSG结构的指针
		*/
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);//将虚拟键消息转换为字符消息，字符消息被送到调用线程的消息队列中，在下一次线程调用函数GetMessage时被读出
            DispatchMessage(&msg);//派发消息到窗口程序，传给窗体函数处理
        }
    }

    return (int) msg.wParam;//程序终止时将信息返回系统
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
	/*函数 MyRegisterClass
	功能：用于注册该窗口应用程序
	参数：见函数内部具体注释
	*/
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);//窗口类的大小

    wcex.style          = CS_HREDRAW | CS_VREDRAW;//设置窗口的样式
    wcex.lpfnWndProc    = WndProc;//窗口处理函数为(WNDPROC)WndProc
    wcex.cbClsExtra     = 0;//窗口类额外字节数（通常为0：窗口类无扩展）
    wcex.cbWndExtra     = 0;// 窗口实例额外字节数（通常为0：窗口实例无扩展）
    wcex.hInstance      = hInstance;//当前实例句柄
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TESTGIT));//指定窗口类的图标句柄（默认图标）
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);//指定窗口类的光标句柄（箭头光标）
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);//指定窗口类的背景画刷句柄
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_TESTGIT);//指定菜单资源名字
    wcex.lpszClassName  = szWindowClass;//指定窗口类的名字
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));//标识某个窗口最小化时显示的图标

    return RegisterClassExW(&wcex);//调用RegisterClassEx()函数向系统注册窗口类 
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。

//		  如果初始化成功，则返回非零值；否则返回0
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 将实例句柄存储在全局变量中

   /*函数CreateWindow
   功能：创建一个窗口类的实例
   函数原型及参数注释
   HWND WINAPI CreateWindow(
  _In_opt_  LPCTSTR lpClassName,//窗口类名称
  _In_opt_  LPCTSTR lpWindowName,//窗口标题
  _In_      DWORD dwStyle,//窗口风格，或称窗口格式
  _In_      int x,//初始 x 坐标
  _In_      int y,//初始 y 坐标
  _In_      int nWidth,//初始 x 方向尺寸
  _In_      int nHeight,//初始 y 方向尺寸
  _In_opt_  HWND hWndParent,//父窗口句柄
  _In_opt_  HMENU hMenu,//窗口菜单句柄
  _In_opt_  HINSTANCE hInstance,//创建程序实例句柄
  _In_opt_  LPVOID lpParam//创建参数（指向一个传递给窗口的参数值的指针）
	);
   
    */
   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);
   //HWND宏定义：用以检索消息的窗口句柄

   if (!hWnd)
   {
      return FALSE;
   }

   /*函数ShowWindow
   功能：在屏幕上显示窗口
   参数：hWnd为窗口句柄;nCmdShow为窗口显示形式标识
   */
   ShowWindow(hWnd, nCmdShow);
   /*函数UpdateWindow
   功能：更新并绘制用户区
   参数：hWnd为窗口句柄
   */
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
	/*函数 WndProc
	功能：Wndproc是Windows操作系统向应用程序发送一系列消息之一，每个窗口会有一个窗口过程的回调函数
	参数：窗口句柄(Window Handle) HWND 当前接收消息的窗口句柄，
	消息ID(Message ID) UINT被传送过来的消息
	WPARAM wParam和LPARAM lParam是包含有关消息的附加信息
	*/
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {

		//消息WM_COMMAND
		//处理对话框消息（当用户从菜单选中一个命令项目、当一个控件发送通知消息给去父窗口或者按下一个快捷键将发送 WM_COMMAND 消息）
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);//控件ID
            // 分析菜单选择: 
            switch (wmId)
            {
            case IDM_ABOUT:
				//从一个对话框资源中创建一个模态对话框
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
				//销毁指定的窗口
                DestroyWindow(hWnd);
                break;
            default:
				//默认时采用系统消息默认处理函数
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;

		//消息WM_PAINT
		//当需要绘制一部分应用窗口的时候，这个消息被Windows或者其他应用程序绘制调用
	case WM_PAINT:
        {
            PAINTSTRUCT ps;//PAINTSTRUCT 结构体
			 //为指定窗口进行绘图工作的准备
            HDC hdc = BeginPaint(hWnd, &ps);//HDC 标识设备环境句柄
            // TODO: 在此处添加使用 hdc 的任何绘图代码...
			//标记指定窗口的绘图结束
            EndPaint(hWnd, &ps);
        }
        break;

		//消息WM_DESTROY
		//窗口销毁后(调用DestroyWindow()后)，消息队列得到的消息
    case WM_DESTROY:
		//调用PostQuitMessage发出WM_QUIT消息
        PostQuitMessage(0);
        break;
    default:
		//默认时采用系统消息默认处理函数
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// “关于”框的消息处理程序。

	/*函数About
	回调函数，对话框消息

	*/
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
		//初始化对话框
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

		//处理对话框消息
    case WM_COMMAND:
		//关闭对话框 消除编辑框消息
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
			//结束对话框
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
