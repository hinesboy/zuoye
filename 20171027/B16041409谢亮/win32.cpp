// win32.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;// 当前实例 instance表示无符号的长整形，是32位的，是用于标示（记录）一个程序的实例。
TCHAR szTitle[MAX_LOADSTRING];// 标题栏文字 tchar是通过define定义的字符串宏
TCHAR szWindowClass[MAX_LOADSTRING];// 标题栏文字 

// Foward declarations of functions included in this code module:
//这个代码模块中包含的函数的前向声明 
ATOM				MyRegisterClass(HINSTANCE hInstance);//atoms表示无符号短整形
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	//初始化全局字符串 
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_WIN32, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	//执行应用程序初始化 
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_WIN32);

	// Main message loop:
	//主消息循环 
	while (GetMessage(&msg, NULL, 0, 0)) 
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) 
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.（注册窗口类） 
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it. 
//
ATOM MyRegisterClass(HINSTANCE hInstance)//注册窗口类 
{
	WNDCLASSEX wcex;//声明变量 

	wcex.cbSize = sizeof(WNDCLASSEX);//计算WNDCLASSEX的大小 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;//从这个窗口类派生的窗口具有的风格 
	wcex.lpfnWndProc	= (WNDPROC)WndProc;//窗口处理函数的指针 
	wcex.cbClsExtra		= 0;//指定紧跟在窗口类结构后放入附加字节数 
	wcex.cbWndExtra		= 0;//指定紧跟在窗口事例后的附加字节数 
	wcex.hInstance		= hInstance;//本模块的事例句柄
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_WIN32);//图标的句柄
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);//光标的句柄
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);//背景画刷的句柄
	wcex.lpszMenuName	= (LPCSTR)IDC_WIN32;//指向菜单的指针
	wcex.lpszClassName	= szWindowClass;//指向类名称的指针
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);//和窗口类关联的小图标

	return RegisterClassEx(&wcex);[D1] //注册窗口类
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window（保存实例句柄并创建主程序窗口） 
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.（将实例句柄保存在一个全局变量中，创建并显示主程序窗口） 
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable（在全局变量中存储实例句柄） 

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow); //显示窗口
   UpdateWindow(hWnd); //刷新窗口

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.（处理主窗口信息） 
//
//  WM_COMMAND	- process the application menu（处理应用程序菜单） 
//  WM_PAINT	- Paint the main window（绘制主窗口） 
//  WM_DESTROY	- post a quit message and return（发布退出消息并返回） 
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	TCHAR szHello[MAX_LOADSTRING];
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);

	switch (message) 
	{
		case WM_COMMAND:
			wmId    = LOWORD(wParam); 
			wmEvent = HIWORD(wParam); 
			// Parse the menu selections:（解析菜单选项） 
			switch (wmId)
			{
				case IDM_ABOUT:
				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
				   break;
				case IDM_EXIT:
				   DestroyWindow(hWnd);
				   break;
				default:
				   return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_PAINT:
			hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code here...
			RECT rt;
			GetClientRect(hWnd, &rt);
			DrawText(hdc, szHello, strlen(szHello), &rt, DT_CENTER);
			EndPaint(hWnd, &ps);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
   }
   return 0;
}

// Mesage handler for about box.（关于消息框的消息处理程序） 
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
				return TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
			{
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
}
