// win32.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance  句柄型数据类型，相当于装入了内存的资源的ID，无符号长整数
TCHAR szTitle[MAX_LOADSTRING];								// The title bar text   可用于描述 ANSI、DBCS 或 Unicode 字符串的 Win32 字符字符串。
TCHAR szWindowClass[MAX_LOADSTRING];								// The title bar text

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);          //atom 表是一个系统定义的表, 它存储字符串和相应的标识符。应用程序将字符串放在 atom 表中, 并接收一个称为atom的16位整数, 它可用于访问该字符串。
BOOL				InitInstance(HINSTANCE, int);                  //如果初始化成功, 则为非零;否则0。
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);           //一个应用程序定义的函数, 用于处理发送到窗口的消息。WNDPROC类型定义指向此回调函数的指针。
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY WinMain(HINSTANCE hInstance,                         //每个 Windows 程序都包含一个名为WinMain或wWinMain的入口点函数
                     HINSTANCE hPrevInstance,                     //hInstance是一种称为 "实例句柄" 或 "模块句柄" 的东西。操作系统在加载内存时使用此值来标识可执行文件 (EXE)。某些 Windows 函数 (例如, 加载图标或位图) 需要实例句柄。
                     LPSTR     lpCmdLine,                         //hPrevInstance没有任何意义。它在16位窗口中使用, 但现在始终为零。
                     int       nCmdShow)						  //nCmdShow是一个标志, 它表示主应用程序窗口是否将被最小化、最大化或正常显示。
{
 	// TODO: Place code here.
	MSG msg;                        //包含线程消息队列中的消息信息。
	HACCEL hAccelTable;             //加速键资源

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);    
	LoadString(hInstance, IDC_WIN32, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);           //在对 CreateWindow 或 CreateWindowEx 函数的调用中注册一个用于后续使用的窗口类。

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_WIN32);

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))        //从调用线程的消息队列中检索消息。该函数调度传入的已发送消息, 直到已发布的消息可用于检索。
	{                                           //参数各自意义：指向从线程的消息队列接收消息信息的 MSG 结构的指针；要检索其消息的窗口的句柄，该窗口必须属于当前线程；要检索的最低消息值的整数值；要检索的最高消息值的整数值。如果wMsgFilterMin和wMsgFilterMax都为零, GetMessage将返回所有可用消息 (即不执行范围筛选)。
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))  //该函数将 WM_KEYDOWN 或 WM_SYSKEYDOWN 消息转换为 WM_COMMAND 或 WM_SYSCOMMAND 消息 (如果在指定的加速器表), 然后将WM_COMMAND或WM_SYSCOMMAND消息直接发送到指定的窗口过程。TranslateAccelerator在窗口过程处理消息之前不返回。
		{
			TranslateMessage(&msg);             //将虚拟键消息转换为字符消息。将字符消息发送到调用线程的消息队列, 以便在下次线程调用 GetMessage 或 PeekMessage 函数时进行读取。
			DispatchMessage(&msg);				//将消息分派给窗口过程。它通常用于发送由 GetMessage 函数检索的消息。
		}
	}

	return msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;                    //包含窗口类信息。它与 RegisterClassEx 和 GetClassInfoEx 函数一起使用。WNDCLASSEX结构与 WNDCLASS 结构类似。有两种不同。WNDCLASSEX包括cbSize成员, 它指定结构的大小和hIconSm成员, 其中包含与窗口类关联的小图标的句柄。

	wcex.cbSize = sizeof(WNDCLASSEX);               //此结构的大小 (以字节为单位)。将此成员设置为sizeof(WNDCLASSEX)。确保在调用 GetClassInfoEx 函数之前设置此成员。unsigned int类型

	wcex.style			= CS_HREDRAW | CS_VREDRAW;  //类样式。 unsigned int类型
	wcex.lpfnWndProc	= (WNDPROC)WndProc;         //指向窗口过程的指针。必须使用 CallWindowProc 函数调用窗口过程。WNDPROC类型
	wcex.cbClsExtra		= 0;                        //要在窗口类结构之后分配的额外字节数。系统将字节初始化为零。int类型
	wcex.cbWndExtra		= 0;                        //在窗口实例之后分配的额外字节数。系统将字节初始化为零。int类型
	wcex.hInstance		= hInstance;				//包含类的窗口过程的实例的句柄。HINSTANCE类型
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_WIN32);		//类图标的句柄。此成员必须是图标资源的句柄。如果此成员为NULL, 系统将提供默认图标。hIcon类型
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);			//类游标的句柄。此成员必须是游标资源的句柄。hCursor类型
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);				//类背景画笔的句柄。此成员可以是画笔的句柄, 用于绘制背景, 或者可以是颜色值。颜色值必须是下列标准系统颜色之一 (必须将值1添加到所选颜色中)。
	wcex.lpszMenuName	= (LPCSTR)IDC_WIN32;					//指向一个以 null 结尾的字符串的指针, 它指定类菜单的资源名称, 因为该名称显示在资源文件中。如果使用整数来标识菜单, 请使用 MAKEINTRESOURCE 宏。如果此成员为NULL, 则属于此类的窗口没有默认菜单。
	wcex.lpszClassName	= szWindowClass;						//指向以 null 结尾的字符串的指针, 或者是一个原子。lpszClassName的最大长度为256。如果lpszClassName大于最大长度, RegisterClassEx 函数将失败。
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);		//与窗口类关联的小图标的句柄。如果此成员为NULL, 系统将搜索由hIcon成员指定的图标资源, 以查找适当大小的图标作为小图标。

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,			//创建重叠的、弹出的或子窗口。它指定窗口类、窗口标题、窗口样式和 (可选) 窗口的初始位置和大小。该函数还指定窗口的父级或所有者 (如果有) 和窗口的菜单。
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);			//设置指定窗口的显示状态。
   UpdateWindow(hWnd);					//如果窗口的更新区域不为空, UpdateWindow函数将通过向窗口发送 WM_PAINT 消息来更新指定窗口的工作区。该函数会绕过应用程序队列, 将WM_PAINT消息直接发送到指定窗口的窗口过程。如果更新区域为空, 则不发送任何消息。

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;			//PAINTSTRUCT结构包含应用程序的信息。此信息可用于绘制该应用程序所拥有的窗口的客户端区域。
	HDC hdc;				//返回由 Microsoft Windows 操作环境向对象的设备上下文提供的句柄。
	TCHAR szHello[MAX_LOADSTRING];
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);		//从与指定模块关联的可执行文件中加载字符串资源, 将该字符串复制到缓冲区中, 并追加一个终止 null 字符。

	switch (message) 
	{
		case WM_COMMAND:			//当用户从菜单中选择命令项时, 当控件向其父窗口发送通知消息时, 或者在转换加速器击键时发送。#define WM_COMMAND  0x0111

			wmId    = LOWORD(wParam);		//从指定值中检索低序单词。
			wmEvent = HIWORD(wParam);		//从指定的32位值中检索高阶单词
			// Parse the menu selections:
			switch (wmId)
			{
				case IDM_ABOUT:		//菜单项：用于“帮助”->“关于”
				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
				   break;
				case IDM_EXIT:		//菜单项：用于“文件”->“退出”
				   DestroyWindow(hWnd);
				   break;
				default:
				   return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_PAINT:			//当系统或其他应用程序请求绘制应用程序窗口的一部分时, 将发送WM_PAINT消息。
			hdc = BeginPaint(hWnd, &ps);
			// TODO: Add any drawing code here...
			RECT rt;
			GetClientRect(hWnd, &rt);		//获取逻辑坐标系中窗口或控件大小和坐标的常用函数
			DrawText(hdc, szHello, strlen(szHello), &rt, DT_CENTER);
			EndPaint(hWnd, &ps);
			break;
		case WM_DESTROY:			//WM_NCDESTROY 留言通知窗口其非区域正在被销毁。 DestroyWindow 函数将WM_NCDESTROY消息发送到 WM_DESTROY 消息之后的窗口。 WM_DESTROY 用于释放与该窗口关联的已分配内存对象。
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
   }
   return 0;
}

// Mesage handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:			//在对话框显示之前立即发送到对话框过程。对话框过程通常使用此消息初始化控件并执行影响对话框外观的任何其他初始化任务。
				return TRUE;

		case WM_COMMAND:			////当用户从菜单中选择命令项时, 当控件向其父窗口发送通知消息时, 或者在转换加速器击键时发送。#define WM_COMMAND  0x0111
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
			{
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
}
