// win32.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"
#define MAX_LOADSTRING 100 //宏定义长度为100

// Global Variables:
HINSTANCE hInst;	//指向void的指针类型				// current instance
TCHAR szTitle[MAX_LOADSTRING];		//char类型数组		// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];//char类型数组		// The title bar text

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance); //ATOM unsigned short类型
BOOL				InitInstance(HINSTANCE, int);         //BOOL int类型
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);  //消息处理函数
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);    //LRESULT long类型

int APIENTRY WinMain(HINSTANCE hInstance,    //WinMain主函数 应用程序的入口点
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,    //指向字符串的长指针
                     int       nCmdShow)
{
 	// TODO: Place code here.
	MSG msg;   //tagMSG结构体
	HACCEL hAccelTable; //指向void的指针类型

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	     //LoadString函数：加载字符串复制到缓冲区中，并附加终止空字符。
	LoadString(hInstance, IDC_WIN32, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);//RegisterClass函数：注册一个窗口类，在函数调用中使用
	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) //应用初始化函数
		//返回值：如果初始化成功则返回非零值，否则返回零值。
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_WIN32);//调用快捷键表

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0)) //调用线程的消息队列里取得一个消息
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg)) //翻译快捷键表
//（把消息跟快捷键表里定义的按键进行比较，如果发现有快捷键，就会转换这个按键消息发送给窗口的消息处理函数）
		{
			TranslateMessage(&msg);//将虚拟键消息转换为字符消息
			DispatchMessage(&msg);//分发从GetMessage取得的消息传给窗体函数处理
		}
	}

	return msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)//注册一个窗口类，在函数调用中使用
{
	WNDCLASSEX wcex;//_WNDCLASSEXW结构体
	wcex.cbSize = sizeof(WNDCLASSEX); //unsigned long类型
	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	    //style为long类型；当窗口的大小变化时重绘整个窗口
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	    //窗口类结构中回调函数指针指向WndProc函数
	wcex.cbClsExtra		= 0;//int类型;指定紧跟在窗口类结构后的附加字节数 
	wcex.cbWndExtra		= 0;//int类型;指定紧跟在窗口实例后的附加字节数 
	wcex.hInstance		= hInstance;//指向void的指针类型;本模块的实例句柄 
	wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_WIN32);//从资源中载入图标
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);//载入光标资源
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	    //背景画刷的句柄;使用颜色的时候，必须把每个颜色宏+1，才能得到正确的颜色
	wcex.lpszMenuName	= (LPCSTR)IDC_WIN32;
	    //指向const char的指针类型;指向菜单的指针
	wcex.lpszClassName	= szWindowClass;    
	    //指向const char的指针类型;指向类名称的指针 
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);
	    //载入图标
	return RegisterClassEx(&wcex);//返回这个窗口类型的标识号，否则返回零。
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
//初始化在Windows下运行的应用程序的每个新实例
{
   HWND hWnd;//指向void的指针类型

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);
   //创建一个窗口。指定窗口类，窗口标题，窗口风格，以及窗口的初始位置及大小

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow); 
     //设定指定窗口的显示状态。参数hWnd窗口句柄，nCmdShow窗口显示样式。
   UpdateWindow(hWnd);//更新窗口

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//消息处理函数，对各种消息进行分析和处理
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;//tagPAINTSTRUCT结构体
	HDC hdc;       //指向void的指针类型
	TCHAR szHello[MAX_LOADSTRING];//char类型数组
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);
	     //加载字符串复制到缓冲区中

	switch (message) 
	{
		case WM_COMMAND:   //点击菜单、按钮、下拉列表框等控件时触发
			wmId    = LOWORD(wParam); //取wParam的低位字节
			wmEvent = HIWORD(wParam); //取wParam的高位字节
			// Parse the menu selections:
			switch (wmId)
			{
				case IDM_ABOUT:
				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
				   //从一个对话框资源中创建一个模态对话框
				   break;
				case IDM_EXIT:
				   DestroyWindow(hWnd);//销毁指定的窗口
				   break;
				default:
				   return DefWindowProc(hWnd, message, wParam, lParam);
				   //缺省消息处理函数，确保每一个消息得到处理
			}
			break;
		case WM_PAINT:
			hdc = BeginPaint(hWnd, &ps);
	    	  //为指定窗口进行绘图工作的准备，并将和绘图有关的信息填充到一个PAINTSTRUCT结构中
			  // TODO: Add any drawing code here...
			RECT rt;  //tagRECT结构体
			GetClientRect(hWnd, &rt);//获取RECT坐标系中窗口的大小和坐标
			DrawText(hdc, szHello, strlen(szHello), &rt, DT_CENTER);
			  //在指定的矩形里写入格式化文本
			EndPaint(hWnd, &ps);//绘图结束，释放绘图区
			break;
		case WM_DESTROY:
			PostQuitMessage(0);//终止应用程序
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);//缺省消息处理
   }
   return 0;
}

// Mesage handler for about box.
//对话框消息处理函数
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG://表明对话框及其所有子控件都创建完毕
				return TRUE;

		case WM_COMMAND: //点击窗口控件时触发
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
				//点击确定或取消按钮，收到两个命令IDOK或IDCANCEL
			{
				EndDialog(hDlg, LOWORD(wParam));
				//清除一个模态对话框,并使系统中止对对话框的任何处理
				return TRUE;
			}
			break;
	}
    return FALSE;
}
