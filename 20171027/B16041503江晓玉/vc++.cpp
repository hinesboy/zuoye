//vc++.cpp: 定义应用程序的入口点。
//

#include "stdafx.h"//包含stdafx.h头文件
#include "vc++.h"//包含vc++头文件

#define MAX_LOADSTRING 100//定义MAX_LOADSTRING为100

// 全局变量: 
HINSTANCE hInst;                                // 当前实例
WCHAR szTitle[MAX_LOADSTRING];                  // 标题栏文本
WCHAR szWindowClass[MAX_LOADSTRING];            // 主窗口类名

// 此代码模块中包含的函数的前向声明: 
ATOM                MyRegisterClass(HINSTANCE hInstance);//ATOM:(WORD)短整型数据  
BOOL                InitInstance(HINSTANCE, int);//BOOL:(int)整型数据 
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);//LRESULT:(long)32位有符号整数 CALLBACK:回调函数    
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);//INT_PTR:(int)整型数据  CALLBACK:回调函数       
//wWinMain所有Windows应用程序的入口
int APIENTRY wWinMain(_In_ HINSTANCE hInstance,//用程序当前实例句柄
                     _In_opt_ HINSTANCE hPrevInstance,//应用程序其他实例句柄
                     _In_ LPWSTR    lpCmdLine,//指向程序命令行参数的指针
                     _In_ int       nCmdShow)//应用程序开始执行时窗口显示方式的整数值标识
{
    UNREFERENCED_PARAMETER(hPrevInstance);//#define UNREFERENCED_PARAMETER(P)          (P)
    UNREFERENCED_PARAMETER(lpCmdLine);//#define UNREFERENCED_PARAMETER(P)          (P)

    // TODO: 在此放置代码。

    // 初始化全局字符串
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);//#define IDS_APP_TITLE	103	导入字符串资源
    LoadStringW(hInstance, IDC_VC, szWindowClass, MAX_LOADSTRING);//#define IDC_VC	109	导入字符串资源
    MyRegisterClass(hInstance);//调用注册窗口函数

    // 执行应用程序初始化: 
    if (!InitInstance (hInstance, nCmdShow))//调用初始化实例函数
    {
        return FALSE;//#define FALSE  0
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_VC));//导入主消息循环

    MSG msg;

    // 主消息循环: 
    while (GetMessage(&msg, nullptr, 0, 0))//GetMessage():从消息队列中读取一条消息，并将消息放在一个msg结构中
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))//
        {
            TranslateMessage(&msg);//TranslateMessage():将消息的虚拟键转换为字符信息
            DispatchMessage(&msg);//DispatchMessage():将参数指向的消息传送带指定的窗口函数
        }
    }

    return (int) msg.wParam;
}



//
//  函数: MyRegisterClass()
//
//  目的: 注册窗口类。
//
ATOM MyRegisterClass(HINSTANCE hInstance)//MyRegisterClass函数用于注册该窗口应用程序
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;//设置窗口样式
    wcex.lpfnWndProc    = WndProc;//回调函数
    wcex.cbClsExtra     = 0;//窗口类无扩展
    wcex.cbWndExtra     = 0;//窗口实例无扩展
    wcex.hInstance      = hInstance;//指定当前应用程序的实例句柄
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_VC));//窗口的图标为默认图标
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);//窗口采用箭头光标
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);//指定窗口类的背景画刷句柄
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_VC);//指定菜单资源的名字
    wcex.lpszClassName  = szWindowClass;//指定窗口类名为“窗口示例”
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));//标识某个窗口最小化时显示的图标

    return RegisterClassExW(&wcex);//调用RegisterClassEx()函数向系统注册窗口类 
}

//
//   函数: InitInstance(HINSTANCE, int)
//
//   目的: 保存实例句柄并创建主窗口
//
//   注释: 
//
//        在此函数中，我们在全局变量中保存实例句柄并
//        创建和显示主程序窗口。
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // 将实例句柄存储在全局变量中

   HWND hWnd = CreateWindowW(//创建窗口
	   szWindowClass, //窗口类名
	   szTitle,//窗口实例的标题名
	   WS_OVERLAPPEDWINDOW,//窗口的风格
      CW_USEDEFAULT, 0,//窗口左上角坐标为默认值
	   CW_USEDEFAULT, 0,//窗口的高和宽为默认值
	   nullptr, //此窗口无父窗口
	   nullptr,//此窗口无主菜单
	   hInstance,//创建此窗口应用程序的当前句柄
	   nullptr);//不使用该值

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);//显示窗口
   UpdateWindow(hWnd);//绘制用户区

   return TRUE;
}

//
//  函数: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  目的:    处理主窗口的消息。
//
//  WM_COMMAND  - 处理应用程序菜单
//  WM_PAINT    - 绘制主窗口
//  WM_DESTROY  - 发送退出消息并返回
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)//WndPro函数是处理系统及用户输入消息
{
    switch (message)
    {
    case WM_COMMAND://#define WM_COMMAND        0x0111
        {
            int wmId = LOWORD(wParam);//LOWORD取出无符号长整型参数的低16位
            // 分析菜单选择: 
            switch (wmId)
            {
            case IDM_ABOUT: //#define IDM_ABOUT	104
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT://#define IDM_EXIT  105
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: 在此处添加使用 hdc 的任何绘图代码...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// “关于”框的消息处理程序。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)// 是About对话框的对话框处理函数
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
