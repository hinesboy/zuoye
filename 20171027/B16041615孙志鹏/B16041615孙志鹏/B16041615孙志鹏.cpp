//B16041615孙志鹏.cpp: 定义应用程序的入口点。
//

#include "stdafx.h"
#include "resource.h"

#define MAX_LOADSTRING 100                      //此处为宏定义

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];								// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];								// The title bar text

																	// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);     //typedef WORD ATOM
BOOL				InitInstance(HINSTANCE, int);             
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);      //typedef CALLBACK _stdcall
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);        //typedef long LONG_PTR typedef LONG_PTR LRESULT

int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow)
{
	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings                                       //int WINAPI LoadString(HINSTANCE hInstance,UINT nID,LPTSTR lpBuffer,int nBufferMax)
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);     //Load a string from the executable file associated with a specified module,copy the string into a buffer and append a terminating null character
	LoadString(hInstance, IDC_B16041615, szWindowClass, MAX_LOADSTRING); //此处IDS_APP_TITLE，IDC_B16041615，MAX_LOADSTRING均为宏定义
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}
	                                                                       //HACCEL WINAPI LoadAccelerators(HINSTANCE hInstance,LPCTSTR lpTableName)
	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_B16041615);     //Load a specified accelerator table

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))          //bool WINAPI GetMessage(LPMSG lpMsg,HWND hWnd,UINT wMsgFilterMin,UINT wMsgFilterMax) Retrieve a message from the calling thread's message queue            
	{                                
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))   //int WINAPI TranslateAccelerator(HWND hWnd,HACCEL hAccTable,LPMSG lpMsg)  Translate a WM_KEYDOWN or WM_SYSKEYDOWN message to a WM_COMMAND or WM_SYSCOMMAND message.
		{
			TranslateMessage(&msg);               //bool API TranslateMessage(const MSG* lpMsg) Translate virtual-key messages into character message
			DispatchMessage(&msg);                //LRESULT WINAPI DispatchMessage(const MSG* lpMsg) Dispatch a message to a window procedure.
		}
	}

	return msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)             //typedef WORD ATOM 
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = (WNDPROC)WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, (LPCTSTR)IDI_B16041615);     //HICON LoadIcon(HINSTANCE hInstance,LPCTSTR lpIconName) Load the specified icon resource from the executable file associated with an application instance
	wcex.hCursor = LoadCursor(NULL, IDC_ARROW);                   //HCURSOR WINAPI LoadCursor(HINSTANCE hInstance,LPCTSTR lpCursorName) Load the specified cursor resource from the executable file associated with an application instance
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = (LPCWSTR)IDC_B16041615;
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);

	return RegisterClassEx(&wcex);               //ATOM WINAPI RegisterClassEx(const WNDCLASSEX* lpwcx) Register a window class for subsequent use in calls to the CreateWindow or CreateWindowEx function
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable

	hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,           //WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT均为宏定义
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);            //bool WINAPI ShowWindow(HWND hWnd,int nCmdShow) Set the specified window's show state 
	UpdateWindow(hWnd);                    //bool UpdateWindow(HWND hWnd) 

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	TCHAR szHello[MAX_LOADSTRING]=L"Hello World";                
	LoadString(hInst, IDS_HELLO, szHello, MAX_LOADSTRING);

	switch (message)
	{
	case WM_COMMAND:
		wmId = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);   //INI_PTR WINAPI DialogBox(HINSTANCE hInstance,LPCTSTR lpTemplate,HWND hWnd,DLGPROC lpDialogFunc) Create a modal dialog box from a dialog box template resource
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);                    //bool WINAPI DestroyWindow(HWND hWnd) Destroy the specified window
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);    //LRESULT WINAPI DefWindowProc(HWND hWnd,UINT Msg,WPARAM wParam,LPARAM lparam) call the default window procedure processing for any window messages that an application does not process  
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);                //HDC BeginPaint(HWND hWnd,PAINTSTRUCT* lpPaint) Prepare the specified window for painting and fills a PAINTSTRUCT structure with information about the painting
		// TODO: Add any drawing code here...
		RECT rt;
		GetClientRect(hWnd, &rt);
		DrawText(hdc, szHello, _tcsclen(szHello), &rt, DT_CENTER);     //int DrawText(HDC hDC,LPCTSTR lpchText,int nCount,LPRECT lpRect,UINT uFormat)
		EndPaint(hWnd, &ps);          //bool EndPaint(HWND hWnd,const PAINTSTRUCT* lpPaint) Mark the end of painting in the specified window
		break;
	case WM_DESTROY:
		PostQuitMessage(0);          //VOID WINAPI PostQuitMessage(int nExitCode) indiacte the system that a thread made a request to terminate
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);        
	}
	return 0;
}

// Mesage handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_INITDIALOG:
		return TRUE;

	case WM_COMMAND: 
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)          //#define IDOK 1    #define IDCANCEL 2  #define LOWORD(l)  ((WORD)(((DWORD_PTR)(l)) & 0xffff))
		{
			EndDialog(hDlg, LOWORD(wParam));                   //bool WINAPI EndDialog(HWND hDlg,INT_PTR nResult) Destroy a modal dialog box,causing the system to end any processing for the dialog box
			return TRUE;
		}
		break;
	}
	return FALSE;
}