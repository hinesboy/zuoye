/* ************************************
*《精通Windows API》
* 示例代码
* client.c
* 14.1 socket通信
**************************************/
/* 头文件 */
#include <stdio.h>
#include "winsock2.h"
/* 常量 */
#define RECV_BUFFER_SIZE 8192

/*************************************
* main
* 功能 socket通信客户端
**************************************/
void main(int argc, char* argv[])
{
	// 变量定义
	SOCKADDR_IN clientService;// 地址
	SOCKET ConnectSocket;// socket
	WSADATA wsaData;// 库，建立一个WSADATA结构
	LPVOID recvbuf;// 接收缓存
	int bytesSent;
	int bytesRecv = 0;
	char sendbuf[32] = "get information";// 默认发送的数据

	// 初始化socket库，	保存ws2_32.dll已经加载
	//调用WSAStartup函数, 连接应用程序与winsock.dll的第一个调用.
	//第一个参数是WINSOCK 版本号,表示使用WINSOCK2版本 
	//第二个参数是指向WSADATA的指针.该函数返回一个INT型值, 通过检查这个值来确定初始化是否成功
	//wsaData用来存储系统传回的关于WINSOCK的资料
	int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	//NO_ERROR是个宏定义，在包含的某个文件里被定义成了0
	if (iResult != NO_ERROR)
		//返回值不等与0,说明初始化失败
		printf("Error at WSAStartup()\n");
	// 创建socket，初始化监听套接字
	ConnectSocket = socket(AF_INET, // IPv4
		SOCK_STREAM, // 顺序的、可靠的、基于连接的、双向的数据流通信
		IPPROTO_TCP// 使用TCP协议
		);
	//把socket设置成无效套接字
	if (ConnectSocket == INVALID_SOCKET)
	{   //WSAGetLastError()指返回上次发生的网络错误
		printf("Error at socket(): %ld\n", WSAGetLastError());
		//中止Windows Sockets DLL的使用
		WSACleanup();
		return;
	}

	// 设置服务端的通信协议、IP地址、端口
	clientService.sin_family = AF_INET;
	clientService.sin_addr.s_addr = inet_addr( "127.0.0.1" );
	clientService.sin_port = htons( 10000 );

	// 连接到服务端
	if ( connect(
		ConnectSocket, // socket
		(SOCKADDR*) &clientService, // 地址
		sizeof(clientService) // 地址的大小
		) == SOCKET_ERROR)
	{   //WSAGetLastError()指返回上次发生的网络错误
		printf( "Failed to connect(%d)\n",WSAGetLastError() );
		//中止Windows Sockets DLL的使用
		WSACleanup();
		return;
	}
	// 准备发送数据
	// 如果输入参数是-d，那么发送的数据是“download file”否则是"get information"
	//lstrcmp的作用是比较两个字符串，不区分大小写
	if(argc ==2 && (!lstrcmp(argv[1], "-d")))
	{//sendbuf发送的数据大小为8k
		lstrcpyn(sendbuf, "download file", 32);
	}
	// 向服务端发送数据
	bytesSent = send( ConnectSocket, // socket
		sendbuf,// 发送的数据 
		lstrlen(sendbuf)+1,// 数据长度
		0 );// 无标志
	//发送失败，返回SOCKET_ERROR错误，应用程序可通过WSAGetLastError()获取相应错误代码
	if(bytesSent == SOCKET_ERROR)
	{
		//WSAGetLastError()指返回上次发生的网络错误
		printf( "send error (%d)\n", WSAGetLastError());
		//关闭套接字
		closesocket(ConnectSocket);
		return;
	}
	printf( "Bytes Sent: %ld\n", bytesSent );

	// 准备接收数据
	//HeapAlloc指内存分配
	//GetProcessHeap取得默认堆的句柄
	//RECV_BUFFER_SIZE 开辟了8192字节
	recvbuf = HeapAlloc(GetProcessHeap(), 0, RECV_BUFFER_SIZE);
	// 循环接收
	while( bytesRecv != SOCKET_ERROR )
	{
		//Sleep(50);
		//recv 正常返回接收的数据的字节数
		bytesRecv = recv( ConnectSocket, // socket
			recvbuf, // 接收数据缓存
			RECV_BUFFER_SIZE,// 缓存大小
			0 );// 无标志
		//接受到的数据为0
		if ( bytesRecv == 0 )
		{//输出关闭连接
			printf( "Connection Closed.\n");
			break;
		}
		// TODO，处理接收的数据，这里只简单的将收到的数据大小显示
		printf( "Bytes Recv: %ld\n", bytesRecv );
	}
	//释放空间
	HeapFree(GetProcessHeap(), 0, recvbuf);
	//中止Windows Sockets DLL的使用
	WSACleanup();
	return;
}