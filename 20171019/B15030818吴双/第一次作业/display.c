/* ************************************
*《精通Windows API》 
* 示例代码
* display.c
* 10.1  系统信息
**************************************/
#include <windows.h>
#include <stdio.h>

void main()
{
   BOOL fResult;
   int aMouseInfo[3];    // aMouseInfo[3]数组存放鼠标信息 
 
   fResult = GetSystemMetrics(SM_MOUSEPRESENT); //GetSystemMetrics函数只有一个参数，通过设置不同的标识符就可以获取系统分辨率、窗体显示区域的宽度和高度、滚动条的宽度和高度。 
 
   if (fResult == 0) 
      printf("No mouse installed.\n"); 
   else 
   { 
      printf("Mouse installed.\n");

      // 判定按钮有没有被交换 

      fResult = GetSystemMetrics(SM_SWAPBUTTON); 
 
      if (fResult == 0) 
         printf("Buttons not swapped.\n"); 
      else printf("Buttons swapped.\n");
 
      // 获取鼠标的移速和阈值 
 
      fResult = SystemParametersInfo(
         SPI_GETMOUSE,  // 获取鼠标信息 
         0,             // 鼠标没有被使用 
         &aMouseInfo,   // 保留鼠标信息 
         0);            // 鼠标没有被使用 

      if( fResult )
      { 
         printf("Speed: %d\n", aMouseInfo[2]);    //打印鼠标移速 
         printf("Threshold (x,y): %d,%d\n", 
            aMouseInfo[0], aMouseInfo[1]); //打印鼠标阈值 
      }
   } 
}
