/* ************************************
*《精通Windows API》
* 示例代码
* cur_mod_dir.c
* 4.3.4	获取当前目录、获取程序所在的目录、获取模块路径
**************************************/

/* 头文件　*/
#include <windows.h>
#include <stdio.h>

/* ************************************
* int main(void)
* 功能	演示使用设置获取当前路径
*		演示获取模块路径
**************************************/
//#define MAX_PATH          260
int main(void)
{
	//用于存储当前路径
	CHAR szCurrentDirectory[MAX_PATH];
	//用于存储模块路径
	CHAR szMoudlePath[MAX_PATH];
	//Kernel32文件名与句柄
	//句柄：一个4字字节长的数值，用于标志应用程序中不同的对象和同类对象中不同的实例
	//lpstr:长指针，指向一个字符串的32位指针，每个字符占1个字节。相当于char *
		//Ex1: LPSTR lpstrMsg = "I'm tired.";
		//Ex2: char strMsg[] = "I'm tired.";
		//LPSTR lpstrMsg = (LPSTR)strMsg;
	LPSTR szKernel32 = "kernel32.dll";
	//HMODULE 是代表应用程序载入的模块，win32系统下通常是被载入模块的线性地址。
	/*HWND是线程相关的，你可以通过HWND找到该窗口所属进程和线程

		Handle 是代表系统的内核对象，如文件句柄，线程句柄，进程句柄。
		系统对内核对象以链表的形式进行管理，载入到内存中的每一个内
		核对象都有一个线性地址，同时相对系统来说，在串列中有一个索引位置，这个索引位置就是内核对象的handle。

		HINSTANCE的本质是模块基地址，他仅仅在同一进程中才有意义，跨进程的HINSTANCE是没有意义

		HMODULE 是代表应用程序载入的模块，win32系统下通常是被载入模块的线性地址。

		HINSTANCE 在win32下与HMODULE是相同的东西(只有在16位windows上，二者有所不同).*/
	HMODULE hKernel32;
	//当前路径的长度，也用于判断获取是否成功
	DWORD dwCurDirPathLen;

	//获取进程当前目录
	dwCurDirPathLen =
		GetCurrentDirectory(MAX_PATH, szCurrentDirectory);
	if (dwCurDirPathLen == 0)
	{
		printf("获取当前目录错误。\n");
		return 0;
	}
	printf("进程当前目录为 %s \n", szCurrentDirectory);

	//将进程当前目录设置为“C:\”
	lstrcpy(szCurrentDirectory, "C:\\");//字符串拷贝，区别在于参数是长指针类型
	if (!SetCurrentDirectory(szCurrentDirectory))
	{
		printf("设置当前目录错误。\n");
		return 0;
	}
	printf("已经设置当前目录为 %s \n", szCurrentDirectory);

	//在当前目录下创建子目录“current_dir”
	//运行完成后C:盘下将出现文件夹“current_dir”
	CreateDirectory("current_dir", NULL);

	//再次获取系统当前目录
	dwCurDirPathLen =
		GetCurrentDirectory(MAX_PATH, szCurrentDirectory);
	if (dwCurDirPathLen == 0)
	{
		printf("获取当前目录错误。\n");
		return 0;
	}
	printf("GetCurrentDirectory获取当前目录为 %s \n",
		szCurrentDirectory);

	//使用NULL参数，获取本模块的路径。
	if (!GetModuleFileName(NULL, szMoudlePath, MAX_PATH))
	{
		printf("获取模块路径录错误。\n");
		return 0;
	}
	printf("本模块路径 %s \n", szMoudlePath);

	//获取Kernel32.dll的模块句柄。
	//显式链接kernel32.dll
	hKernel32 = LoadLibrary(szKernel32);

	//使用Kernel32.dll的模块句柄，获取其路径。
	if (!GetModuleFileName(hKernel32, szMoudlePath, MAX_PATH))
	{
		printf("获取模块路径错误。\n");
		return 0;
	}
	printf("kernel32模块路径 %s \n", szMoudlePath);

	system("pause");
	return 0;

}