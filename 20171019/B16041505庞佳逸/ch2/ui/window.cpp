/* ************************************
*《精通Windows API》 
* 示例代码
* window.cpp
* 2.2  Windows API的功能分类
**************************************/


#include <windows.h>

HINSTANCE hinst; // 对象句柄

//WinMain			
//FUNCTION		Windows应用程序的入口点
//#define WINAPI __stdcall  采用__stdcal约定时，函数参数按照从右到左的顺序入栈，被调用的函数在返回前清理传送参数的栈
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int); 

// MainWndProc
// FUNCTION		窗口消息处理函数
// LRESULT	-	long 消息处理结果
// CALLBACK -	__stdcall 控制参数压栈顺序
// HWND		-	窗口句柄
// UNIT		-	unsigned int 消息
// WPARAM	-	unsigned int 附加信息
// LPARAM	-	long 附加信息
LRESULT CALLBACK MainWndProc(HWND, UINT, WPARAM, LPARAM); 


int WINAPI WinMain(HINSTANCE hinstance, // 对象句柄
                   HINSTANCE hPrevInstance, 
                   LPSTR lpCmdLine, // 窄字符串
                   int nCmdShow) 
{     
    WNDCLASSEX wcx;         //　窗口类
    HWND hwnd;              //  窗口句柄     
    MSG msg;                //　消息
    BOOL fGotMessage;       //　是否成功获取消息
    hinst = hinstance;      //  应用程序实例句柄，保存为全局变量

    // 填充窗口类的数据结构
    wcx.cbSize = sizeof(wcx);          // 结构体的大小
    wcx.style = CS_HREDRAW | 
        CS_VREDRAW;                    // 样式：大小改变时重绘界面 
    wcx.lpfnWndProc = MainWndProc;     // 窗口消息处理函数 
    wcx.cbClsExtra = 0;                // 不使用类内存
    wcx.cbWndExtra = 0;                // 不使用窗口内存 
    wcx.hInstance = hinstance;         // 所属的应用程序实例句柄 
    wcx.hIcon = LoadIcon(NULL, 
        IDI_APPLICATION);              // 图标：默认
    wcx.hCursor = LoadCursor(NULL, 
        IDC_ARROW);                    // 光标：默认
    wcx.hbrBackground = (HBRUSH)GetStockObject( 
        WHITE_BRUSH);                  // 背景：白色
    wcx.lpszMenuName =  NULL;          // 菜单：不使用
    wcx.lpszClassName = "MainWClass";  // 窗口类名 
    wcx.hIconSm = (HICON)LoadImage(hinstance, // 小图标
        MAKEINTRESOURCE(5),
        IMAGE_ICON, 
        GetSystemMetrics(SM_CXSMICON), //获取系统配置设置 Retrieves the specified system metric or system configuration setting.
        GetSystemMetrics(SM_CYSMICON), //SM_CYSMICON 图标
        LR_DEFAULTCOLOR);			   //LR_DEFAULTCOLOR 默认颜色

    // 注册窗口类
    if(!RegisterClassEx(&wcx))
    {
        return 1;
    }

    // 创建窗口 
    hwnd = CreateWindow( 
        "MainWClass",        // 窗口名
        "CH 2-3",            // 窗口标题 
        WS_OVERLAPPEDWINDOW, // 窗口样式  
        CW_USEDEFAULT,       // 水平位置X：默认 
        CW_USEDEFAULT,       // 垂直位置Y：默认
        CW_USEDEFAULT,       // 宽度：默认
        CW_USEDEFAULT,       // 高度：默认 
        (HWND) NULL,         // 父窗口：无 
        (HMENU) NULL,        // 菜单：使用窗口类的菜单 
        hinstance,           // 应用程序实例句柄 
        (LPVOID) NULL);      // 窗口创建时数据：无 

    if (!hwnd) 
    {
        return 1; 
    }

    // 显示窗口  Sets the specified window's show state.
    ShowWindow(hwnd, nCmdShow); 
	// 更新窗口	 Sets the specified window's show state.
    UpdateWindow(hwnd); 
    
	while (
        (fGotMessage = GetMessage(&msg, (HWND) NULL, 0, 0)) != 0 
        && fGotMessage != -1) //从消息队列中取得消息 Retrieves a message from the calling thread's message queue.
    { 
		TranslateMessage(&msg);	// 把键盘消息翻译成为字符消息 
        DispatchMessage(&msg); // 将消息分发给窗口过程 Dispatches a message to a window procedure.
    } 
    return msg.wParam; // 整型变量，附加信息

} 

LRESULT CALLBACK MainWndProc(HWND hwnd,
                             UINT uMsg,
                             WPARAM wParam,
                             LPARAM lParam
                             )
{
    switch (uMsg) 
    { 
        case WM_DESTROY: // #define WM_DESTROY 0x0002 窗口被销毁后得到的信息
            ExitThread(0); // 退出线程
            return 0; 
        default: 
            return DefWindowProc(hwnd, uMsg, wParam, lParam); // 调用默认窗口过程 处理任何未被应用程序处理的窗口消息 Calls the default window procedure to provide default processing for any window messages that an application does not process 
    } 
}