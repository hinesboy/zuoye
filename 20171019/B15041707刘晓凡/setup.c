/* ************************************
*《精通Windows API》
* 示例代码
* setup.c
* 15.3 setup.exe
**************************************/
/* 头文件 */
#include <Windows.h>
#include <Setupapi.h>
#include <shlobj.h>
/* 库 */
#pragma comment (lib, "shell32.lib")
#pragma comment (lib, "Setupapi.lib")

/*************************************
* VOID GetSourceDirectory(LPSTR szPath)
* 功能 获得当前路径
* szPath，返回路径
**************************************/
VOID GetSourceDirectory(LPSTR szPath)                              //传的参数是char类型的指针
{
	int i;                                                        //定义一个int变量

	GetModuleFileName(NULL,szPath,MAX_PATH);                     //宏定义，获得执行程序的绝对路径

	i=strlen(szPath);                                           //获得路径的长度
	while ((i>0)&&(szPath[i-1]!='\\'))                         //如果长度大于0，且绝对路径中执行程序的名称没有去掉
	{
		szPath[--i]=0;                                        //长度先-1,再把szPath[--i]置零
	}
}

/*************************************
* WinMain
* 功能 调用相关Setup API进行安装
**************************************/
INT WinMain(
			HINSTANCE hInstance,                           //定义一个结构体类型的指针
			HINSTANCE hPrevInstance,                       //定义一个结构体类型的指针
			LPSTR lpCmdLine,                              //定义一个指向char类型的指针
			int nCmdShow                                  //定义一个int类型的变量
			)
{
	HINF hInf;                                           // INF文件句柄，定义一个指向void类型的指针
	CHAR szSrcPath[MAX_PATH];                            // 源路径，定义一个char类型的数组
	CHAR szDisPath[MAX_PATH];                            // 目的路径，定义一个char类型的数组
	BOOL bResult;                                       // 定义一个int类型的变量
	PVOID pContext;                                     //定义一个指向void类型的指针
	// 与本程序在同一目录下的Setup.inf
	GetSourceDirectory(szSrcPath);                     //调用获得当前路径的函数,此函数用的宏定义
	lstrcat(szSrcPath,"setup.inf");                   //宏定义，查找同一目录下的文件
	// 打开 inf 文件
	hInf = SetupOpenInfFile(szSrcPath, NULL, INF_STYLE_WIN4, NULL);
	//传参：filename,infclass,infstyle,errorline
	// 是否成功
	if (hInf == INVALID_HANDLE_VALUE)//如果符合
	{
		MessageBox(NULL,                              //参数：第一个参数hWnd是结构体类型的变量，
			"Error: Could not open the INF file.",   //第二个lpText是字符型的指针
			"ERROR",                                //第三个lpCaption是字符型的指针
			MB_OK|MB_ICONERROR);                    //第四个 uType是int类型
		return FALSE;                              //表示不成功
	}
	// 获得Program Files的路径
	SHGetSpecialFolderPath(NULL,                  //参数：第一个参数结构体类型的变量，
		szDisPath, CSIDL_PROGRAM_FILES , FALSE);  //第二个是字符型的指针,第三个是int型参数，第四个是int类型
	// 构造目的路径
	lstrcat(szDisPath,"\\MyInstall");

	// 给inf配置文件中的路径ID赋值，使用路径替换路径ID
	bResult = SetupSetDirectoryId(hInf, 32768, szDisPath);
	//参数：1.InfHandle指向void类型的指针，2.id是long类型的参数，3.Directory是char类型的指针
	if (!bResult)//如果没有配置成功
	{
		MessageBox(NULL,
			"Error: Could not associate a directory ID with the destination directory.",
			"ERROR",
			MB_OK|MB_ICONERROR); 
		SetupCloseInfFile(hInf);                                  //关闭句柄文件，参数InfHandle：void类型的指针
		return FALSE;                                            //表示匹配不成功
	}

	// 设置默认callback函数的参数
	pContext=SetupInitDefaultQueueCallback(NULL);              //参数：结构体类型的指针变量

	// 进行安装
	bResult=SetupInstallFromInfSection(
		NULL,                                                // 父窗口句柄，Owner，结构体类型的指针变量
		hInf,                                               // INF文件句柄，InfHandle，指向void类型的指针
		"Install",                                         // INF文件中，配置了安装信息的节名，SectionName，char类型的指针
		SPINST_FILES | SPINST_REGISTRY ,                  // 安装标志，Flags，源于Int类型
		NULL,                                            // 安装键值，RelativeKeyRoot
		NULL,                                           // 源文件和路径，可以在INF文件中配置，SourceRootPath，
		0,                                             // 复制时的动作，CopyFlags，
		(PSP_FILE_CALLBACK)SetupDefaultQueueCallback, // 回调函数MsgHandler，
		pContext,                                    // 回调函数的参数，Context，
		NULL,                                       // 设备安装信息，DeviceInfoSet
		NULL                                       // 设备数据信息，DeviceInfoData
		);
	// 安装是否成功
	if (!bResult)
	{
		// 失败，输出错误信息
		MessageBox(NULL,
			"SetupInstallFromInfSection",
			"ERROR",
			MB_OK|MB_ICONERROR);
		//关闭
		SetupTermDefaultQueueCallback(pContext);    //关闭回调函数，参数：void类型的指针
		SetupCloseInfFile(hInf);                   //关闭句柄文件,参数：void类型的指针
		return FALSE;                             //操作失败
	}

	// 关闭
	SetupTermDefaultQueueCallback(pContext);    //关闭回调函数
	SetupCloseInfFile(hInf);                    //关闭句柄文件

	return TRUE;//操作成功
}