/* ************************************
*《精通Windows API》
* 示例代码
* ioctl.c
* 16.2 IO控制、内核通信
* B15041716 董慧
**************************************/
/* 头文件 */
/*
Windows.h是Windows 应用程序开发中常用的头文件，在Windows应用程序开发中所使用的很多的
数据类型、结构、API接口函数都在Windows.h 或 Windows.h 所包含的其他头文件中进行了声明
*/
#include <Windows.h>
//定义了设备控制函数需要用到的控制码
#include <Winioctl.h>
#include <stdio.h>
/* 函数声明 */
//数据类型 LPSTR 是 Windows API 中常用的字符串类型。 
//DWORD 用于表示无符号整型的数
DWORD EnjectCdrom(LPSTR szCdRomName);//弹出光盘
DWORD PrintNTFSInfo(LPSTR szVolumeName);//显示NTFS文件系统信息

/*************************************
* main
* 功能	-cdrom <盘符> 弹出光盘
*			-ntfs <盘符> 显示nfts分区的信息
**************************************/
//argc记录了用户在运行程序的命令行中输入的参数的个数.argv[]通常指向程序中的可执行文件的文件名。 
int main(int argc, char* argv[])
{
	CHAR szName[64];
	if(argc == 3)
	{
		// 构造设备名
		/*
		wsprintf作用：格式化字符.wsprintf(缓冲区,格式，要格式化的值)；第一个参数是字符缓冲区，后面是格式字符串，
		wsprintf不是将格式化结果写到标准输出，而是将其写入缓冲区中，该函数返回该字符串的长度。
		*/
		wsprintf(szName, "\\\\.\\%s.", argv[2]);
		/*
		int lstrcmp(LPCTSTR lpString1,LPCTSTR lpString2);
		参数解释：lpString1是以空为结束的字符串，lpString2是以空为结束的字符串
		返回值：返回整数类型的字符串比较结果，如果小于零，前者小于后者，如果等于零，两者相等，如果大于零，后者大于前者
		函数作用：比较两个字符串的大小，默认大小写有意义
		*/
		if(lstrcmp(argv[1],"-cdrom") == 0)
		{
			EnjectCdrom( szName );//弹出指定的光盘
			return 0;
		}
		// 获取NTFS分区详细信息
		if(lstrcmp(argv[1],"-ntfs") == 0)
		{
			PrintNTFSInfo( szName );//显示指定的NTFS驱动器信息
			return 0;
		}
	}
	// 使用方法
	//volume:卷
	printf("usage:  \n\t %s -cdrom <volume>\n\t %s -ntfs <volume>\nlike this: \n\t -cdrom G:", 
		argv[0], argv[0]);
	return 0;
}

/*************************************
* DWORD EnjectCdrom(LPSTR szCdRomName)
* 功能	弹出指定的光盘
* 参数	szCdRomName，设备名
**************************************/
DWORD EnjectCdrom(LPSTR szCdRomName)
{
	HANDLE hDevice;// HANDLE 类型实质上是无类型指针 void，定义设备变量
	DWORD dwBytesReturned;//定义字节返回变量
	//CreateFile是一个多功能的函数，可打开或创建以下对象，并返回可访问的句柄：控制台，通信资源，目录（只读打开），磁盘驱动器，文件等。
	hDevice = CreateFile(szCdRomName, // 设备名(LPCTSTR lpFileName,普通文件名或者设备文件名)
		GENERIC_ALL, // 存取权限(DWORD dwDesiredAccess,访问模式（写/读）)
		FILE_SHARE_READ|FILE_SHARE_WRITE|FILE_SHARE_DELETE, // 共享方式(DWORD dwShareMode,共享模式)
		NULL, // 默认安全属性(LPSECURITY_ATTRIBUTES lpSecurityAttributes,指向安全属性的指针)
		OPEN_EXISTING,//打开一个文件或设备，只有当它存在时。(DWORD dwCreationDisposition,如何创建)
		FILE_ATTRIBUTE_NORMAL, //默认属性。该文件没有设置其他的属性。此属性仅在单独使用时有效。(DWORD dwFlagsAndAttributes,文件属性)
		NULL); //(HANDLE hTemplateFile,用于复制文件句柄)
	//INVALID_HANDLE_VALUE表示无效的句柄值，类似于指针里的NULL。INVALID_HANDLE_VALUE表示出错，会设置GetLastError。
	if (hDevice == INVALID_HANDLE_VALUE)
	{
		printf("Could not open file (error %d)\n", GetLastError());
		return 0;
	}
	// 发送IOCTL
	//DeviceIoControl是直接发送控制代码到指定的设备驱动程序，使相应的移动设备以执行相应的操作的函数
	if(!DeviceIoControl(
		(HANDLE) hDevice, // 设备句柄（CreateFile返回的设备句柄）
		IOCTL_STORAGE_EJECT_MEDIA, // 控制码,可以弹出光驱(应用程序调用驱动程序的控制命令，就是IOCTL_XXX IOCTLs。)
		NULL, // 输入缓存(lpInBuffer Any，应用程序传递给驱动程序的数据缓冲区地址。)
		0, // 输入缓存大小(nInBufferSize Long，应用程序传递给驱动程序的数据缓冲区大小，字节数。)
		NULL, // 输出缓存(lpOutBuffer Any，驱动程序返回给应用程序的数据缓冲区地址。)
		0, // 输出缓存大小(nOutBufferSize Long，驱动程序返回给应用程序的数据缓冲区大小，字节数。)
		&dwBytesReturned, // 实际需要的输输入缓存大小(lpBytesReturned Long，驱动程序实际返回给应用程序的数据字节数地址。)
		NULL // 非OVERLAPPED(lpOverlapped OVERLAPPED，这个结构用于重叠操作。针对同步操作，请用ByVal As Long传递零值)
		))
	{
		printf("DeviceIoControl error (%d)",GetLastError());
		return 0;
	}
	return 1;
}

/*************************************
* DWORD PrintNTFSInfo(LPSTR szVolumeName)
* 功能	获取显示指定的NTFS驱动器信息
* 参数	szVolumeName，设备名
**************************************/
DWORD PrintNTFSInfo(LPSTR szVolumeName)
{
	// FSCTL_GET_NTFS_VOLUME_DATA IO控制的返回值保存在NTFS_VOLUME_DATA_BUFFER结构中，NTFS卷标数据缓冲区
	NTFS_VOLUME_DATA_BUFFER nvdb;
	DWORD dwBufferSize;//缓冲区大小
	HANDLE hDevice;
	// 清空参数
	//ZeroMemory只是将指定的内存块清零
	/*
	声明
	void ZeroMemory( PVOID Destination,SIZE_T Length );
	参数
	Destination :指向一块准备用0来填充的内存区域的开始地址。
	Length :准备用0来填充的内存区域的大小，按字节来计算。
	*/
	ZeroMemory(&nvdb,sizeof(nvdb));

	hDevice = CreateFile(szVolumeName, 
		GENERIC_ALL, //存取权限
		FILE_SHARE_READ|FILE_SHARE_WRITE, // 共享方式
		NULL, // 默认安全属性
		OPEN_EXISTING, //打开一个文件或设备，只有当它存在时。(DWORD dwCreationDisposition,如何创建)
		FILE_ATTRIBUTE_NORMAL,  //默认属性。(DWORD dwFlagsAndAttributes,文件属性)
		NULL);  //(HANDLE hTemplateFile,用于复制文件句柄)
	if (hDevice == INVALID_HANDLE_VALUE)
	{
		printf("Could not open file (error %d)\n", GetLastError());
		return 0;
	}

	if(DeviceIoControl(
		hDevice, // 设备句柄
		FSCTL_GET_NTFS_VOLUME_DATA, // 控制码（该控制码用于返回指定元件的卷标信息。）
		NULL, // 输入缓存
		0, // 输入缓存大小
		&nvdb, // 输出缓存
		sizeof( NTFS_VOLUME_DATA_BUFFER ), // 输出缓存大小
		&dwBufferSize, // 返回的实际数据大小
		NULL // 非OVERLAPPED
		))
	{
		// 打印获取的信息
		printf("SerialNumber %lu\n",nvdb.VolumeSerialNumber);//显示硬盘卷标序列号
		printf("Starting logical cluster number of the master file table: %lu\n",nvdb.MftStartLcn);//启动主文件表的逻辑簇号：MFT开始簇
		printf("Length of the master file table: %lu\n",nvdb.MftValidDataLength);//主文件表长度：MFT有效数据长度
		printf("... ...\n");
	}
	else
	{
		printf("DeviceIoControl error: (%d)\n",GetLastError());
		
		return 0;
	}
	return 1;
}