/* ************************************
*《精通Windows API》 
* 示例代码
* windata.c
* 2.1.1	常用的Windows数据类型
**************************************/

/* 头文件　*/
#include <windows.h>
#include <stdio.h>

/* ************************************
* 功能	Windows 数据类型演示
**************************************/
int WINAPI WinMain(
            HINSTANCE hInstance,
            HINSTANCE hPrevInstance,
            LPSTR lpCmdLine,
            int nCmdShow
            )
/* WINAPI 宏定义 __stdcall类型
   WinMain(hInstance 当前实例句柄,hPrevInstance 前一个实例句柄,lpCmdLine 命令行,nCmdShow 控制窗口的显示方式) Windows函数 win32应用程序入口
   HINSTANCE 宏定义 HMODULE 类型定义 void*类型
   LPSTR 类型定义 _Null_terminated_类型 */
{
    //定义字符串
    LPSTR szString = "Windows data type, string.";
    //定义字符数组
    CHAR lpString[120];//要大于szString的长度
    //定义DWORD类型的数据
    DWORD dwMax = 0xFFFFFFFF;
    DWORD dwOne = 0x1;
    //定义INT类型的数据
    INT iMax = 0xFFFFFFFF;
    INT iOne = 0x1;
/* DWORD 类型定义 unsigned long类型
   INT 类型定义 int类型 */ 
    //显示字符串
    MessageBox(NULL,szString,"LPSTR",MB_OK);
    //复制内存，将字符串复制到数组中（包括NULL结束符）
    CopyMemory(lpString, szString,lstrlen(szString)+1);
    //显示复制的字符串
    MessageBox(NULL,lpString,"CHAR[]",MB_OK);

/* MessageBox(要创建的消息框的所有者窗口的句柄,要显示的信息,对话框标题,对话框类型) 宏定义 MessageA()/MessageW() 显示一个包含系统图标，一组按钮和一个简短的应用程序特定消息(如状态或错误信息)的模态对话框
   MB_OK 宏定义 0x00000000L
   CopyMemory(目的内存块,源内存块,要复制的目标的字节数) 宏定义 RltCopyMemory() 把源存储块的内容拷贝到目标存储块
   lstrlen(要检查的以null结尾的字符串) 宏定义 lstrlenA()/lstrlenW() Windows函数 确定指定字符串的长度（不包括终止空字符）*/

    //比较DWORD并显示结果
    if(dwMax>dwOne)
    {
        MessageBox(NULL,"DWORD类型的数据 OxFFFFFFFF > 0x1","DWORD",MB_OK);
    }
    //比较INT并显示结果
    if(iMax<iOne)
    {
        MessageBox(NULL,"INT类型的数据 OxFFFFFFFF < 0x1","INT",MB_OK);
    }
    return 0;
}