// StrcatPro.cpp: 定义控制台应用程序的入口点。
//
#include "stdafx.h"
#include <iostream>
using namespace std;

//连接字符串
char * strcat_s(char *, char *);
//获取字符串长度
int getLen(char *);

TEST(StrcatPro, strcat1)
{
	EXPECT_STREQ("abcdef", strcat_s("abc", "def"));
}
TEST(StrcatPro, strcat2)
{
	EXPECT_STREQ("abc", strcat_s("abc", ""));
}
TEST(StrcatPro, strcat3)
{
	EXPECT_STREQ("abc", strcat_s("abc", NULL));
}
TEST(StrcatPro, strcat4)
{
	EXPECT_STREQ("abc", strcat_s(NULL, "abc"));
}
TEST(StrcatPro, strcat5)
{
	EXPECT_STREQ("", strcat_s(NULL, NULL));
}

int main(int argc, char* argv[])
{
	char str1[10], str2[10];
	cout << "Please input two string:" << endl;
	cin >> str1;
	cin >> str2;
	cout<<strcat_s(str1,str2)<<endl;

	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();

	system("pause");
    return 0;

}

int getLen(char *str)
{
	int len = 0;//字符串长度
	while (str[len] != '\0')
	{
		len++;
	}
	return len;
}

char * strcat_s(char *str1, char *str2)
{
	int len1 = 0, len2 = 0;
	if (str1 == NULL && str2 == NULL)
	{
		return "";
	}
	if (str1 == NULL)
	{
		return str2;
	}
	if (str2 == NULL)
	{
		return str1;
	}
	len1 = getLen(str1);
	len2 = getLen(str2);
	char *dest = (char *)malloc(len1+len2+1);//申请空间
	for (int i = 0; i < len1; i++)
	{
		dest[i] = str1[i];//复制字符串
	}
	for (int i = 0; i < len2; i++)
	{
		dest[len1+i] = str2[i];//连接复制字符串
	}
	dest[len1 + len2] = '\0';//字符串结束
	return dest;
}



