实验心得：

- 要注意特判传入指针为NULL的情况，以及返回值为NULL的情况
- visual studio配置include、lib路径的时候要注意当前配置的事release还是debug，彼此不通用

结果截图：

![实验结果.png](./result.png)
