#include "stdafx.h"
#include "Strcat.h"
#include "gtest\gtest.h"

TEST(strcats, case1)
{
	/*���� strcat("abc", "def");
	���� strcat("abc", "");
	���� strcat("abc", NULL);
	���� strcat(NULL, "abc");
	���� strcat(NULL, NULL);*/
	EXPECT_STREQ("abcdef", strcats("abc", "def"));
	EXPECT_STREQ("abc", strcats("abc", ""));
	EXPECT_STREQ("abc", strcats("abc", NULL));
	EXPECT_STREQ("abc", strcats(NULL, "abc"));
	EXPECT_STREQ(NULL, strcats(NULL, NULL));
	system("pause");
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}