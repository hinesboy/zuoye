/* ************************************
*《精通Windows API》 
* 示例代码
* current_dir.c
* 4.3.4	获取当前目录、获取程序所在的目录、获取模块路径
* 2007年10月
**************************************/

/* 头文件　*/
#include <windows.h>
#include <stdio.h>

//wsprintf连接
#pragma comment (lib, "User32.lib")//杂注 评论（库，“User32库”） 这句话表示程序编译时要自动链接User32.lib，这样编译时不需要在命令行添加上User.lib


/* ************************************
* int main(void)
* 功能	演示使用设置获取当前路径
*		演示获取模块路径
*
* 参数	无
*
* 2007年10月
*
**************************************/
int AddStreamToFile(LPSTR szFilePath, 
					LPSTR szStreamName,
					PVOID pStreamData,
					DWORD dwStreamDataSize
					)//stream是组（也可能是和河流的意思）的意思；LPSTR是一种字符串数据类型（被定义成一个指向以NULL（\0）结尾的32位ANSI字符数组指针）；
					//PVOID==void*表示一个普通指针类型；DWORD是无符号的，代表unsigned long 四个字节；
{
	CHAR szStreamPath[MAX_PATH];
	HANDLE hStream;//HANDLE是句柄类型
	DWORD dwWritted;
	wsprintf(szStreamPath,"%s:%s",szFilePath,szStreamName);

	hStream = CreateFile(szStreamPath, 
		GENERIC_WRITE,//表示允许对设备进行写访问
		0, 
		NULL,
		CREATE_ALWAYS, //创建文件
		FILE_ATTRIBUTE_NORMAL, //默认属性
		NULL);

	if(hStream == INVALID_HANDLE_VALUE)//INVALID_HANDLE_VALUE表示无效的返回值，就是被微软定义为-1
	{
		printf("create file error: %d\n",GetLastError());
		return 1;
	}
	if(!WriteFile(hStream,pStreamData,dwStreamDataSize,&dwWritted,NULL))
	{
		printf("write file error: %d\n",GetLastError());
		CloseHandle(hStream);
		return 1;
	}
	printf("writted %d byte\n",dwWritted);
	CloseHandle(hStream);
	return 0;

}
int ReadStreamData(LPSTR szStreamPath, 
				   LPVOID lpDataBuffer,
				   DWORD dwBufferSize
				   )
{

	HANDLE hStream;
	DWORD dwRead;

	DWORD dwDataSize;


	hStream = CreateFile(szStreamPath, 
		GENERIC_READ,//有序设备进行读访问
		0, 
		NULL,
		OPEN_EXISTING, //文件必须已经存在，有设备提出要求
		FILE_ATTRIBUTE_NORMAL, //默认属性
		NULL);

	if(hStream == INVALID_HANDLE_VALUE)//INVALID_HANDLE_VALUE表示无效的返回值，就是被微软定义为-1
	{
		printf("open stream error: %d\n",GetLastError());
		return 1;
	}
	dwDataSize = GetFileSize(hStream,NULL);
	printf("stream data size is %d, is %s than buffer's\n",
		dwDataSize,(dwDataSize > dwBufferSize ? "larger":"not larger"));
	if(!ReadFile(hStream,lpDataBuffer,dwBufferSize,&dwRead,NULL))
	{
		printf("read file error: %d\n",GetLastError());
	}
	printf("read %d byte\n",dwRead);
	CloseHandle(hStream);
	return 0;

}
//DWROD EnumStreams2k3(LPWSTR szFilePath)
//{
//	WIN32_FIND_STREAM_DATA wfsd;
//	HANDLE hFind;
//	hFind = FindFirstStreamW(szFilePath,
//		FindStreamInfoStandard,
//		&wfsd,
//		0);
//	if(hFind == INVALID_HANDLE_VALUE)
//	{
//		printf("find stream error: %d",GetLastError());
//		return 1;
//	}
//	do
//	{
//
//	}
//	return 0;
//}

LPSTR szStreamData1 = "THIS IS A STREAM DATA";//LPSTR是一种字符串数据类型（被定义成一个指向以NULL（\0）结尾的32位ANSI字符数组指针）
BYTE lpStreamData2[10] = {0x00,0x01,0x02,0x03};//从字符串最左端开始，每两个字符组成一个Byte，存储到byte数组的合适位置

int main(void)
{
	DWORD i=0;//DWORD是无符号的，代表unsigned long 四个字节

	DWORD dwData1Len = lstrlen(szStreamData1);

	AddStreamToFile("test.txt","s1",szStreamData1,dwData1Len);
	AddStreamToFile("test.txt","s2",lpStreamData2,10);
	ZeroMemory(szStreamData1,dwData1Len);
	ZeroMemory(lpStreamData2,10);
	ReadStreamData("test.txt:s1",szStreamData1,dwData1Len);
	printf("read %d bytes form test.txt:s1: \"%s\"\n",
		dwData1Len, szStreamData1);
	ReadStreamData("test.txt:s2",lpStreamData2,10);
	printf("read 10 bytes form test.txt:s2: ");
	for(; i<10; i++)
	{
		printf("0x%#02d ",lpStreamData2[i]);
	}
	return 0;
}
