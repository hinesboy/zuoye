#include "strctl.h"
#include <assert.h>
#include <iostream>

int length(const char * str)
{
	
	int length = 0;
	if (str != NULL)
	{
		while (1)
		{
			if (*(str + length) != '\0')
				length++;
			else
				break;
		}
	}
	return length;
}

char * my_strcat (const char* str1, const char* str2)
{
	int length1 = length(str1);
	int length2 = length(str2);
	int length = length1 + length2 + 1;
	//std::cout << length << std::endl;
	char * p = new char[length];
	char * r = p;
	for (int i = 0; i < length1; i++)
	{
		*r = *(str1 + i);
		r++;
	}
	for (int j = 0; j < length2; j++)
	{
		*r = *(str2 + j);
		r++;
	}
	*r = 0;
	return p;
}