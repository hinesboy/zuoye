# 作业STRCAT

独立实现了获取字符串长度
独立实现了拼接字符串

区别与系统strcpy，函数另行申请空间，不修改原字符串
函数声明my_strcpy(const char * str1,const char * str2);
声明参数均为const，并重命名为my_strcpy以与基本库函数区分。

对于NULL作为空字符串处理，均为NULL则返回""