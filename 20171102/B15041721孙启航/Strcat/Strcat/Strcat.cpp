// Strcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <gtest/gtest.h>

int strlength(char* str) {
	int length = 0;
	while (str[length] != '\0')
	{
		length++;
	}

	return length;
}

char* strcat_(char* str1, char* str2) {
	if (str1 == NULL && str2 == NULL) {
		return NULL;
	}
	else if (str1 == NULL) {
		return str2;
	}
	else if (str2 == NULL) {
		return str1;
	}
	int str1_length = strlength(str1);
	int str2_length = strlength(str2);

	

	char* dest = (char *)malloc(str1_length+ str2_length+1);
	for (int i = 0; i < str1_length; i++)
	{
		dest[i] = str1[i];
	}
	for (int i = 0; i < str2_length; i++)
	{
		dest[str1_length+i] = str2[i];
	}

	dest[str1_length + str2_length] = '\0';

	return dest;

}
/*
int main()
{
	char *str_1 = "abc";
	char *str_2 = "defg";
	char *result = strcat(str_1, str_2);
	int len = strlength(result);
	cout << result << endl;

	system("pause");
    return 0;
}
*/

TEST(testCase, test0)
{
	
	char *str_1 = "";
	char *str_2 = "";
	EXPECT_STREQ("abcdef", strcat_("abc", "def"));
}

TEST(testCase, test1)
{
	EXPECT_STREQ("abc", strcat_("abc", ""));
}

TEST(testCase, test2)
{
	EXPECT_STREQ("abc", strcat_("abc", ""));
}
TEST(testCase, test3)
{
	EXPECT_STREQ("abc", strcat_(NULL, "abc"));
}
TEST(testCase, test4)
{
	EXPECT_STREQ("abc", strcat_("abc", NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}