// StringBuilder.cpp: 定义控制台应用程序的入口点。
#include "stdafx.h"
char *stringc(char *str1, char *str2)
{
	int length1 = 0;
	int length2 = 0;
	int i;
	int j;
	
	if (str1 == NULL &&str2 == NULL)
	{
		return NULL;
	}
	if (str1 == NULL)
	{
		return str2;
	}
	if (str2 == NULL)
	{
		return str1;
	}

	while (str1[length1] != '\0')
	{
		length1++;
	}
	while (str2[length2] != '\0')
	{
		length2++;
	}
	char *str3 = (char*)malloc(sizeof(char)*(length1 + length2 + 1));

	for (i = 0; i < length1; i++)
	{
		str3[i] = str1[i];
	}
	for (j = 0; j < length2; j++)
	{
		str3[j + length1] = str2[j];
	}
	str3[length1 + length2] = '\0';
	return str3;
}

TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", stringc("abc", "def"));
	EXPECT_STREQ("abc", stringc("abc", ""));
	EXPECT_STREQ("abc", stringc("abc", NULL));
	EXPECT_STREQ("abc", stringc(NULL, "abc"));
	EXPECT_STREQ(NULL, stringc(NULL, NULL));

}

int main(int argc, _TCHAR* argv[])
{
	char *s1 = "abc";
	char *s2 = "def";
	char *s3 = stringc(s1, s2);
	printf("字符串拼接结果: %s \n",s3);
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	system("pause");
	return 0;
}




