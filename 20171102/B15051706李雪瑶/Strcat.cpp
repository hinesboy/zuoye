// StringBuilder.cpp: 定义控制台应用程序的入口点。
#include "stdafx.h"
char *motage(char *str1, char *str2)
{
	int length1 = 0;
	int length2 = 0;
	int i;
	int j;
	if (str1 == NULL || str2 == NULL)
	{
		if (str1 == NULL &&str2 == NULL)
		{
			return NULL;
		}
		else if (str1 == NULL)
		{
			return str2;
		}
		else
		{
			return str1;
		}
	}
	while (str1[length1] != '\0')
	{
		length1++;
	}
	while (str2[length2] != '\0')
	{
		length2++;
	}
	char *str3 = (char*)malloc(sizeof(char)*(length1 + length2 + 1));

	for (i = 0; i < length1; i++)
	{
		str3[i] = str1[i];
	}
	for (j = 0; j < length2; j++)
	{
		str3[j + length1] = str2[j];
	}
	str3[length1 + length2] = '\0';
	return str3;
}

TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", motage("abc", "def"));
	EXPECT_STREQ("abc", motage("abc", ""));
	EXPECT_STREQ("abc", motage("abc", NULL));
	EXPECT_STREQ("abc", motage(NULL, "abc"));
	EXPECT_STREQ(NULL, motage(NULL, NULL));

}
int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

/*int main()
{
char *s1 = "abc";
char *s2 = "def";
char *s3 = strcat(s1, s2);
printf("字符串拼接结果: %s \n",s3);
return 0;
}
*/



