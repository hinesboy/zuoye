﻿助教讲解如何把作业提交到码云

上机内容

一、创建win32 console application应用程序 **strcat**


二、实现char* strcat(char* str1, char* str2)

提示：

分别计算str1和str2的长度，

申请内存空间，

逐个字符复制到申请的内存空间中，

字符串结束标记写入


三、用 **gtest** 来进行单元测试

1. 测试 strcat("abc","def");

2. 测试 strcat("abc","");

3. 测试 strcat("abc",NULL);

4. 测试 strcat(NULL,"abc");

5. 测试 strcat(NULL,NULL);

四、在自己文件夹下面撰写 **README.md** 文档

包括：把实验心得和实验结果截图放在该文档中

五、将strcat项目中的代码和测试代码提交到码云上作业日期的文件夹下

比如日期为20171102，学号为： B120416， 姓名为：张巧，则文件夹为 20171102/B120416张巧/
![输入图片说明](https://gitee.com/uploads/images/2017/1115/201914_f3ade18b_1589127.png "B16041629袁家乐.png")

实验心得:主要理解指针、字符串等的基本使用。关键在于如何很好的分析问题。