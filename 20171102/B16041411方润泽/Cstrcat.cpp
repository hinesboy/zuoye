// Cstrcat.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include<iostream.h>
#include<stdlib.h>
char *strcat(char* str1,char* str2)
{
	char *temp;
	int length1=0,length2=0,length3,i=0;
	if(str1)
	{
		while(str1[i++]!='\0')		
			length1++;	
			i=0;
	}
	if(str2)
	{
		while(str2[i++]!='\0')
			length2++;
			i++;
	}
	length3=length1+length2;
	temp=(char*)malloc(sizeof(char)*(length3+1));
	for(i=0;i<length1;i++)
		temp[i]=str1[i];
	for(i=0;i<length2;i++)
		temp[i+length1]=str2[i];
	temp[length3]='\0';
	return temp;
}
int main(int argc, char* argv[])
{
	char *str1;
	str1=strcat("abc"," ");
	cout<<str1;
	return 0;
}
