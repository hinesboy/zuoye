#include "stdafx.h"  
#include "strcat.h" 
#include "gtest/gtest.h"

TEST(strcat, strcat1) {
  EXPECT_STREQ("abcdef", strcat_s("abc","def"));
}
TEST(strcat, strcat2) {
	EXPECT_STREQ("abc", strcat_s("abc", ""));
}
TEST(strcat, strcat3) {
	EXPECT_STREQ("abc", strcat_s("abc", NULL));
}
TEST(strcat, strcat4) {
	EXPECT_STREQ("abc", strcat_s(NULL, "abc"));
}
TEST(strcat, strcat5) {
	EXPECT_STREQ(NULL, strcat_s(NULL, NULL));
}
int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
