
实验内容

一、创建win32 console application应用程序strcat

二、实现char* strcat(char* str1, char* str2)
提示：分别计算str1和str2的长度，申请内存空间，逐个字符复制到申请的内存空间中，字符串结束标记写入


三、用gtest来进行单元测试

1. 测试 strcat("abc","def");
2. 测试 strcat("abc","");
3. 测试 strcat("abc",NULL);
4. 测试 strcat(NULL,"abc");
5. 测试 strcat(NULL,NULL);

四、实验截图如下：

![输入图片说明](https://gitee.com/uploads/images/2017/1109/170515_9d079048_1585590.png "实验结果截图.png")

五、实验小结：

   通过本次实验，我学习到了如何实现两个字符串连接，首先计算两个字符串的长度，然后根据两个字符串的长度申请一个新的内存空间，
   接着逐个将字符复制到申请的内存空间中，最后写入字符串结束标志。我还掌握了gtest的用法。gtest刚开始研究时，确实遇到了许多
   问题，但在组长和同学的帮助下最后得以解决。需要注意的是EXPECT_STREQ和EXPECT_EQ的区别，前者是应用于字符串的，后者是应用
   于数字的。并且我还发现通过本次实验，自己学的东西实在是太少了，所以以后还是要更加努力的学习更多深入的知识。


