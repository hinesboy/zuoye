// PeiZhi.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"

TEST(FooTest, HandleNoneZeroInput)
{
	char s1[7] = { "people" };
	char s2[6] = { "human" };
	EXPECT_EQ("peoplehuman", strcat(s1, s2));
	getchar();
}


int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
