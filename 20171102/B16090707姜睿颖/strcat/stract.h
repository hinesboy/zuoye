#pragma once
#include "stdafx.h"
#ifndef _Stract_
#define _Stract_

char* mystrcat(char* str1, char* str2)
{
	int i1 = 0, j1 = 0, i2 = 0, j2 = 0;
	char* p;
	for (; str1&&str1[i1] != 0; i1++)
	{
		j1++;
	}
	for (; str2&&str2[i2] != 0; i2++)
	{
		j2++;
	}
	p = (char*)malloc((j1 + j2+1) * sizeof(char));
	for (i1 = 0; i1 < j1 + j2; i1++)
	{
		if (i1 < j1)
		{
			p[i1] = str1[i1];
		}
		else
		{
			p[i1] = str2[i1 - j1];
		}
	}
	p[i1] = '\0';
	return p;
}
#endif // !_Stract_

/*

三、用 **gtest** 来进行单元测试

1. 测试 strcat("abc","def");

2. 测试 strcat("abc","");

3. 测试 strcat("abc",NULL);

4. 测试 strcat(NULL,"abc");

5. 测试 strcat(NULL,NULL);
四、在自己文件夹下面撰写 **README.md** 文档

包括：把实验心得和实验结果截图放在该文档中
*/

