##作业心得

一、学会了如何在vs2017中创建win32 console application应用程序


二、同时自己实现char* mystrcat(char* str1, char* str2)的基本功能：
分别计算str1和str2的长度，申请内存空间，
逐个字符复制到申请的内存空间中，
字符串结束标记写入


三、我学会了用 **gtest** 来进行单元测试（使用EXPECT_STREQ(expected_str,actual_str);这一函数）

1. 测试 mystrcat("abc","def");
2. 测试 mystrcat("abc","");
3. 测试 mystrcat("abc",NULL);
4. 测试 mystrcat(NULL,"abc");
5. 测试 mystrcat(NULL,NULL);

且以上五个全部通过测试。

四、学会在自己文件夹下面用MARKDOWNEDITOR软件撰写 **README.md** 文档


五、通过将strcat项目中的代码和测试代码提交到码云上作业日期的文件夹下，巩固如何用码云提交作业

![Alt text](http://a2.qpic.cn/psb?/V12zMyyF04Kif8/dUqcVkpw.JY5X5wf1Jf0T8*0lZ0T*zatT*nuYf4rG0Q!/b/dOIAAAAAAAAA&bo=.gOAAgAAAAARAEw!&rf=viewer_4)