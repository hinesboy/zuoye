#include"str_lib.h"
/* 计算char * 数组长度 */
inline int getLength(char * str)
{
	int i = 0;
	if (str)
	{
		while (str[i] != '\0')
		{
			i++;
		}
	}
	return i;
}
/* 将origin的内容放入target中 */
inline void strcpy(char * target, char * origin, int begin, int end)
{
	if (origin)
	{
		int i = begin, j = 0;
		for (; i < end; i++)
		{
			target[i] = origin[j++];
		}
	}
}

char * strcat(char  * str1, char  * str2)
{
	// 获得两个字符串长度和
	int str1_len = getLength(str1), str2_len = getLength(str2);
	int len = str1_len + str2_len;
	// 申请动态空间
	char * str = (char*)malloc(len + 1);
	// 将str1 str2 放入str中
	strcpy(str, str1, 0, str1_len);
	strcpy(str, str2, str1_len, len);
	// str末位添加结束标志
	if (len)
	{
		str[len] = '\0';
	}
	else
	{
		free(str);
		str = NULL;
	}
	return str;
}

