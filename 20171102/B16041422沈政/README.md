![实验截图](https://gitee.com/uploads/images/2017/1108/210443_e5257db6_1611777.png "实验截图.png")

 **实验心得** 
原本以为很简单的实验，没想到发现这么多问题。
初步完成程序后，运行，结果一直输出乱码。研究后发现原来是因为没有在返回值str末尾添加'\0'，虽然在申请空间时有额外加一个字节，但那时并没有意识到这个操作的用意。看来学习还是要追根究底的好。
添加'\0'后，又出现输出为空的问题，反复看了代码才知道是为str赋值时遍历至结尾并直接返回的缘故，只好再遍历回去。由此可知，char类型的指针和char数组还是有所区别的。
成功实现之后，发现其实可以省略计算长度、申请空间的操作，直接在str1上赋值。但不知道会不会引发内存问题，还有待进一步的验证。

最后是关于git的使用，不知道如何修改，很多同学提交错误后只能从头再来，所以希望老师可以给出其他一些常用操作的教程。