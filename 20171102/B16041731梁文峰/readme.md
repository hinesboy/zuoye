心得体会：
1，学习了git客户端与码云的相关操作
2，熟悉gtest的使用
3，在写strcat时，要对各种参数为NULL的情况加以对待

运行截图：
![strcat测试运行截图](https://gitee.com/uploads/images/2017/1204/215534_1b2ba7ed_1575092.png "test截图.png")
