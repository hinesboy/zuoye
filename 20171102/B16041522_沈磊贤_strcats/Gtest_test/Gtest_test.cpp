// Gtest_test.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "strcats.h"
#include <gtest\gtest.h>


TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", strcats("abc", "def"));
	
}

TEST(FooTest, HandleEmptyInput)
{
	EXPECT_STREQ("abc", strcats("abc", ""));
	EXPECT_STREQ("abc", strcats("abc", ""));
}

TEST(FooTest, HandleNULLInput)
{
	EXPECT_STREQ("abc", strcats(NULL, "abc"));
	EXPECT_STREQ("", strcats(NULL, NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	system("pause");
	return 0;
	
}
