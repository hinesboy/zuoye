#include <iostream>
#include <gtest/gtest.h>
using namespace std;

char* mystrcat(char* str1, char* str2)
{
	int len1 = 0;
	int	len2 = 0;
	char* ch;

	if (str1 != NULL)
	{
		ch = str1;
		while (*ch != '\0')
		{
			ch++;
			len1++;
		}
	}

	if (str2 != NULL)
	{
		ch = str2;
		while (*ch != '\0')
		{
			ch++;
			len2++;
		}
	}

	if (len1 + len2 == 0)
	{
		return NULL;
	}

	ch = new char[len1 + len2 + 1];

	for (int i = 0; i < len1; i++)
	{
		ch[i] = str1[i];
	}
	for (int i = 0; i < len2; i++)
	{
		ch[len1 + i] = str2[i];
	}
	ch[len1 + len2] = '\0';
	return ch;
}

TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", mystrcat("abc", "def"));
	EXPECT_STREQ("abc", mystrcat("abc", ""));
	EXPECT_STREQ("abc", mystrcat("abc", NULL));
	EXPECT_STREQ("abc", mystrcat(NULL, "abc"));
	EXPECT_STREQ(NULL, mystrcat(NULL, NULL));
}

int main(int argc, char* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}