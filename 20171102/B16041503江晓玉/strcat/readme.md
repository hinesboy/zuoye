

作业心得

一、实现char* strcat(char* str1, char* str2)

通过提示：我复习了：计算str1和str2的长度，申请内存空间，逐个字符复制到申请的内存空间中

，字符串结束标记写入等关于字符串的操作。

二、能够用 **gtest** 来进行单元测试：分别测试了Strcat("abc","def")，Strcat

("abc","")，Strcat("abc",NULL)，Strcat(NULL,"abc")，Strcat(NULL,NULL);

三、能够在自己文件夹下面撰写 **README.md** 文档

四、学会如何把作业提交到码云，并将strcat项目中的代码和测试代码提到码云上作业日期的文件夹下

五、截图见文件中的实验截图.png