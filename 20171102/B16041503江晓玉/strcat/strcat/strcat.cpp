// strcat.cpp: 定义控制台应用程序的入口点。
//分别计算str1和str2的长度；申请存储空间；逐个字符复制到申请的内存空间；字符串结束标记写入
//测试 strcat("abc", "def");
//测试 strcat("abc", "");
//测试 strcat("abc", NULL);
//测试 strcat(NULL, "abc");
//测试 strcat(NULL, NULL);

#include "stdafx.h"
#include"strcat.h"
TEST(stract, Stract)
{
	EXPECT_STREQ("abcdef", Strcat("abc","def"));
	EXPECT_STREQ("abc", Strcat("abc", ""));
	EXPECT_STREQ("abc", Strcat("abc", NULL));
	EXPECT_STREQ("abc", Strcat(NULL, "abc"));
	EXPECT_STREQ("", Strcat(NULL, NULL));
}

int main(int argc,char* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

