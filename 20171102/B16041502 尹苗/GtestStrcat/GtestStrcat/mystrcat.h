#pragma once
#ifndef _FOO_H_
#define _FOO_H_

#include "stdafx.h"
#include<stdio.h>
#include<iostream>
#include<malloc.h>
#include<string.h>
char * myStrcat(char *str1, char *str2)
{
	int length1 = 0, length2 = 0;
	char *p = str1, *q = str2;
	while (str1&& *p++ != '\0')
	{
		length1++;
	}
	while (str2&& *q++ != '\0')
	{
		length2++;
	}
	char *str = (char*)malloc(sizeof(char)*(length1 + length2) + 1);
	char *t = str;
	while (str1&&*str1 != '\0')
	{
		*str++ = *str1++;
	}
	while (str2&&*str2 != '\0')
	{
		*str++ = *str2++;
	}
	*str = '\0';
	return t;
}

#endif