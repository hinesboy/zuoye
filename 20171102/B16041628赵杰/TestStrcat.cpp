﻿// TestStrcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include<gtest/gtest.h>
#include <stdlib.h>

char* my_strcat(char* str1, char* str2)
{
	int i, s1 = 0, s2 = 0;
	char *p = str1, *q = str2, *t, *s;
	if (str1 == NULL)
	{
		if (str2 == NULL)
			return NULL;
		else
			return str2;
	}
	else
	{
		if (!str2)
			return str1;
	}
	while (*p != '\0')
	{
		s1++;
		p++;
	}
	while (*q != '\0')
	{
		s2++;
		q++;
	}
	t = (char *)malloc(sizeof(char)*(s1 + s2 + 1));
	s = t;
	p = str1;
	q = str2;
	for (i = 0; i < s1; i++)
	{
		*(t + i) = *p;
		p++;
	}
	for (; i < s1 + s2; i++)
	{
		*(t + i) = *q;
		q++;
	}
	*(t + i) = '\0';
	return s;
}

TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("ABCDEF",my_strcat("ABC","DEF"));
	EXPECT_STREQ("ABC", my_strcat("ABC", ""));
	EXPECT_STREQ("ABC", my_strcat("ABC", NULL));
	EXPECT_STREQ("ABC", my_strcat(NULL, "ABC"));
	EXPECT_STREQ(NULL, my_strcat(NULL, NULL));
}
int _tmain(int argc,_TCHAR* argv[])
{
	printf("strcat(\"ABC\",\"DEF\")：%s\n", my_strcat("ABC", "DEF"));
	printf("strcat(\"ABC\",\"\")：%s\n", my_strcat("ABC", ""));
	printf("strcat(\"ABC\",NULL)：%s\n", my_strcat("ABC", NULL));
	printf("strcat(NULL,\"DEF\")：%s\n", my_strcat(NULL, "DEF"));
	printf("strcat(NULL,NULL)：%s\n", my_strcat(NULL, NULL));	
	testing::InitGoogleTest(&argc,argv);
    return RUN_ALL_TESTS();
}