#include "gtest\gtest.h"
#include "pch.h"
#include "../strcat/strcat.h"
TEST(TestCaseName, TestName) {
  EXPECT_STREQ("abcdef",_strcat("abc","def"));
  EXPECT_STREQ("abc", _strcat("abc", ""));
  EXPECT_STREQ("abc", _strcat("abc", NULL));
  EXPECT_STREQ("abc", _strcat(NULL, "abc"));
  EXPECT_STREQ("", _strcat(NULL, NULL));
}	

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	system("pause");
	return 0; 
}