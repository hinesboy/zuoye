#include "stdafx.h"
#include <iostream>
using namespace std;

char* strcat_s(char* str1, char* str2)
{
	int str1Len = 0, str2Len = 0;

	if (str1 == NULL&&str2 == NULL)
	{
		return NULL;
	}
	if (str1 == NULL)
	{
		return str2;
	}
	if (str2 == NULL)
	{
		return str1;
	}
	while (str1[str1Len] != '\0')//统计字符串str1的长度
	{
		str1Len++;
	}
	while (str2[str2Len] != '\0')//统计字符串str2的长度
	{
		str2Len++;
	}

	char* strDest = (char*)malloc(str1Len + str2Len + 1);//申请内存空间
	int i = 0, s1 = 0, s2 = 0;

	while (str1[s1] != '\0')
	{
		strDest[i++] = str1[s1];//复制字符串str1
		s1++;
	}
	while (str1[s2] != '\0')
	{
		strDest[i++] = str2[s2];//复制字符串str2
		s2++;
	}

	strDest[str1Len + str2Len] = '\0';//末尾加结束符

	return strDest;

}