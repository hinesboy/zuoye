// strcat.cpp: 定义控制台应用程序的入口点。
#include "stdafx.h"
#include"strcat.h"
#include "gtest/gtest.h"
//#define EXPECT_STREQ(str1,str2)

//使用宏TEST()来定义我们的测试函数
//TEST()的第一个参数即测试用例的名称，第二个参数为该测试用例中测试实例的名称
TEST(strcat, strcat_s)
{
	EXPECT_STREQ("abcdef", strcat_s("abc", "def"));//EXPECT_*系列，当检查点失败时，不终止所在测试函数，继续往下执行。
}
TEST(strcat, strcat_s1)
{
	EXPECT_STREQ("abc", strcat_s("abc", ""));
}
TEST(strcat, strcat_s2)
{
	EXPECT_STREQ("abc", strcat_s("abc", NULL));
}
TEST(strcat, strcat_s3)
{
	EXPECT_STREQ("abc", strcat_s(NULL, "abc"));
}
TEST(strcat, strcat_s4)
{
	EXPECT_STREQ(NULL, strcat_s(NULL, NULL));
}


int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();//“RUN_ALL_TESTS()” ：运行所有测试案例
}