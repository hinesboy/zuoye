#pragma once

#include <cstring>
#include <iostream>

char* my_strcat(const char* s1, const char* s2) {
	int len1 = s1==NULL?0:strlen(s1);
	int len2 = s2==NULL?0:strlen(s2);
	char* result = new char(len1+len2+1);
	int position = 0;
	for (int i = 0; i < len1; i++) {
		result[position++] = s1[i];
	}
	for (int i = 0; i < len2; i++) {
		result[position++] = s2[i];
	}
	result[position] = '\0';
	return result;
}
