// charCatProject.cpp: 定义控制台应用程序的入口点。
//
#include "stdafx.h"
#include <gtest\gtest.h>
#include <tchar.h>
#include <stdlib.h>
#include <iostream>
#include "charHandler.h"

TEST(case1, test1) {
	std::cout << my_strcat("abc", "def") << std::endl;
	std::cout << my_strcat("abc", "") << std::endl;
	std::cout << my_strcat("abc", NULL) << std::endl;
	std::cout << my_strcat(NULL, "abc") << std::endl;
	std::cout << my_strcat(NULL, NULL) << std::endl;
	system("pause");
}

int main(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	return 0;
}

