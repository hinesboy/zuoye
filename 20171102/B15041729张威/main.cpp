#include <QCoreApplication>
#include "gtest/gtest.h"
#include <iostream>
#include <cstdlib>

int mstrlen(char *str);
char *mstrcat(char *str1, char *str2);

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();

    return a.exec();
}

int mstrlen(char *str)
{
    int len = 0;

    if(str)
    {
        while( *str++ )
        {
            ++len;
        }
    }
    return len;
}

char *mstrcat(char *str1, char *str2)
{
    int len1 = mstrlen(str1);
    int len2 = mstrlen(str2);
    char *str = (char*)malloc(len1+len2+1);

    char *p = str;
    if(str1)
    {
        while( *p++ = *str1++ )
        {}
        --p;
    }
    if(str2)
    {
        while( *p++ = *str2++ )
        {}
        --p;
    }

    *p = 0;
    return str;
}

TEST(testCase, test0)
{
    EXPECT_STREQ("abcdef", mstrcat("abc", "def"));
}

TEST(testCase, test1)
{
    EXPECT_STREQ("abc", mstrcat("abc", ""));
}

TEST(testCase, test2)
{
    EXPECT_STREQ("abc", mstrcat("abc", NULL));
}

TEST(testCase, test3)
{
    EXPECT_STREQ("abc", mstrcat(NULL, "abc"));
}

TEST(testCase, test4)
{
    EXPECT_STREQ("", mstrcat(NULL, NULL));
}

