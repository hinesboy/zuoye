// strcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "strcat.h"

TEST(test, 1)
{
	EXPECT_EQ(*"abcdef", *my_strcat("abc", "def"));
}
TEST(test, 2)
{
	EXPECT_STREQ("abc", my_strcat("abc", ""));
}
TEST(test, 3)
{
	EXPECT_STREQ("abc", my_strcat("abc", NULL));
}
TEST(test, 4)
{
	EXPECT_STREQ("abc", my_strcat(NULL, "abc"));
}
TEST(test, 5)
{
	EXPECT_STREQ("", my_strcat(NULL, NULL));
}
int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

