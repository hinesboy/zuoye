## 11.02实验 实现 char* strcat(char* str1,char* str2)

### 实验内容

一、创建win32 console application应用程序 **strcat**

二、实现char* strcat(char* str1, char* str2)

提示：分别计算str1和str2的长度，申请内存空间，逐个字符复制到申请的内存空间中，字符串结束标记写入

三、用 **gtest** 来进行单元测试

1. 测试 strcat("abc","def");
2. 测试 strcat("abc","");
3. 测试 strcat("abc",NULL);
4. 测试 strcat(NULL,"abc");
5. 测试 strcat(NULL,NULL);

### 实验心得

+ 当自定义函数命名为strcat时，因与C语言自带的函数重载，测试时 **strcat(NULL,NULL)** 找不到相应的重载函数，需要改掉自己的函数名

+ 对于GTEST的使用更熟悉

	- 测试比较的是字符串类型，所以使用了 **EXPECT_STREQ** 断言

	- 当使用 **EXPECT_EQ** 断言时，比较的int类型的值，就变成比较char*指针的地址，所以要加上 * 比较指针指向的内容

### 实验截图

见images文件夹

### 说明

strcat.cpp——测试文件

strcat.h——函数代码
