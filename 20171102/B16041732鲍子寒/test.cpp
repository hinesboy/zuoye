#include "pch.h"


char* mystrcat(char* str1, char* str2)
{
	int temp1 = 0, temp2 = 0, temp = 0;
	if (str1 != NULL) {
		while (str1[temp1] != '\0')
		{
			temp1++;
		}
	}
	if (str2 != NULL) {
		while (str2[temp2] != '\0')
		{
			temp2++;
		}
	}
	temp = temp1 + temp2 + 1;
	char* newstr = new char[temp];
	if (str1 == NULL && str2 != NULL)
	{
		for (int i = 0; i < temp2; i++) {
			newstr[i] = str2[i];
		}
		newstr[temp2] = '\0';
	}
	else if (str1 != NULL && str2 == NULL)
	{
		for (int i = 0; i < temp1; i++) {
			newstr[i] = str1[i];
		}
		newstr[temp1] = '\0';
	}
	else if (str1 == NULL && str2 == NULL)
	{
		{
			return NULL;
		}
	}
	else
	{
		for (int i = 0; i < temp1; i++) {
			newstr[i] = str1[i];
		}
		for (int i = 0; i < temp2; i++) {
			newstr[i + temp1] = str2[i];
		}
		newstr[temp - 1] = '\0';
	}
	return newstr;
}

TEST(StrcatTest, Test1)
{
	EXPECT_STREQ("abcdef", mystrcat("abc", "def"));
}

TEST(StrcatTest, Test2)
{
	EXPECT_STREQ("abc", mystrcat("abc", ""));
}

TEST(StrcatTest, Test3)
{
	EXPECT_STREQ("abc", mystrcat("abc", NULL));
}

TEST(StrcatTest, Test4)
{
	EXPECT_STREQ("abc", mystrcat(NULL, "abc"));
}

TEST(StrcatTest, Test5)
{
	EXPECT_STREQ(NULL, mystrcat(NULL, NULL));
}

int main(int argc, char* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}