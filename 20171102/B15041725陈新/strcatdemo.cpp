// strcatdemo.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include<iostream>
#include<stdlib.h>
#include "malloc.h"
#include<gtest\gtest.h>

using namespace std;

char* strcat1(char* str1, char* str2) {
	int str1_length = 0;
	int str2_length = 0;

	while (str1 != NULL && str1[str1_length] != '\0')
	{
		str1_length++;
	}

	while (str2 != NULL && str2[str2_length] != '\0')
	{
		str2_length++;
	}



	int count = str1_length + str2_length;
	char *str3 = (char *)malloc(count + 1);

	for (int m = 0; m < count; m++)
	{
		if (m<str1_length)
		{
			str3[m] = str1[m];
		}
		else
		{
			str3[m] = str2[m - str1_length];
		}

	}

	str3[count] = '\0';

	return str3;
}

/*int main() {
cout << strcat1("abc", "def") << endl;
cout << strcat1("abc", "") << endl;
cout << strcat1("abc", NULL) << endl;
cout << strcat1(NULL, "abc") << endl;
cout << strcat1(NULL, NULL) << endl;

system("pause");
return 0;
}*/

TEST(testCase, test0)
{
	EXPECT_STREQ("abcdef", strcat1("abc", "def"));
}

TEST(testCase, test1)
{
	EXPECT_STREQ("abc", strcat1("abc", ""));
}

TEST(testCase, test2)
{
	EXPECT_STREQ("abc", strcat1("abc", NULL));
}

TEST(testCase, test3)
{
	EXPECT_STREQ("abc", strcat1(NULL, "abc"));
}

TEST(testCase, test4)
{
	EXPECT_STREQ(NULL, strcat1(NULL, NULL));
}


int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}



