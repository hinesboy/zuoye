#include "stdafx.h"
#include <iostream>
#include <gtest/gtest.h>

using namespace std;

char* mystrcat(char* str1, char* str2)
{
	int c = 0;
	int d = 0;
	if (str1 == NULL&&str2 == NULL) {
		return "";
	}
	if (str1 == NULL) {
		return str2;
	}
	if (str2 == NULL) {
		return str1;
	}
	c = strlen(str1);
	d = strlen(str2);

	std::cout << "str1的长度：" << c << '\n';
	std::cout << "str2的长度：" << d << '\n';

	int length = c + d;
	char* p = (char*)malloc(length + 1);
	for (int i = 0; i < c; i++) {
		p[i] = str1[i];
	}
	for (int j = 0; j < d; j++) {
		p[j + c] = str2[j];
	}
	p[length] = '\0';
	return p;
}

TEST(StrcatPro01, strcat1) {
	EXPECT_STREQ("abcdef", mystrcat("abc", "def"));
}
TEST(StrcatPro01, strcat2) {
	EXPECT_STREQ("abc", mystrcat("abc", ""));
}
TEST(StrcatPro01, strcat3) {
	EXPECT_STREQ("abc", mystrcat("abc", NULL));
}
TEST(StrcatPro01, strcat4) {
	EXPECT_STREQ("", mystrcat(NULL, NULL));
}
TEST(StrcatPro01, strcat5) {
	EXPECT_STREQ("abc", mystrcat(NULL, "abc"));
}

int main(int argc, char* argv[])
{
	char* text = mystrcat("123", "4567");
	cout << text << endl;

	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();

	system("pause");
	return 0;
}