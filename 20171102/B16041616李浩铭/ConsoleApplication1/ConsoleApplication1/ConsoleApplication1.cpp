// ConsoleApplication1.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include<string.h>
#include<gtest\gtest.h>
char * mystrcat(char *str1, char *str2)
{
	int m, n,i,j,k;
	char *str;
	if (str1 == NULL&&str2 == NULL)
		return NULL;
	if (str1 == NULL)
		return str2;
	if (str2 == NULL)
		return str1;
	m = strlen(str1);
	n = strlen(str2);
	str = (char *)malloc(sizeof(char)*(m + n));
	for (i = 0; i < m; i++)
	{
		str[i] = str1[i];
	}
	for (j = 0, k = m; j < n; j++, k++)
	{
		str[k] = str2[j];
	}
	str[k] = '\0';
	return str;
}

TEST(FootTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef",mystrcat("abc","def"));
	EXPECT_STREQ("abc",mystrcat("abc",""));
	EXPECT_STREQ("abc", mystrcat("abc", NULL));
	EXPECT_STREQ("abc", mystrcat(NULL, "abc"));
	EXPECT_STREQ(NULL, mystrcat(NULL, NULL));
}
int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}