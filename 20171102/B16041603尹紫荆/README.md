# 11.02
## strcat函数：由于需要连接NULL与字符串，函数内需要判断str是否为空，否则指针会出现异常。
## Gtest：1. 需要在链接器-输入-附加依赖项中添加gtestd.lib的地址，否则出现很多无法解析的外部命令错误。2. 使用EXPECT_STREQ比较字符串，并将char[]数组转换为char*，否则运行会失败。
![](http://ww1.sinaimg.cn/large/0060lm7Tly1fl3wdnxb7wj30t70ladfw.jpg)