// Demo.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <gtest\gtest.h>
char *Strcat(char *str1, char *str2)
{
	if (str1 == NULL&&str2 == NULL)
		return NULL;
	else if (str2 == NULL)
		return str1;
	else if (str1 == NULL)
		return str2;
	else
	{
		char *address = str1;
		int i = 0, j = 0;
		i = strlen(str1);
		j = strlen(str2);
		address = (char *)malloc((i + j + 1) * sizeof(char));
		i = 0, j = 0;
		while (str1[i] != '\0')
		{
			address[j] = str1[i];
			i++;
			j++;
		}
		i = 0;
		while (str2[i] != '\0')
		{
			address[j] = str2[i];
			i++;
			j++;
		}
		address[j] = '\0';
		return address;
	}
}

TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", Strcat("abc", "def"));
	EXPECT_STREQ("abc", Strcat("abc", ""));
	EXPECT_STREQ("abc", Strcat("abc", NULL));
	EXPECT_STREQ("abc", Strcat(NULL, "abc"));
	EXPECT_STREQ(NULL, Strcat(NULL, NULL));
}
int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}