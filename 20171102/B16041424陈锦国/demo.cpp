// ConsoleApplication1.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include<stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string>
#include<gtest/gtest.h>
using namespace std;
char *strcat_s(char *str1,char *str2)
{
	int i = 0, j = 0;
	char *ch;
	char *rt;
	char *temp;
	ch = str1;
	while (*ch != '\0')
	{
		i++; ch++;
	}
	ch = str2;
	while (*ch!= '\0')
	{
		j++; ch++;
	}
	rt = (char*)malloc((i + j + 1)*sizeof(char));
	temp = rt;
	ch = str1;
	while (*ch != '\0')
	{
		*rt = *ch;
		ch++;
		rt++;
	}
	ch = str2;
	while (*ch!= '0')
	{
		*rt = *ch;
		ch++;
		rt++;
	}
	rt = '\0';
	cout<<temp << endl;
	cout<<i<<"---"<<j<<endl;
	return temp;
}
TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", strcat_s("abc", "def"));
	EXPECT_STREQ("abc", strcat_s("abc", ""));
	EXPECT_STREQ("abc", strcat_s("abc", NULL));
	EXPECT_STREQ("abc", strcat_s(NULL, "abc"));
	EXPECT_STREQ(NULL, strcat_s(NULL, NULL));
}
int main(int argc, char* argv[])
{
	//strcat_s("abc","def");
	char ch;
	cin >> ch;
	//strcat("abc","");
	//strcat("abc",NULL);
	//strcat(NULL,"abc");
	//strcat(NULL,NULL);
	return 0;
}


