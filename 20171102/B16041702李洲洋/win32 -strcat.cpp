// win32 -strcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include<iostream>
#include<gtest/gtest.h>
char* my_strcat(char* str1, char* str2)
{
	if (str1 == NULL) {
		if (str2 == NULL) {
			return NULL;
		}
		else {
			return str2;
		}
	}
	else if (str2 == NULL) {
		return str1;
	}
	int num1 = strlen(str1), num2 = strlen(str2);
	/*
	while (*str1 != '\0') {
	str1++;
	num1++;
	}
	while (*str2 != '\0') {
	str2++;
	num2++;
	}
	*/
	char *str = NULL;//把str1和str2复制到同一新的str字符串上
					 //str = (char *)malloc((num1+num2)*sizeof(char)+1);
	str = new char[num1 + num2 + 1];
	for (int i = 0; i < num1; i++) {
		str[i] = str1[i];
	}
	for (int i = 0; i < num2; i++) {
		str[i + num1] = str2[i];
	}
	str[num1 + num2] = '\0';

	if (num1 + num2 > 0) {
		return str;
	}
	else {
		return NULL;
	}

}
TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", my_strcat("abc", "def"));
	EXPECT_STREQ("abc", my_strcat("abc", ""));
	EXPECT_STREQ("abc", my_strcat("abc", NULL));
	EXPECT_STREQ("abc", my_strcat(NULL, "abc"));
	EXPECT_STREQ(NULL, my_strcat(NULL, NULL));

}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
}
