﻿PS：strcat项目代码和测试代码都放在win32 -strcat.cpp文件中

实验心得：

此次终于学会了使用gtest，十分好用，找出了代码中的逻辑错误。

同时通过这个也反思了我自己编写代码在逻辑上的问题。

对于用git敲命令行的方式还是不是很熟悉。

实验截图：

![实验结果](https://gitee.com/uploads/images/2017/1123/180023_7a5f28a5_1598054.png "成功.PNG")