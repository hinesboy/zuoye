// strcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"

TEST(test, 1)
{
	EXPECT_STREQ("abcdef", mystrcat("abc","def"));
}
TEST(test, 2)
{
EXPECT_STREQ("abc", mystrcat("abc",""));
}
TEST(test, 3)
{
EXPECT_STREQ("abc", mystrcat("abc", NULL));
}
TEST(test, 4)
{
EXPECT_STREQ("abc", mystrcat(NULL, "abc"));
}
TEST(test, 5)
{
EXPECT_STREQ(NULL, mystrcat(NULL, NULL));
}
int _tmain(int argc, _TCHAR* argv[])
{

	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

