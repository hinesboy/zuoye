#include "strcat.h"
#include "stdafx.h"
/*
char* strcat(char* str1, char* str2)
{
	int m = 0, n = 0, i = 0;
	while (str1[m] != '\0')
	{
		m++;
	}
	while (str2[n] != '\0')
	{
		n++;
	}
	printf("TEST:\nstr1\"%6s\" 长度为: %d\nstr2\"%6s\" 长度为: %d\n", str1, m, str2, n);
	char* newchar = (char*)malloc(sizeof(char)*(m + n + 1));
	for (i = 0; i < m; i++)
	{
		newchar[i] = str1[i];
	}
	for (i = 0; i < n; i++)
	{
		newchar[m + i] = str2[i];
	}
	newchar[m + n] = '\0';
	printf("合并后str3 \"%6s\" 长度为%d\n", newchar, m + n);
	return newchar;
}*/

char* mystrcat(char*str1, char*str2)
{
	int i = 0;
	int j = 0;
	int len1 = 0;
	int len2 = 0;
	char*src1 = str1;
	char*src2 = str2;
	if (str1==NULL && str2==NULL) return NULL;
	else if (str1 == NULL) return str2;
	else if (str2 == NULL) return str1;
	while (src1 && (*src1) != '\0')
	{
		len1++;
		src1++;
	}
	while (src2 && (*src2) != '\0')
	{
		len2++;
		src2++;
	}
	char*tmp = (char*)malloc(sizeof(char)*(len1 + len2 + 1));

	for (; i < len1; i++)
	{
		tmp[i] = str1[i];
	}
	for (; i < len1 + len2; i++, j++)
	{
		tmp[i] = str2[j];
	}
	tmp[i] = '\0';
	return tmp;
}