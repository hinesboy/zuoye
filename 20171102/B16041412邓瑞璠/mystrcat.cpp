#include<stdio.h>
#include<malloc.h>

char* mystrcat(char* str1, char* str2)
{
	int l1 = 0, l2 = 0, l;
	char* t;
	t = str1;
	if (t != NULL)
	{
		while (*t != '\0')
		{
			*t++;
			l1++;
		}
	}
	t = str2;
	if (t != NULL)
	{
		while (*t != '\0')
		{
			*t++;
			l2++;
		}
	}
	l = l1 + l2 + 1;
	char* temp;
	char* p;
	temp = (char*)malloc(l * sizeof(char));
	t = str1;
	p = temp;
	if (t != NULL)
	{
		while (*t != '\0')
		{
			*p = *t;
			*t++;
			*p++;
		}
	}
	t = str2;
	if (t != NULL)
	{
		while (*t != '\0')
		{
			*p = *t;
			*t++;
			*p++;
		}
	}
	*p = '\0';
	return temp;
}