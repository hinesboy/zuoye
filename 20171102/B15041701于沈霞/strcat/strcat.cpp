#include "stdafx.h"

#include <iostream>
#include <stdlib.h>
using namespace std;

char * strcat_s(char *, char *);
int getlen(char *);

int main(void)
{
	char str1[100], str2[100];

	cin >> str1;
	cin >> str2;

	cout << strcat_s(str1,str2);

	return 0;
}

int getlen(char *str)
{
	int len = 0;

	while (str[len] != '\0')
	{
		len++;
	}

	return len;
}

char * strcat_s(char *str1, char *str2)
{
	int len1 = 0, len2 = 0;

	if (str1 != NULL)
	{
		len1 = getlen(str1);
	}

	if (str2 != NULL)
	{
		len2 = getlen(str2);
	}

	if (str1 == NULL && str2 == NULL)
	{
		return NULL;
	}

	char * des = (char *)malloc(sizeof(char)*(len1 + len2 + 1));

	int i = 0, j;

	for (j = 0; j<len1; j++)
	{
		des[i++] = str1[j];
	}
	for (j = 0; j<len2; j++)
	{
		des[i++] = str2[j];
	}

	des[len1 + len2] = '\0';//加末尾结束符

	return des;
}