#include "stdafx.h"
//#include "stdio.h"
//#include <iostream>
#include "fun.h"
#include "gtest/gtest.h"


TEST(fun, strcat_s3) {
	EXPECT_STREQ("abc", strcat_s("abc",NULL));
}

TEST(fun, strcat_s4) {
	EXPECT_STREQ("abc",strcat_s(NULL,"abc"));
}

TEST(fun, strcat_s2) {
	EXPECT_STREQ("abc", strcat_s("abc",""));
}

TEST(fun, strcat_s1) {
	EXPECT_STREQ("abcdef", strcat_s("abc","def"));
}

TEST(fun, strcat_s5) {
	EXPECT_STREQ("", strcat_s(NULL,NULL));
}



int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}