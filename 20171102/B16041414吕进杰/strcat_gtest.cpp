#include<stdio.h>
#include<stdlib.h>
#include<gtest/gtest.h>

char* mystrcat(char* str1, char* str2)
{
	int i=0,j=0,k=0;
	char *p=str1;
	char *q=str2;
	if (p != NULL)
	{
		while (*p++ != '\0')
			j++;
	}
	if (q != NULL)
	{
		while (*q++ != '\0')
			j++;
	}
	char *p2=str1;
	char *q2=str2;
	char *t=(char*)malloc(sizeof(char)*(i+j+1));
	for (k=0;k<i;k++)
	{
		*(t+k)=*p2++;
	}
	for (k=0;k<j;k++)
	{
		*(t+k+i)=*q2++;
	}
	*(t+i+j)='\0';
	return t;
}

TEST(FooTest, HandleNoneZeroInput)
{
	 EXPECT_STREQ("abcdef",mystrcat("abc","def"));
	 EXPECT_STREQ("abc", mystrcat("abc", ""));
	 EXPECT_STREQ("abc", mystrcat("abc", NULL));
	 EXPECT_STREQ("abc", mystrcat(NULL, "abc"));
	 EXPECT_STREQ(NULL, mystrcat(NULL,NULL));
}
int _tmain(int argc, _TCHAR * argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}