#include<iostream>
#include<tchar.h>
#include"gtest/gtest.h"
char *join1(char *, char*);
int strlen(char *);
int strlen(char *s) {
	int a = 0;
	if (s == NULL)return 0;
	while (*s != '\0') {
		a++;
		s++;
	}
	return a;
}
char *join1(char *a, char *b) {
	a =  a == NULL? "":a;
	b =  b == NULL? "":b;
	char *c = (char *)malloc(strlen(a) + strlen(b) + 1); //局部变量，用malloc申请内存  
	if (c == NULL) return "";
	char *tempc = c;                                     //把首地址存下来,用于函数返回  
	
	while (*a != '\0') {                                 //字符串a赋值给c
		*c++ = *a++;
	}
	do {                                                 //字符串b赋值给c
		*c++ = *b++;
	} while (*c != '\0');
	
	//注意，此时指针c已经指向拼接之后的字符串的结尾'\0' !  
	return tempc;  
}
TEST(TestCaseName1, TestName) {
	EXPECT_STREQ("123", join1("1", "23"));
}
TEST(TestCaseName2, TestName) {
	EXPECT_STREQ("123", join1(NULL, "123"));
}
TEST(TestCaseName3, TestName) {
	EXPECT_STREQ("123", join1("123", NULL));
}
TEST(TestCaseName4, TestName) {
	EXPECT_STREQ("", join1(NULL, NULL));
}
int _tmain(int argc, _TCHAR * argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();

}
/*
int main(int argc, char* argv[]) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
	
	char a[20]; //   
	char b[20]; //  
	std::cout<<"请输入字符串一：";
	std::cin >> a;
	std::cout <<std::endl<< "请输入字符串二：";
	std::cin >> b;

	char *e = join1(NULL, NULL);
	std::cout<<std::endl<<"拼接字符串得：";
	std::cout<<e<<std::endl;
	return 0;	
}
*/

