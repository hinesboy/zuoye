// strcat.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <gtest.h>

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();

    return 0;
}
char* strcat1(char *str1, char* str2)
{
	int i = 0, temp = 0, length1, length2;
	while (str1[i] != '\0')
	{
		temp++;
		i++;
	}
	length1 = temp + 1;
	temp = 0;
	i = 0;
	while (str2[i] != '\0')
		temp++;
	length2 = temp + 1;
	char* Strcat = (char*)malloc(length1+length2+1);
	for (i = 0; i < length1; i++)
	{
		Strcat[i] = str1[i];
	}
	for (i = length1; i < length2 + length1 + 2; i++)
	{
		Strcat[i] = str2[i - length2 - length1 - 1];
	}
	return Strcat;
}
TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", strcat1("abc","def"));
	EXPECT_STREQ("abc", strcat("abc",""));
	EXPECT_STREQ("abc", strcat("abc",NULL));
	EXPECT_STREQ("abc", strcat(NULL, "abc"));
	EXPECT_STREQ("", strcat(NULL, NULL));
}



