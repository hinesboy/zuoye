﻿一、创建win32 console application应用程序
strcat
二、实现char* strcat(char* str1, char* str2)
提示：
分别计算str1和str2的长度，
申请内存空间，
逐个字符复制到申请的内存空间中，
字符串结束标记写入
三、用 gtest 来进行单元测试
测试 strcat("abc","def");
测试 strcat("abc","");
测试 strcat("abc",NULL);
测试 strcat(NULL,"abc");
测试 strcat(NULL,NULL);
四、在自己文件夹下面撰写 README.md 文档
包括：把实验心得和实验结果截图放在该文档中
五、将strcat项目中的代码和测试代码提交到码云上作业日期的文件夹下
