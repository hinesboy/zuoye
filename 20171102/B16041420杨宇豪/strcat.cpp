// strcat.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
char* strcat(char* str1, char* str2)
{
    if (str1 == NULL || str2 == NULL)
    {
        printf("The string can't be both null.\n");
        return NULL;
    }
    else
    {
        int length = 0;
        char *t = str1;
        char *r = str2;
        int i = 0;
        while (t[i] != '\0')
        {
            t++; length++;
        }
        while (r[i] != '\0')
        {
            r++; length++;
        }

        char *c = (char *)malloc(length * sizeof(char) + 1);
        char *temp = c;

        while (*str1 != '\0')
            *c++ = *str1++;
        while ((*c++ = *str2++) != '\0');
        return temp;
    }
}

int main()
{
    char *a = "abc";
    char *b = "def";
    char *c = NULL;
    printf("%s\n", strcat(a, b));
    printf("%s\n", strcat(a, ""));
    printf("%s\n", strcat(a, NULL));
    printf("%s\n", strcat(NULL, b));
    printf("%s\n", strcat(NULL, NULL));
    return 0;
}

