// windows实验1.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"

char* my_stract(char* s1, char* s2) {
	int a, b;
	char* str,*p;
	if (s1 == NULL&&s2 == NULL)
	{
		str = NULL;
		return str;
	}
	if (s1 == NULL&&s2 != NULL)
	{
		return s2;
	}
	if (s1 != NULL&&s2 == NULL)
	{
		return s1;
	}
	a = strlen(s1);
	b = strlen(s2);
	str = (char *)malloc(sizeof(char)*(a + b+1));
	p = str;
	while (*s1 != '\0')
	{
		*str++ = *s1++;
	}
	while((*str++ = *s2++) != '\0');
	return p;
}
TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STRCASEEQ("abcdef", my_stract("abc", "def"));
	EXPECT_STRCASEEQ("abc", my_stract("abc", ""));
	EXPECT_STRCASEEQ("abc", my_stract("abc", NULL));
	EXPECT_STRCASEEQ("abc", my_stract(NULL, "abc"));
	EXPECT_STRCASEEQ(NULL, my_stract(NULL, NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	//char* str1 = "abc";
	char* str2 = "def";
	char* str1 = NULL;
	//char* str2 = NULL;
	printf("%s", my_stract(str1, str2));
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

