// strcat.cpp: 定义控制台应用程序的入口点。
//
#include "stdafx.h"
char* My_strcat(char *str1, char *str2)
{
	if (str1 == NULL&&str2 == NULL)
		return NULL;
	if (str1 == NULL)
		return str2;
	if (str2 == NULL)
		return str1;
	int T1 = 0, T2 = 0;
	char  *t;
	t = str1;
	while (*t++ != '\0')
	{
		T1++;
	}
	t = str2;
	while (*t++ != '\0')
	{
		T2++;
	}
	char *p = (char*)malloc(sizeof(char)*(T1 + T2 + 1)); p[0] = 't';
	t = str1;
	int i = 0;
	while (i<T1)
	{
		p[i] = t[i];
		i++;
	}
	t = str2;
	while (i<(T2 + T1))
	{
		p[i] = t[i - T1];
		i++;
	}
	p[i] = '\0';
	return &p[0];
}

TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("ABCDEF", My_strcat("ABC", "DEF"));
	EXPECT_STREQ("ABC", My_strcat("ABC", ""));
	EXPECT_STREQ("ABC", My_strcat("ABC", NULL));
	EXPECT_STREQ("ABC", My_strcat(NULL, "ABC"));
	EXPECT_STREQ(NULL, My_strcat(NULL, NULL));
}
int _tmain(int argc, _TCHAR* argv[])
{
	printf("strcat(\"ABC\",\"DEF\")：%s\n", My_strcat("ABC", "DEF"));
	printf("strcat(\"ABC\",\"\")：%s\n", My_strcat("ABC", ""));
	printf("strcat(\"ABC\",NULL)：%s\n", My_strcat("ABC", NULL));
	printf("strcat(NULL,\"DEF\")：%s\n", My_strcat(NULL, "DEF"));
	printf("strcat(NULL,NULL)：%s\n", My_strcat(NULL, NULL));
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	return RUN_ALL_TESTS();
}

