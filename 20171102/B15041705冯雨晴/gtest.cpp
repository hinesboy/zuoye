#include <gtest/gtest.h>
#include<stdio.h>
#include<iostream>
using namespace std;
char * myStrcat(char * str1, char * str2);
int myStrlen(char * str1);

int main(int argc, _TCHAR* argv[]) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcefg", myStrcat("abc", "efg"));
	EXPECT_STREQ("abc", myStrcat("abc", NULL));
	EXPECT_STREQ("abc", myStrcat(NULL, "abc"));
	EXPECT_STREQ("abc", myStrcat("abc", NULL));
	EXPECT_STREQ("空字符串", myStrcat(NULL,NULL));
}
char * myStrcat(char * str1, char * str2) {
	int len1 = myStrlen(str1);
	int len2 = myStrlen(str2);
	if ((len1 + len2) == 0) {
		return "空字符串";
	}
	char * obj = (char*)malloc(len1 + len2 + 1);
	for (int i = 0; i < len1; i++) {
		obj[i] = str1[i];
	}
	for (int j = 0; j < len2; j++) {
		obj[j + len1] = str2[j];
	}
	obj[len1 + len2] = '\0';
	return obj;
}
int myStrlen(char * str) {
	if (str == NULL || str == "") {
		return 0;
	}
	int i = 0;
	while (str[i] != '\0') {
		i++;
	}
	return i;
}