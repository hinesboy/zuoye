#pragma once
#include <stdio.h>

char* strcatString(char* str1, char* str2)
{
	int len1 = 0, len2 = 0;
	if (str1 == NULL && str2 == NULL)
		return NULL;
	if (str1 == NULL)
		len1 = 0;
	if (str2 == NULL)
		len2 = 0;

	while (str1[len1] != '\0')
		len1++;
	while (str2[len2] != '\0')
		len2++;

	char *s = (char *)malloc(sizeof char(len1 + len2 + 1));
	int i = 0, j;
	for (j = 0; j < len1; j++)
		s[i++] = str1[j];
	for (j = 0; j < len2; j++)
		s[i++] = str2[j];
	s[len1 + len2] = '\0';
	return s;
}
