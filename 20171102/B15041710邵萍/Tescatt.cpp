
#include "stdafx.h"
#include "str.h"
#include "gtest/gtest.h"
//#include<iostream>

/*
������ gtest �����е�Ԫ����
���� strcat("abc", "def");
���� strcat("abc", "");
���� strcat("abc", NULL);
���� strcat(NULL, "abc");
���� strcat(NULL, NULL);
*/

TEST(str, str_s) {
	EXPECT_STREQ("abcdef", str_s("abc", "def"));
	EXPECT_STREQ("abc", str_s("abc", ""));
	EXPECT_STREQ("abc", str_s("abc", NULL));
	EXPECT_STREQ("abc", str_s(NULL, "abc"));
	EXPECT_STREQ("", str_s(NULL, NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}