#include "pch.h"
#include <iostream>
using namespace std;

char* mainstrcat(char *str1, char *str2)
{
	if (str1 == NULL)
		return str2;
	if (str2 == NULL)
		return str1;
	int count1 = 0, count2 = 0;
	char  *t;
	t = str1;
	while (*t != '\0')
	{
		count1++;
		t++;
	}
	t = str2;
	while (*t!= '\0')
	{
		count2++;
		t++;
	}
	char *p = (char*)malloc(sizeof(char)*(count1 +count2 + 1)); p[0] = 't';
	t = str1;
	int i = 0;
	while (i<count1)
	{
		p[i] = t[i];
		i++;
	}
	t = str2;
	while (i<(count2+count1))
	{
		p[i] = t[i - count1];
		i++;
	}
	p[i] = '\0';
	return &p[0];
}

TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("ABCDEF",mainstrcat("ABC", "DEF"));
	EXPECT_STREQ("ABC", mainstrcat("ABC", ""));
	EXPECT_STREQ("ABC",mainstrcat("ABC", NULL));
	EXPECT_STREQ("ABC", mainstrcat(NULL, "ABC"));
	EXPECT_STREQ(NULL,mainstrcat(NULL, NULL));
}
int main(int argc, char * argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	return RUN_ALL_TESTS();
}