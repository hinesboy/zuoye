// strcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include <gtest/gtest.h>
char* Strcat(char* str1, char* str2) {
	int length1 = 0;
	int length2 = 0;
	int length = 0;

	if (str1 == NULL && str2 == NULL) {
		return NULL;
	}
	else if (str1 == NULL)
	{
		return str2;
	}
	else if (str2 == NULL)
	{
		return str1;
	}
	else {
		while (str1[length1] != '\0') {
			length1++;
		}
		while (str1[length2] != '\0') {
			length2++;
		}
		length = length1 + length2;
		char* str = (char*)malloc(length + 1);
		for (int i = 0; i < length1; i++) {
			str[i] = str1[i];
		}
		for (int j = 0; j < length2; j++) {
			str[j + length1] = str2[j];
		}
		str[length] = '\0';
		return str;
	}
}
TEST(case1, test1)
{
	EXPECT_STREQ("abcdef", Strcat("abc", "def"));
}

TEST(case2, test2)
{
	EXPECT_STREQ("abc", Strcat("abc", ""));
}

TEST(case3, test3)
{
	EXPECT_STREQ("abc", Strcat("abc", ""));
}
TEST(case4, test4)
{
	EXPECT_STREQ("abc", Strcat(NULL, "abc"));
}
TEST(case5, test5)
{
	EXPECT_STREQ("abc", Strcat("abc", NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

int main()
{
	return 0;
}
