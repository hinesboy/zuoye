#include "gtest/gtest.h"  
#include  "fun.h"
#include <tchar.h>   //若不包含，main中参数会报错

TEST(FootTest,HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef",stract("abc","def"));
	EXPECT_STREQ("abc", stract("abc", ""));
	EXPECT_STREQ("abc", stract("abc", NULL));
	EXPECT_STREQ("abc", stract(NULL, "abc"));
	EXPECT_STREQ(NULL, stract(NULL,NULL));
}

int _tmain(int argc, _TCHAR* argv[])
{
	//多个测试用例时使用，如果不写，运行RUN_ALL_TESTS()时会全部测试，加上则只返回对应的测试结果  
	//testing::GTEST_FLAG(filter) = "test_case_name.test_name";
	//测试初始化
	testing::InitGoogleTest(&argc, argv);
	//return RUN_ALL_TESTS();
	RUN_ALL_TESTS();
	//暂停，方便观看结果,结果窗口将会一闪而过  
	system("PAUSE");
	return 0;
}