// 112实验2.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include <string.h>
#include <malloc.h>
#include<gtest\gtest.h>

char* Strcat(char* str1, char* str2)
{
	if (str1 == NULL&&str2 == NULL)
	{
		return NULL;
	}
	if (str1 == NULL)
		return str2;
	if (str2 == NULL)
		return str1;
	char* str3;
	int i;
	int n1 = 0;
	int n2 = 0;
	while (str1[n1] != '\0')
	{
		n1++;
	}

	while (str2[n2] != '\0')
	{
		n2++;
	}
	str3 = (char*)malloc(sizeof(char)*(n1 + n2));
	for (i = 0; i < n1; i++)
	{
		str3[i] = str1[i];
	}
	for (i = n1; i < n1 + n2; i++)
	{
		str3[i] = str2[i - n1];
	}
	str3[n1 + n2] = '\0';
	return str3;
}
TEST(FootTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", Strcat("abc", "def"));
	EXPECT_STREQ("abc", Strcat("abc", ""));
	EXPECT_STREQ("abc", Strcat("abc", NULL));
	EXPECT_STREQ("abc", Strcat(NULL, "abc"));
	EXPECT_STREQ(NULL, Strcat(NULL, NULL));

}
int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
/*
测试 strcat("abc", "def");
测试 strcat("abc", "");
测试 strcat("abc", NULL);
测试 strcat(NULL, "abc");
测试 strcat(NULL, NULL);

*/