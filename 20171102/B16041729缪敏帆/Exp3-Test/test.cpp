#include "pch.h"
int mystrlen(char* str)
{
	char* p = str;
	int c = 0;
	while (*p != '\0')
	{
		c++;
		p++;
	}
	return c;
}
char* mystrcat(char* str1, char* str2)
{
	if (str1 == NULL && str2 == NULL)
		return NULL;
	if (str1 == NULL)
		str1 = "";
	if (str2 == NULL)
		str2 = "";
	char* rst = new char[mystrlen(str1) + mystrlen(str2) + 1];
	char* tmp = rst;
	while (*str1 != '\0')
	{
		*rst = *str1;
		rst++;
		str1++;
	}
	while (*str2 != '\0')
	{
		*rst = *str2;
		rst++;
		str2++;
	}
	*rst = '\0';
	return tmp;
}
TEST(strcat, s1) {
	EXPECT_STREQ(mystrcat("abc","def"), "abcdef");
}

TEST(strcat, s2) {
	EXPECT_STREQ(mystrcat("abc", ""), "abc");
}

TEST(strcat, sn) {
	EXPECT_STREQ(mystrcat("abc", NULL), "abc");
}
TEST(strcat, ns) {
	EXPECT_STREQ(mystrcat(NULL, "abc"), "abc");
}
TEST(strcat, ss) {
	EXPECT_STREQ(mystrcat(NULL, NULL), "");
}

int main(int argc,char* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}