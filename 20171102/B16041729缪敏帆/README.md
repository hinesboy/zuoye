# 实验心得   
1、strcat对空指针NULL要进行特殊处理   
2、""和NULL的区别：""是指向char数组{‘\0’}的指针，有实际意义；NULL则是空指针，不指向任何内容   
3、gtest 字符串的比较用ASSERT_STREQ()和EXPECT_STREQ()   
4、git贡献代码时，要fork为自己的远程库，最后pull request贡献代码。直接clone别人的库则无权push   
##实验结果截图   
![result](https://gitee.com/uploads/images/2017/1109/194305_1cfb4778_1576079.jpeg "exp3result.jpg")
