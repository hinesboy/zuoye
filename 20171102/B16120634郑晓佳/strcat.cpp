// strcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "stract1.h"
#include <gtest/gtest.h>



TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_STREQ("abcdef", strcat1("abc", "def"));
	EXPECT_STREQ("abc", strcat1("abc", ""));
	EXPECT_STREQ("abc", strcat1("abc", NULL));
	EXPECT_STREQ("abc", strcat1(NULL, "abc"));
	EXPECT_STREQ(NULL, strcat1(NULL, NULL));

}

int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	//getchar();
	system("pause");
	return 0;

}