﻿<<<<<<< HEAD
# 作业内容

1. 创建win32 console application应用程序 **strcat**
2. 实现char* strcat(char* str1, char* str2)
3. 用 **gtest** 来进行单元测试
    1. 测试 strcat("abc","def");
    2. 测试 strcat("abc","");
    3. 测试 strcat("abc",NULL);
    4. 测试 strcat(NULL,"abc");
    5. 测试 strcat(NULL,NULL);

4. 实验心得和实验结果
在本次实验前，我们已经在前两节课完成了strcat的实现，也尝试用gtest做过测试。在本次实验课之前我，我对gtest使用还很不熟练，导致前几次做测试，一直有错误。后来才发现因为测试的是字符串，需要将测试代码中的 EXPECT_EQ改为EXPECT_STREQ。
通过本次实验，自己也增加了gtest的使用，也学会了使用git。

5. 实验过程错误和结果图
    ![错误](https://gitee.com/Act_xiaojia/zuoye/blob/master/20171102/B16120634%E9%83%91%E6%99%93%E4%BD%B3/img-folder/error.png)
    ![结果](https://gitee.com/Act_xiaojia/zuoye/blob/master/20171102/B16120634%E9%83%91%E6%99%93%E4%BD%B3/img-folder/ok.png)