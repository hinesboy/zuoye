测试用例
---
```c++
TEST(TestCaseName, test0) {
	EXPECT_STREQ("abcdef", myStrcat("abc", "def"));
}
TEST(TestCaseName, test1) {
	EXPECT_STREQ("abc", myStrcat("abc", ""));
}
TEST(TestCaseName, test2) {
	EXPECT_STREQ("abc", myStrcat("abc", NULL));
}
TEST(TestCaseName, test3) {
	EXPECT_STREQ("abc", myStrcat(NULL, "abc"));
}
TEST(TestCaseName, test4) {
	EXPECT_STREQ(NULL, myStrcat(NULL, NULL));
}
```
运行结果
---
![image](http://wx2.sinaimg.cn/mw690/00677rG9gy1fl3wlz73v1j30l90d60tn.jpg)

**感想：**  
&ensp;&ensp;很长时间没写c++了，代码应该有很多不合理的地方。最近也在从头开始整理知识点，相信很快就能梳理完。