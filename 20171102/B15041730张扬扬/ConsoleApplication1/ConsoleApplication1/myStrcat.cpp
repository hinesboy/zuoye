#include "stdafx.h"
#include "myStrcat.h"
/*
*@author zhangyangyang
*@time   2017-11-2
*自己实现strcat（）合并字符串的功能
*代码应该有很多不合理的地方，自己以前一直搞Java
*C++的东西现在才开始整理，以后会继续完善
*/
char* myStrcat(char *str1, const char * str2)
{
	if (str1 == NULL & str2 ==NULL)
	{
		return NULL;
	}
	if (str1 == NULL)                //对str1  str2空指针处理
	{
		return (char *)str2;
	}
	if (str2 == NULL)
	{
		return str1;
	}
	int str1Size = sizeof(str1);
	int str2Size = sizeof(str2);
	char *p = (char *)malloc(str1Size + str2Size - 1);  
	char *x = p;          //定义一个指针记录p
	while (*str1 != '\0')
	{
		*p++ = *str1++;
	}
	while (*str2 != '\0')
	{
		*p++ = *str2++;
	}
	*p = '\0';         //不能返回p，因为此时P指向最后
	return x;
	
}