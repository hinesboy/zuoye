// strcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
using namespace std;

char *strcat(char *str1, char *str2);


int main()
{
	char str1[7] = "hello ";
	char str2[7] = "world!";
	char str3[1] = "";
	char * p1 = strcat(str1, str2);
	char * p2 = strcat(str3, str2);
	char * p3 = strcat(str1, str3);
	cout << p1 << endl;
	cout << p2 << endl;
	cout << p3 << endl;
	char x1[100], x2[100];
	cout << "请输入第一个字符串:";
	cin >> x1;
	cout << "请输入第二个字符串:";
	cin >> x2;
	cout << "拼接好的字符串:" << strcat(x1, x2) << endl;
    return 0;
}

char *strcat(char *str1, char *str2)
{
	int lstr1 = 0, lstr2 = 0;
	if (str1 != NULL)
	{
		while (str1[lstr1] != '\0')
		{
			lstr1++;
		}
	}
	if (str2 != NULL)
	{
		while (str2[lstr2] != '\0')
		{
			lstr2++;
		}
	}
	if (lstr1 == 0)
	{
		char *p = new char[lstr2 + 1];
		for (int i = 0; i<lstr2; ++i)
		{
			p[i] = str2[i];
		}
		p[lstr2] = '\0';
		return p;
	}
	else if (lstr2 == 0)
	{
		char *p = new char[lstr1 + 1];
		for (int i = 0; i<lstr1; ++i)
		{
			p[i] = str1[i];
		}
		p[lstr1] = '\0';
		return p;
	}
	else if (lstr1 == 0 && lstr2 == 0)
	{
		char *p = new char;
		*p = '\0';
		return p;
	}
	else
	{
		char *p = new char[lstr1 + lstr2 + 1];
		for (int i = 0; i<lstr1; ++i)
		{
			p[i] = str1[i];
		}
		for (int i = 0; i<lstr2; ++i)
		{
			p[lstr1 + i] = str2[i];
		}
		p[lstr1 + lstr2] = '\0';
		return p;
	}
}