﻿# 实验心得：

## 控制台下面的使用

*   a.首先编译gTest的源代码，会得到gtest.lib，然后将该lib文件拷贝到工程目录下并加入工程。 通过属性设置 或者 #pra....都可以。

*   b.在工程的附加目录里面包含gtest的include目录，这个目录在gTest的源代码里面。

*   c.在工程里面添加gTest的测试代码就可以了。
   
>>   如果编译不过去，可能是Debug/Release版本，那么gtest.lib也必须是debug/Release编译出来的，而且工程的运行时库(md/mdt之类的)要和编译gTest.lib时gTest的运行时库一致。

[实验截图](./实验截图.png)
