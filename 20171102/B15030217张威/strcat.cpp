// strcat.cpp: 定义控制台应用程序的入口点。
//

#include "stdafx.h"
//#include <iostream>
#include <gtest\gtest.h>
//using namespace std;
char* strcatt(char* str1, char* str2) {
	int len1 = 0, len2 = 0;
	char *p = str1, *q = str2;
	if (str1 == NULL&&str2 == NULL) return NULL;
	if (str1 == NULL) len1 = 0;
	else {
		while (*(p++) != '\0') ++len1;
	}
	if (str2 == NULL) len2 = 0;
	else {
		while (*(q++) != '\0') ++len2;
	}
	char *ans = (char *)malloc((len1 + len2) * sizeof(char));
	int j = 0;
	for (int i = 0; i < len1; ++i) ans[j++] = str1[i];
	for (int i = 0; i < len2; ++i) ans[j++] = str2[i];
	ans[j] = '\0';
	return ans;
}
/*TEST(FooTest, HandleNoneZeroInput)
{
	EXPECT_EQ("abcdef", strcatt("abc", "def"));
	EXPECT_EQ("abc", strcatt("abc", ""));
	EXPECT_EQ("abc", strcatt("abc", NULL));
	EXPECT_EQ("abc", strcatt(NULL, "abc"));
	EXPECT_EQ(NULL, strcatt(NULL, NULL));
}*/
TEST(testCase, test0)
{
	EXPECT_STREQ("abcdef", strcatt("abc", "def"));
}

TEST(testCase, test1)
{
	EXPECT_STREQ("abc", strcatt("abc", ""));
}

TEST(testCase, test2)
{
	EXPECT_STREQ("abc", strcatt("abc", NULL));
}

TEST(testCase, test3)
{
	EXPECT_STREQ("abc", strcatt(NULL, "abc"));
}

TEST(testCase, test4)
{
	EXPECT_STREQ(NULL, strcatt(NULL, NULL));
}


int _tmain(int argc, _TCHAR* argv[])
{
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

