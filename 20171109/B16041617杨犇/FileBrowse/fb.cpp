/* ************************************
*《精通Windows API》
* 示例代码
* fb.c
* 12.1.4  浏览文件夹对话框
**************************************/
/* 头文件　*/
#include <Windows.h>
#include <shlobj.h>
/* 函数申明　*/
DWORD Browse(HWND hwnd);

/*************************************
* WinMain
* 功能	程序入口点，调用Browse
*
* 参数	未使用
**************************************/
int WINAPI WinMain(
	HINSTANCE hInstance,           //应用程序当前实例句柄
	HINSTANCE hPrevInstance,       //应用程序先前实例句柄
	LPSTR lpCmdLine,               //指向应用程序命令中，除了程序名的null字符结束的字符串
	int nCmdShow                   //程序窗口表现状态
)
{
	Browse(NULL);
}

/*************************************
* WinMain
* 功能	弹出“浏览文件夹”对话框，
并获取用户选择的文件夹目录
*
* 参数	HWND hwnd	父窗口句柄
**************************************/
DWORD Browse(HWND hwnd)
{
	// 用于保存路径
	CHAR szRoot[MAX_PATH];             //用于存游戏根路径的字符串
	CHAR szChoose[MAX_PATH];           //用于存选择游戏路径的字符串
	CHAR szDisplayName[MAX_PATH];      //用于存演示游戏路径的字符串
	// 相关变量
	LPITEMIDLIST pidlRoot = NULL;      //初始化要显示的文件夾的根
	LPITEMIDLIST pidlSelected = NULL;  
	BROWSEINFO bi = { 0 };             //初始化BROWSEINFO结构体变量bi
	LPMALLOC pMalloc = NULL;

	// “浏览文件夹”的根路径，开发人员可根据情况选择，比如只浏览“我的文档”。
	SHGetFolderLocation(NULL, CSIDL_DESKTOP, NULL, 0, &pidlRoot);       //返回一个指向ITEMIDLIST的指針，ITEMIDLIST表示了用戶所选择的文件夹
	SHGetPathFromIDList(pidlRoot, szRoot);        //將一个ITEMLIST转换成文件系统中的路径
	// 填充 BROWSEINFO 结构
	bi.hwndOwner = hwnd;                     //浏览文件夹对话框的父窗体句柄
	bi.pidlRoot = pidlRoot;                  //ITEMIDLIST结构的地址，包含浏览时的初始根目录
	bi.pszDisplayName = szDisplayName;       //保存用户选中的目录字符串的内存地址
	bi.lpszTitle = "Choose a target";        //该浏览文件夹对话框的显示文本
	bi.ulFlags = 0;                          //描述了对话框的选项，参数为0
	bi.lpfn = NULL;                          //浏览对话框回调函数的地址
	bi.lParam = 0;                           //对话框传递给回调函数的一个参数指针
	// 弹出对话框
	pidlSelected = SHBrowseForFolder(&bi);   //调用SHBrowseForFolder(&bi);弹出对话框
	// DisplayName
	MessageBox(NULL, szDisplayName, "Display Name:", MB_OK);
	// 选择的文件夹
	SHGetPathFromIDList(pidlSelected, szChoose);
	MessageBox(NULL, szChoose, "Choose:", MB_OK);
	// 释放
	ILFree(pidlRoot);
	return 0;
}