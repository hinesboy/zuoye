/* ************************************
*《精通Windows API》
* 示例代码
* setup.c
* 15.3 setup.exe 安装程序
**************************************/
/* 头文件 */
/*Windows.h 是 Windows 应用程序开发中常用的头文件，
在Windows应用程序开发中所使用的很多的数据类型、结构、API接口函数都在Windows.h 
或 Windows.h 所包含的其他头文件中进行了声明*/
/*SetupAPI是一个被用来执行安装设备的一系列操作的方法的集合，主要用于安装设备驱动（device driver），
被用在类安装程序（class installers）、协安装程序（co-installers）和
设备安装应用程序(device installation applications)中*/
/*shlobj.h包含函数获取指定系统路径*/
#include <Windows.h>
#include <Setupapi.h>
#include <shlobj.h>
/* 库 */
#pragma comment (lib, "shell32.lib")
#pragma comment (lib, "Setupapi.lib")
//Shell API 都在 shlobj.h 头文件中声明，由 She1132.dll 导出，链接时需要使用到 She1132.lib 库

/*************************************
* VOID GetSourceDirectory(LPSTR szPath)
* 功能 获得当前路径
* szPath，返回路径
**************************************/
VOID GetSourceDirectory(LPSTR szPath)//LPTSTR 以NULL为终止符字符串指针
{
	int i;
	//获取文件的绝对路径
	GetModuleFileName(NULL,//null表示获取当前应用程序的路径
					  szPath,//szPath数组用来保存获取到的执行程序当前的绝对路径
					  MAX_PATH//MAX_PATH是一个宏定义，值为260，即数组szPath的大小
				      );												

	i=strlen(szPath);//获取文件路径的长度，下面是对文件路径的操作
	while ((i>0)&&(szPath[i-1]!='\\'))//最后一个‘\’后面就是文件名
	{
		szPath[--i]=0;
	}
}

/*************************************
* WinMain
* 功能 调用相关Setup API进行安装
**************************************/
//在系统调用WinMain函数时，参数会传递给应用程序
INT WinMain(
			HINSTANCE hInstance,//handle to current instance该程序当前运行的实例的句柄
			HINSTANCE hPrevInstance,//handle to previous instance当前实例的前一个实例的句柄
			LPSTR lpCmdLine,//command line一个以空终止的字符串，指定传递给应用程序的命令行参数
			int nCmdShow//show state指定程序的窗口应该如何显示，例如最大化、最小化、隐藏等
			)
{
	HINF hInf; // INF文件句柄
	CHAR szSrcPath[MAX_PATH];// 源路径
	CHAR szDisPath[MAX_PATH];// 目的路径
	BOOL bResult;
	PVOID pContext;//pContext指向HWInit函数返回的指针
	// 与本程序在同一目录下的Setup.inf
	GetSourceDirectory(szSrcPath);//获取该程序所在的目录
	lstrcat(szSrcPath,"setup.inf");//LPTSTR 两个参数类型均为以NULL为终止符字符串指针 
								   //字符串拼接，获取改程序所在目录下的setup.inf文件路径
	// 打开 inf 文件（Device Information File）设备信息文件
	hInf = SetupOpenInfFile(szSrcPath, NULL, INF_STYLE_WIN4, NULL);//打开setup.inf文件
																	//szSrcPath是inf文件的绝度路径，
	// 是否成功
	if (hInf == INVALID_HANDLE_VALUE)//句柄值若是无效的，则打开inf文件失败
	{
		//定义一个弹窗
		MessageBox(NULL,//HWND hWnd，消息框拥有的窗口，为NULL，则消息框没有拥有窗口
					"Error: Could not open the INF file.",//LPCTSTR lpText 消息框内容
					"ERROR",//LPCTSTR lpCaption 消息框的标题
					MB_OK|MB_ICONERROR //UINT uType 指定一个决定对话框的内容和行为的位标志集
				   );//MB_OK 默认值，有一个确认按钮；MB_ICONERROR 有一个停止消息图标
		return FALSE;
	}
	// 获得Program Files的路径
	SHGetSpecialFolderPath(NULL, //HWND hwndOwner,  窗口所有者的句柄，可以直接传null
							szDisPath, // LPTSTR lpszPath, 返回路径的缓冲区
							CSIDL_PROGRAM_FILES ,// int nFolder, 系统路径的CSIDL标识,即指定的Program Files的CSIDL标识
							//CSIDL (constant special item ID list)是系统定义的特殊条目的ID列表，在shlobj.h中进行定义
							FALSE // BOOL fCreate，指示文件夹不存在时是否创建。为false则不创建，否则创建
						   );
	// 构造目的路径
	lstrcat(szDisPath,"\\MyInstall");//字符串构造安装目的路径在文件夹MyInstall下

	// 给inf配置文件中的路径ID赋值，使用路径替换路径ID
	//返回值bool类型，表明目录ID与目标目录关联是否成功
	bResult = SetupSetDirectoryId(hInf, 32768, szDisPath);//设置为程序安装路径，以便程序构造目的文件夹
	if (!bResult)//设置安装目录ID失败
	{
		MessageBox(NULL,//消息框没有拥有窗口
					"Error: Could not associate a directory ID with the destination directory.",//消息框内容
					"ERROR",//消息框标题
					MB_OK|MB_ICONERROR //MB_OK 默认值，有一个确认按钮；MB_ICONERROR 有一个停止消息图标
				   );
		SetupCloseInfFile(hInf);//关闭inf文件，并释放打开inf文件时产生的hInf句柄
		return FALSE;
	}

	// 设置默认callback函数的参数
	pContext=SetupInitDefaultQueueCallback(NULL);

	// 进行安装
	//默认安装返回值为bool类型，赋值给bResult
	bResult=SetupInstallFromInfSection(
		NULL, // 父窗口句柄
		hInf, // INF文件句柄
		"Install", // INF文件中，配置了安装信息的节名//表明按照install节定义的安装过程来安装
		SPINST_FILES | SPINST_REGISTRY , // 安装标志//定义了哪几类的动作会被执行
										//这里的安装标志表示只有与文件和注册表相关的操作才会被实施
		NULL, // 安装键值
		NULL, // 源文件和路径，可以在INF文件中配置
		0, // 复制时的动作
		(PSP_FILE_CALLBACK)SetupDefaultQueueCallback, // 回调函数
		pContext, // 回调函数的参数
		NULL, // 设备信息
		NULL // 设备信息
		);
	// 安装是否成功
	if (!bResult)
	{
		// 失败，输出错误信息
		MessageBox(NULL,//HWND hWnd，消息框拥有的窗口，为NULL，则消息框没有拥有窗口
					"SetupInstallFromInfSection",//LPCTSTR lpText 消息框内容
				    "ERROR", //LPCTSTR lpCaption 消息框的标题
					MB_OK|MB_ICONERROR //UINT uType 指定一个决定对话框的内容和行为的位标志集
		           );//MB_OK 默认值，有一个确认按钮；MB_ICONERROR 有一个停止消息图标
		//关闭，释放掉SetupInitDefaultQueueCallback占用的资源
		SetupTermDefaultQueueCallback(pContext);//pContext是默认回调函数的参数
		SetupCloseInfFile(hInf);//关闭inf文件，并释放打开inf文件时产生的hInf句柄
		return FALSE;
	}

	// 关闭，释放掉SetupInitDefaultQueueCallback占用的资源
	SetupTermDefaultQueueCallback(pContext);//pContext是默认回调函数的参数
	SetupCloseInfFile(hInf);//关闭inf文件，并释放打开inf文件时产生的hInf句柄

	return TRUE;
}