/* ************************************
*《精通Windows API》
* 示例代码
* time.c
* 10.2  系统时间
**************************************/
#include <Windows.h>
#include <stdio.h>
/* ************************************
* int main()
* 功能	获取并显示系统当前时间，然后将时间提前一个小时
**************************************/
int main()
{
	SYSTEMTIME st;//声明对象
	GetLocalTime( &st );// 获取当前时间，以本时区时间格式
	printf("Now: %d-%d-%d, %d:%d:%d",//打印时间
		st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
	st.wHour --;// 提前一小时
	SetLocalTime( &st );// 设置当前系统时间
}
