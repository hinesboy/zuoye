/* ************************************
*《精通Windows API》 
* 示例代码
* cur_mod_dir.c
* 4.3.4	获取当前目录、获取程序所在的目录、获取模块路径
**************************************/

/* 头文件　*/
#include <windows.h>
#include <stdio.h>

/* ************************************
* int main(void)
* 功能	演示使用设置获取当前路径
*		演示获取模块路径
**************************************/
int main(void)
{
	//用于存储当前路径
	CHAR szCurrentDirectory[MAX_PATH];//用260个char来存储当前路径
	//用于存储模块路径
	CHAR szMoudlePath[MAX_PATH];//用260个char来存储模块路径
	//Kernel32文件名与句柄
	LPSTR szKernel32 = "kernel32.dll";//char * szKernel32;类型重命名//typedef _Null_terminated_（以'\0'结尾的字符串） CHAR  *LPSTR
	HMODULE hKernel32;//HMODULE表示模块句柄                         //typedef HINSTANCE HMODULE;
	//当前路径的长度，也用于判断获取是否成功
	DWORD dwCurDirPathLen;//unsigned long dwCurDirPathLen;          //typedef unsigned long       DWORD;
	
	//获取进程当前目录
	dwCurDirPathLen = 
		GetCurrentDirectory(MAX_PATH,szCurrentDirectory);//获取模块文件名
	if(dwCurDirPathLen == 0)
	{
		printf("获取当前目录错误。\n"); 
		return 0;
	}
	printf("进程当前目录为 %s \n",szCurrentDirectory); 
	
	//将进程当前目录设置为“C:\”
	lstrcpy(szCurrentDirectory, "C:\\");//复制当前路径到"C:\\"指向的缓冲区
	if(!SetCurrentDirectory(szCurrentDirectory))//设置进程的当前目录
	{
		printf("设置当前目录错误。\n"); 
		return 0;
	}
	printf("已经设置当前目录为 %s \n",szCurrentDirectory); 

	//在当前目录下创建子目录“current_dir”
	//运行完成后C:盘下将出现文件夹“current_dir”
	CreateDirectory("current_dir", NULL);//第一个参数值为文件夹名称，第二个参数值为安全属性，一般设置为NULL即可。如果正确创建，返回值为1，如果没有正常创建文件夹，则返回0。

	//再次获取系统当前目录
	dwCurDirPathLen = 
		GetCurrentDirectory(MAX_PATH,szCurrentDirectory);//获取模块文件名
	if(dwCurDirPathLen == 0)
	{
		printf("获取当前目录错误。\n"); 
		return 0;
	}
	printf("GetCurrentDirectory获取当前目录为 %s \n",szCurrentDirectory); 

	//使用NULL参数，获取本模块的路径。句柄为NULL:该函数返回该应用程序全路径
	if(!GetModuleFileName(NULL,szMoudlePath,MAX_PATH))//获取当前进程已加载模块的文件的完整路径，该模块必须由当前进程加载。
	{
		printf("获取模块路径录错误。\n"); 
		return 0;
	}
	printf("本模块路径 %s \n",szMoudlePath);

	//获取Kernel32.dll的模块句柄。
	hKernel32 = LoadLibrary(szKernel32);//载入指定的动态链接库，并将它映射到当前进程使用的地址空间。一旦载入，即可访问库内保存的资源

	//使用Kernel32.dll的模块句柄，获取其路径。
	if(!GetModuleFileName(hKernel32,szMoudlePath,MAX_PATH))//把句柄载入指定字符串缓冲区，装载到缓冲区的最大字符数量不多于MAX_PATH=260
	{
		printf("获取模块路径错误。\n"); 
		return 0;
	}
	printf("kernel32模块路径 %s \n",szMoudlePath); 

	return 0;
}