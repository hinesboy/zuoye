//start.cpp: 定义应用程序的入口点。
//

#include "stdafx.h"
#include "start.h"

/* ************************************
*《精通Windows API》
* 示例代码
* start.c
* 1.1.1	第一个示例程序，弹出消息对话框
**************************************/

/* 预处理　*/
/* 头文件　*/
//#include <windows.h>
//连接时使用User32.lib
#pragma comment (lib, "User32.lib")

/* ************************************
* WinMain
* 功能	Windows应用程序示例
**************************************/
	/*
	WinMain：应用程序入口点
	函数返回一个int价值。返回值不被操作系统使用，但可以使用返回值将状态代码传递给编写的其他程序
	*/
int WinMain(
	HINSTANCE hInstance,	//一个称为“实例句柄”或“模块句柄”的东西。操作系统在内存中加载时使用这个值来标识可执行文件(EXE)。实例句柄是某些窗口函数所必需的 - 例如，加载图标或位图
	HINSTANCE hPrevInstance,// 毫无意义。它在16位的视窗中使用，但现在总是零
	LPSTR lpCmdLine,		//将命令行参数作为unicode字符串包含
	int nCmdShow			//表示主应用程序窗口是否将最小化、最大化或正常显示的标志
)
{
	// 调用API函数 MessageBox
	/*
	MessageBox函数显示一个模式对话框

	句法：
	int WINAPI MessageBox(
	_In_opt_ HWND    hWnd,//要创建的消息框所有者窗口的句柄
	_In_opt_ LPCTSTR lpText,//要显示的消息
	_In_opt_ LPCTSTR lpCaption,//对话框标题。如果此参数为空，默认的标题是错误
	_In_     UINT    uType//对话框的内容和行为
	);
	
	*/
	MessageBox(NULL,				//消息框没有所有者窗口
		TEXT("开始学习Windows编程"),
		TEXT("消息对话框"),			
		MB_OK);						//消息框包含一个按钮：OK，这是默认的
	return 0;
}