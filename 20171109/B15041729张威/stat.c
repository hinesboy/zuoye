/* ************************************
*《精通Windows API》 
* 示例代码
* stat.c
* 5.5.2  获得当前系统内存使用情况
**************************************/

/* 头文件　*/
#include <windows.h>
#include <stdio.h>

/*************************************
* int main(void)
* 功能	获取内存使用情况
*
* 参数	未使用
**************************************/
int main(void)
{
/*

typedef unsigned __LONG32 DWORD;	定义双字类型   

gcc对标准C语言进行了扩展，但用到这些扩展功能时，编译器会提出警告，使用__extension__关键字会告诉gcc不要提出警告。
#define __MINGW_EXTENSION __extension__ 
__MINGW_EXTENSION typedef unsigned __int64 ULONGLONG;
typedef ULONGLONG DWORDLONG;

typedef struct _MEMORYSTATUSEX {
	DWORD dwLength;
	DWORD dwMemoryLoad;
	DWORDLONG ullTotalPhys;
	DWORDLONG ullAvailPhys;
	DWORDLONG ullTotalPageFile;
	DWORDLONG ullAvailPageFile;
	DWORDLONG ullTotalVirtual;
	DWORDLONG ullAvailVirtual;
	DWORDLONG ullAvailExtendedVirtual;
} MEMORYSTATUSEX,*LPMEMORYSTATUSEX;
内存使用情况信息存储结构  
*/	
	//用于保存信息
	MEMORYSTATUSEX memstatusex;
	//设置结构大小
	memstatusex.dwLength = sizeof(memstatusex);
	//获取系统内存使用情况
	/* 
	__declspec用于指定所给定类型的实例的与Microsoft相关的存储方式。
	其它的有关存储方式的修饰符如static与extern等是C和C++语言的ANSI规范，而__declspec是一种扩展属性的定义。
	#define DECLSPEC_IMPORT __declspec (dllimport)
	#define WINBASEAPI DECLSPEC_IMPORT
	typedef int WINBOOL;
	#define __stdcall
	#define WINAPI __stdcall
	WINBASEAPI WINBOOL WINAPI GlobalMemoryStatusEx (LPMEMORYSTATUSEX lpBuffer);
	*/
	GlobalMemoryStatusEx(&memstatusex);
	//打印输入结果
	printf("TotalPhys\tAvailPhys\tTotalVirtual\tAvailVirtual\t"
		"TotalPageFile\tuAvailPageFile\tMemoryLoad\n"
		"%I64u\t%I64u\t%I64u\t%I64u\t%I64u\t%I64u\t%u%%",
		memstatusex.ullTotalPhys,memstatusex.ullAvailPhys,
		memstatusex.ullTotalVirtual,memstatusex.ullAvailVirtual,
		memstatusex.ullTotalPageFile,memstatusex.ullAvailPageFile,
		memstatusex.dwMemoryLoad);
	return 0;
}
