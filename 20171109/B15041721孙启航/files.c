/* ************************************
 *《精通Windows API》 
 * 示例代码
 * files.c
 * 4.3.1	删除、复制、重命名、移动文件
 **************************************/

/* 头文件　*/
#include <windows.h>       //编译预处理命令，引用window.h头文件，可以使用其中的函数
#include <stdio.h>             //编译预处理命令，引用stdio.h头文件，可以使用其中的函数

/* ************************************
 * int main( int argc, PCHAR argv[] )
 * 功能	应用程序主函数，根据输入参数
 *		删除、复制、重命名文件
 *
 * 参数	删除文件：
 *			-d 文件路径
 *		将文件路径1的文件复制到文件路径2：
 *			-c 文件路径1 文件路径2
 *		将文件路径1的文件移动、重命名为文件路径2的文件
 *			-m 文件路径1 文件路径2
 **************************************/
int main(int argc, PCHAR argv[])  //定义返回值类型为整数型的主函数main，参数为一个整数argc，一个字符串argv
{
	//-d参数，删除文件。
	if(0==lstrcmp("-d",argv[1]) && argc==3)  //调用lstrcmp比较目标字符串与-d是否相同，区分大小写，同时判断argc是否为3
	{
		if(!DeleteFile(argv[2]))  //如果删除操作失败
		{
			printf("删除文件错误：%x\n",GetLastError());  //输出取得的删除函数执行失败时的错误信息。
		}
		else
		{
			printf("删除成功！\n");  //删除成功
		}
	}
	//-c参数，复制文件。
	//如果文件存在，询问用户是否覆盖
	else if(0==lstrcmp("-c",argv[1]) && argc==4)  //调用lstrcmp比较目标字符串与 -c是否相同，区分大小写，同时判断argc是否为4
	{
		//复制，不覆盖已经存在的文件
		if(!CopyFile(argv[2],argv[3],TRUE))  //如果复制操作失败，TRUE表示不覆盖已经存在的文件
		{
			//LastError == 0x50，文件存在。
			if(GetLastError() == 0x50)  //如果错误原因为文件已存在
			{
				printf("文件%s已经存在，是否覆盖？y/n：",argv[3]);  //输出错误信息，提示是否覆盖
				if('y'==getchar())  //若选择'是'
				{
					//复制，覆盖已经存在的文件。
					if(!CopyFile(argv[2],argv[3],FALSE))  //如果覆盖文件失败
					{
						printf("复制文件错误，%d\n",GetLastError());  //输出取得的错误信息
					}
					else
					{
						printf("复制成功！\n");  //覆盖文件成功
					}
				}
				else
				{
					return 0;  //程序中止
				}
			}
		}
		else
		{
			printf("复制成功！\n");  //复制成功
		}
	}
	//-m参数，移动、重命名文件。
	else if(0==lstrcmp("-m",argv[1]) && argc==4)  //调用lstrcmp比较目标字符串与-m是否相同，区分大小写，同时判断argc是否为4
	{
		if(!MoveFile(argv[2],argv[3]))  //如果移动文件失败
		{
			printf("移动文件错误：%d\n",GetLastError());  //输出获得的错误信息
		}
		else
		{
			printf("移动文件成功！\n");  //移动成功
		}
	}
	else
	{
		printf("参数错误！\n");  //提示参数错误
	}
}

