// display.cpp: 定义控制台应用程序的入口点。

#include "stdafx.h"

/* ************************************
*《精通Windows API》
* 示例代码
* display.c
* 10.1  系统信息
**************************************/
#include <windows.h>
#include <stdio.h>

void main()
{
	BOOL fResult;
	int aMouseInfo[3];
	//GetSystemMetrics用于得到被定义的系统数据或者系统配置信息
	//GetSystemMetrics是一个计算机函数，该函数只有一个参数，称之为「索引」，这个索引有75个标识符，
	//通过设置不同的标识符就可以获取系统分辨率、窗体显示区域的宽度和高度、滚动条的宽度和高度。
	//SM_MOUSEPRESENT 如果为TRUE或不为0的值则安装了鼠标，否则没有安装
	fResult = GetSystemMetrics(SM_MOUSEPRESENT);

	if (fResult == 0)
		printf("No mouse installed.\n");
	else
	{
		printf("Mouse installed.\n");

		// Determine whether the buttons are swapped. 
		//SM_SWAPBUTTON 如果为TRUE或不为0的值则鼠标左右键交换，否则没有
		fResult = GetSystemMetrics(SM_SWAPBUTTON);

		if (fResult == 0)
			printf("Buttons not swapped.\n");
		else printf("Buttons swapped.\n");

		// Get the mouse speed and the threshold values. 

		fResult = SystemParametersInfo(
			//SPI_GETMOUSE:检索鼠标的2个阈值和加速特性。pvParam参数必须指向一个长度为3的整型数组，分别存储此值。
			SPI_GETMOUSE,  // get mouse information 指定要查询或设置的系统级参数（动作设置的，该参数分GET和SET两种行动）
			0,             // not used 设置是随第一个参数的设置作相应调整的
			&aMouseInfo,   // holds mouse information 设置是随第一个参数的设置作相应调整的
			0);            // not used 在设置系统参数的时候，是否应更新用户设置参数。可以是零(禁止更新)，或下述任何一个参数：
		    //SPIF－UPDATEINIFILE：更新WIN.INI和注册表中的用户配置文件。
			//SPIF－SENDWININICHANGE：倘若也设置了SPIF－UPDATEINIFILE，将一条
			//WM－WININICHANGE消息发给所有应用程序，否则没有作用。这条消息告诉应用程序已经改变了用户配置设置。

		if (fResult)
		{
			printf("Speed: %d\n", aMouseInfo[2]);
			printf("Threshold (x,y): %d,%d\n",
				aMouseInfo[0], aMouseInfo[1]);
		}
	}
}

