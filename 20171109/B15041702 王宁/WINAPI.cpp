/* ************************************
*《精通Windows API》 
* 示例代码
* Enumeratevolume.c
* 4.2.1	遍历驱动器并获取驱动器属性
**************************************/

#define _WIN32_WINNT 0x0501

/* 头文件　*/
#include <windows.h>
#include <stdio.h>

/* 预定义　*/
#define BUFSIZE   MAX_PATH //定义最大内存 260

/* 函数申明　*/
BOOL GetDirverInfo(LPSTR szDrive);//string szDrive 获取驱动器属性

/* ************************************
* 功能	应用程序主函数，遍历驱动器并调用
*		GetDirverInfo 获取驱动器属性
**************************************/

int main(void)
{
	TCHAR buf[BUFSIZE];      // 卷标信息 char * buf
	HANDLE hVol;             // 卷遍历句柄
	BOOL bFlag;              //定义一个标志，句柄是否被关闭 

   /*************************************************************************
	*HANDLE FindFirstVolume(LPTSTR lpszVolumName,DWORD cchBufferLength);
	*lpszVolumName:指向驱动器名的内存缓冲区。
	*cchBufferLength：参数lpszVolumName所指向的缓冲区大小，以字节为单位。
	*返回值：
	*驱动器查找句柄，可作为FindNextVolume和FindVolumeClose的参数，
	*如果执行失败，返回NULL。
	**************************************************************************/
	hVol = FindFirstVolume (buf, BUFSIZE );
    
	//调用失败返回 #define INVALID_HANDLE_VALUE (HANDLE)(-1),值为-1的句柄 
	if (hVol == INVALID_HANDLE_VALUE)
	{
		printf (TEXT("No volumes found!\n"));//没有找到卷 
		return (-1);                        //退出程序，返回-1 
	}

	GetDirverInfo (buf);//FindFirstVolume 显示第一个磁盘的属性 

   /**************************************************************************
    * FindNextVolume
	* 功能：查找主机后继的逻辑驱动器
	* BOOL FindNextVolume(
	*                    HANDLE hFindVolume,
	*                    LPTSTR lpszVolumeName,
	*                    DWORD cchBufferLength
	*                                      );
	* 参数：
	* hFindVolume：FindFirstVolume所返回的驱动器查找句柄。
	* lpszVolumeName：指向保存驱动器的内存缓冲区。
	* cchBufferLength:参数lpszVolumeName所指向的缓冲区大小，以字节为单位。
	* 返回值：
	* 返回BOOL表示是否成功，如果失败说明已经查找所有的逻辑驱动器。
    **************************************************************************/
   //FindNextVolume 显示第二个磁盘
	while( FindNextVolume( hVol,buf,BUFSIZE))
	{
		GetDirverInfo (buf);
	}
	
    /*************************************************************************
	BOOL WINAPI FindVolumeClose(HANDLE hFileVolume);
	功能：关闭FindFirstVolume打开的卷遍历句柄
	参数：
	hFindVolume：要关闭的驱动器查找句柄。
	返回值：
	返回BOOL值表示是否成功关闭句柄。
	**************************************************************************/
	bFlag = FindVolumeClose(hVol);//关闭句柄 

	return (bFlag);
}

/* ************************************
 * BOOL GetDirverInfo(LPSTR szDrive)
 * 功能	获取驱动器的属性
 * 参数	LPSTR szDrive
 * 	指明要获取属性的驱动器的根路径 如 C:\
 * 返回值 BOOL 是否成功
 **************************************/
BOOL GetDirverInfo(LPSTR szDrive)
{
	UINT uDriveType;                     //驱动器类型 
	DWORD dwVolumeSerialNumber;          //序列号 
	DWORD dwMaximumComponentLength;      //最大组成长度 
	DWORD dwFileSystemFlags;             //文件系统标志 
	CHAR szFileSystemNameBuffer[BUFSIZE];//文件类型
	CHAR szDirveName[MAX_PATH];          //驱动器名 
	printf("\n%s\n",szDrive);            //输出驱动器的根路径字符串
	uDriveType = GetDriveType(szDrive);  //获取驱动器类型 
	
	switch(uDriveType)
	{
		case DRIVE_UNKNOWN:  //无法识别的设备 
			printf("The drive type cannot be determined. ");
			break;
		case DRIVE_NO_ROOT_DIR://没有根目录的驱动器 
			printf("The root path is invalid, for example, no volume is mounted at the path. ");
			break;
		case DRIVE_REMOVABLE://可移动设备 
			printf("The drive is a type that has removable media, for example, a floppy drive or removable hard disk. ");
			break;
		case DRIVE_FIXED://不可移动的磁盘 
			printf("The drive is a type that cannot be removed, for example, a fixed hard drive. ");
			break;
		case DRIVE_REMOTE://网络硬盘 
			printf("The drive is a remote (network) drive. ");
			break;
		case DRIVE_CDROM://CD光驱 
			printf("The drive is a CD-ROM drive. ");
			break;
		case DRIVE_RAMDISK://内存虚拟盘 
			printf("The drive is a RAM disk. ");
			break;
		default:
			break;
	}
	
  /*****************************************************************************
	* BOOL GetVolumeInfomation(
	* LPCTSTR lpRootPathName,
	* LPTSTR lpVolumeNameBuffer,
	* DWORD nVolumeNameSize,
	* LPDWORD lpVolumeSerialNumber,
	* LPDWORD lpMaximumComponentLength,
	* LPDWORD lpFileSystemFlags,
	* LPTSTR lpFileSystemNameBuffer,
	* DWORD nFileSystemNameSize
	* );
	* 功能：获取逻辑驱动器信息
	* 参数：
	* lpRootPathName：输入参数，指向所要获取属性的驱动器的根路径字符串。
	* lpvolumeNameBuffer:输出参数，返回驱动器名。
	* nVolumeNameSize：输入参数，lpVolumeNameBuffer的内存缓冲区的大小。
	* lpVolumeSerialNumber：输出参数，存储驱动器序列号。
	* lpMaximumComponentLength：输出参数，返回文件系统所支持的文件组成部分的最大值。
	* lpFileSystemFlags:输出参数，属性可以用来判断多种驱动器属性值，
	* 如 FILE_VOLUME_QUOTAS表示支持磁盘配额，
	* FILE_SUPPORTS_ENCRYPTION表示文件系统是否是支持EFS加密等。
	* lpFileSystemNameBuffer：输出参数，表示文件类型，如“NTFS”，“CDFS”等。
	* nFileSystemNameSize：lpFileSystemNameBuffer的缓冲区的大小。
	* 返回值：
	* 返回BOOL值，表示信息获取是否成功。
	**************************************************************************/
	if (!GetVolumeInformation(
		szDrive,       //驱动器的根路径字符串
		szDirveName,  //返回驱动器名
		MAX_PATH,    //lpVolumeNameBuffer的内存缓冲区的大小
		&dwVolumeSerialNumber, //存储驱动器序列号
		&dwMaximumComponentLength, //文件系统所支持的文件组成部分的最大值
		&dwFileSystemFlags,       //断多种驱动器属性值
		szFileSystemNameBuffer,   //表示文件类型 
		BUFSIZE                   //lpFileSystemNameBuffer的缓冲区的大小
		))
	{
		return FALSE; //没有找到返回FALSE 
	}
	if(0!=lstrlen(szDirveName)) //返回指定字符串的字节长度
	{
		printf ("\nDrive Name is %s\n",szDirveName); //显示动器名 
	}
    
    //显示存储驱动器序列号 
	printf ("\nVolume Serial Number is %u",dwVolumeSerialNumber); 
	
	//显示文件系统所支持的文件组成部分的最大值
	printf ("\nMaximum Component Length is %u",dwMaximumComponentLength);
	
	//显示文件类型 
	printf ("\nSystem Type is %s\n",szFileSystemNameBuffer);

	if(dwFileSystemFlags & FILE_SUPPORTS_REPARSE_POINTS)
	{
		/************************************************************
		 * 文件系统支不支持挂载 
		 * 挂载文件到一个虚拟盘或一个虚拟文件夹中，
		 *通过访问这个虚拟盘或文件夹使用整个文件。
	     ***********************************************************/ 
		printf ("The file system does not support volume mount points.\n");
	}
	if(dwFileSystemFlags & FILE_VOLUME_QUOTAS)
	{
		//文件系统支持磁盘分配 
		printf ("The file system supports disk quotas.\n");
	}
	if(dwFileSystemFlags & FILE_CASE_SENSITIVE_SEARCH)
	{
		//文件系统支持区分大小写的文件名 
		printf ("The file system supports case-sensitive file names.\n");
	}
	//you can use these value to get more informaion
	//
	//FILE_CASE_PRESERVED_NAMES   区分可保护的文件名 
	//FILE_CASE_SENSITIVE_SEARCH  文件大小写区分查找 
	//FILE_FILE_COMPRESSION       文件压缩 
	//FILE_NAMED_STREAMS          定义流 
	//FILE_PERSISTENT_ACLS        文件系统维护和增强访问控制表(只在NTFS下)
	//FILE_READ_ONLY_VOLUME       只读卷 
	//FILE_SUPPORTS_ENCRYPTION    支持文件加密 
	//FILE_SUPPORTS_OBJECT_IDS    支持对象目标检测 
	//FILE_SUPPORTS_REPARSE_POINTS 支持重定向 
	//FILE_SUPPORTS_SPARSE_FILES  支持稀疏文件 
	//FILE_UNICODE_ON_DISK        磁盘上的文件字符集 
	//FILE_VOLUME_IS_COMPRESSED   卷是压缩的 
	//FILE_VOLUME_QUOTAS          卷分配 
	printf("...\n");
	return TRUE;
}
