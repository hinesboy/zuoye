# 运行结果
***
### display.c
![输入图片说明](https://gitee.com/uploads/images/2017/1130/214100_018b5d6b_1577681.png "display.png")
### osinfo.c
![输入图片说明](https://gitee.com/uploads/images/2017/1130/214111_71aec6f1_1577681.png "osinfo.png")
### register.c
![输入图片说明](https://gitee.com/uploads/images/2017/1130/214118_20c45681_1577681.png "reg.png")
### time.c 
![输入图片说明](https://gitee.com/uploads/images/2017/1130/214126_469e4500_1577681.png "time.png")


# 心得体会
***
* 代码注释对于一个团队合作的项目来说还是很重要的，能够让团队里的每个成员很清晰地读懂代码，从而提高合作效率。
* git仓库是代码管理，团队合作的重要工具，刚开始学起来比较陌生，不熟练，各个指令还需要进一步掌握。在指导文档的引导下一步一步做就相对比较容易。
* 码云平台和git搭配可提高工作效率。