/* ************************************
*《精通Windows API》 
* 示例代码
* fb.c
* 12.1.4  浏览文件夹对话框
**************************************/
/* 头文件　*/
#include <Windows.h>
#include <shlobj.h>
/* 函数申明　*/
DWORD Browse(HWND hwnd) ;

/*************************************
* WinMain
* 功能	程序入口点，调用Browse
*
* 参数	未使用
**************************************/
int WINAPI WinMain(
				   HINSTANCE hInstance,//应用程序当前实例的句柄 
				   HINSTANCE hPrevInstance, 
				   LPSTR lpCmdLine,//LPSTR是一种字符串数据类型，被定义成是一个指向以NULL('\0')结尾的32为ANSI字符数组指针 
				   int nCmdShow//指明窗口如何显示 
				   )
{
	Browse(NULL); 
}

/*************************************
* WinMain
* 功能	弹出“浏览文件夹”对话框，
并获取用户选择的文件夹目录
*
* 参数	HWND hwnd	父窗口句柄
**************************************/
DWORD Browse(HWND hwnd) 
{
	// 用于保存路径
	CHAR szRoot[MAX_PATH];
	CHAR szChoose[MAX_PATH];
	CHAR szDisplayName[MAX_PATH];
	// 相关变量
	LPITEMIDLIST pidlRoot = NULL;
	LPITEMIDLIST pidlSelected = NULL;
	BROWSEINFO bi = {0};
	LPMALLOC pMalloc = NULL;

	// “浏览文件夹”的根路径，开发人员可根据情况选择，比如只浏览“我的文档”。
	SHGetFolderLocation(NULL, CSIDL_DESKTOP, NULL, 0, &pidlRoot);
	//从 PIDL 转换为路径
	SHGetPathFromIDList(pidlRoot,szRoot);
	// 填充 BROWSEINFO 结构
	bi.hwndOwner = hwnd;//浏览文件对话框的父窗体句柄 
	bi.pidlRoot = pidlRoot;//初始化制定的root目录 
	bi.pszDisplayName = szDisplayName;//此参数如果为null则不能显示对话框 
	bi.lpszTitle = "Choose a target";//在窗口内显示提示用户的语句 
	bi.ulFlags = 0;//设置弹出的对话框属性。此处意思为包含文件。如果不设这个值，默认的是只有文件夹 
	bi.lpfn = NULL; //应用程序定义的浏览对话框回调函数的地址。当对话框中的事件发生时，该对话框将调用回调函数 
	bi.lParam = 0;//对话框传递给回调函数的一个参数指针 
	// 弹出对话框
	pidlSelected = SHBrowseForFolder(&bi);
	// DisplayName
	MessageBox(NULL,szDisplayName,"Display Name:",MB_OK);
	// 选择的文件夹
	SHGetPathFromIDList( pidlSelected, szChoose );
	MessageBox(NULL,szChoose,"Choose:",MB_OK);
	// 释放
	ILFree(pidlRoot);
	return 0;
}
