/* ************************************
*《精通Windows API》 
* 示例代码
* mlsrv.c
* 11.1 通过mailslot进程间通信
**************************************/
/* 头文件 */
#include <windows.h>
#include <stdio.h>
/* 全局变量 */
HANDLE hSlot; //无类型的指针
LPTSTR lpszSlotName = TEXT("\\\\.\\mailslot\\sample_mailslot"); //LPTSTR就是指向char类型的指针，TEXT里面的内容为邮槽名
LPTSTR Message = TEXT("Message for mailslot in primary domain."); //通信的内容

/* ************************************
* void main()
* 功能	进程间mailslot通信服务器端
**************************************/
void main()
{ 
	DWORD cbMessage, cMessage, cbRead; //无符号的长整型
	BOOL fResult; //int类型
	LPTSTR lpszBuffer; //LPTSTR就是指向char类型的指针
	TCHAR achID[80]; //char类型
	DWORD cAllMessages; //无符号的长整型
	HANDLE hEvent; //无类型的指针
	OVERLAPPED ov; //定义了一个结构体

	cbMessage = cMessage = cbRead = 0; 

	hSlot = CreateMailslot( //创建一个邮路，返回一个句柄
		lpszSlotName,		// mailslot 名
		0,							// 不限制消息大小 
		MAILSLOT_WAIT_FOREVER,         // 无超时 
		(LPSECURITY_ATTRIBUTES) NULL); 

	if (hSlot == INVALID_HANDLE_VALUE) 
	{ 
		printf("CreateMailslot failed with %d\n", GetLastError());
		return ; 
	} 
	else printf("Mailslot created successfully.\n"); 

	while(1)
	{
		// 获取mailslot信息，返回值非0表示成功
		fResult = GetMailslotInfo(hSlot, // mailslot 句柄 
			(LPDWORD) NULL,               // 无最大消息限制
			&cbMessage,                   // 下一条消息的大小
			&cMessage,                    // 消息的数量
			(LPDWORD) NULL);              // 无时限

		if (!fResult) 
		{ 
			printf("GetMailslotInfo failed with %d.\n", GetLastError()); 
			return ; 
		} 

		if (cbMessage == MAILSLOT_NO_MESSAGE) 
		{ 
			// 没有消息，过一段时间再去读
			Sleep(20000);
			continue;
		} 

		cAllMessages = cMessage; //记录消息的总条数

		while (cMessage != 0)  // 获取全部消息，有可能不只一条
		{ 
			// 提示信息 函数wsprintf()将一系列的字符和数值输入到缓冲区
			wsprintf((LPTSTR) achID, //输出缓冲区，最大为1024字节
				"\nMessage #%d of %d\n", 
				cAllMessages - cMessage + 1, 
				cAllMessages); 

			// 分配空间 
			/*
				HeapAlloc是一个Windows API函数。它用来在指定的堆上分配内存，并且分配后的内存不可移动。
					参数1：HANDLE hHeap,（要分配堆的句柄，可以通过HeapCreate()函数或GetProcessHeap()函数获得。）
					参数2：DWORD dwFlags,
					参数3：SIZE_T dwBytes,（要分配堆的字节数。）
				返回值：
					如果成功分配内存，返回值为一个指向所分配内存块的首地址的（void*）指针。
					如果分配内存失败，并且没有指定HEAP_GENERATE_EXCEPTIONS，则返回NULL。
					如果指定了HEAP_GENERATE_EXCEPTIONS，则抛出异常，而不返回NULL：
			*/
			lpszBuffer = (LPTSTR) HeapAlloc(GetProcessHeap(),
				HEAP_ZERO_MEMORY,
				lstrlen((LPTSTR) achID)*sizeof(TCHAR) + cbMessage); 
			if( NULL == lpszBuffer )
			{
				return ;
			}
			// 读取消息
			fResult = ReadFile(hSlot,	// mailslot句柄
				lpszBuffer,			// 缓存
				cbMessage,			// 消息的长度
				&cbRead,			// 实际读取的长度
				NULL); 

			if (!fResult) 
			{ 
				printf("ReadFile failed with %d.\n", GetLastError()); 
				GlobalFree((HGLOBAL) lpszBuffer); 
				return ; 
			} 

			// 处理信息，显示
			/*
				lstrcat是函数功能：该函数将一个字符串附加在另一个字符串后面。
				参数：
					lpString1：一个以NULL为终止符字符串指针。这个缓冲区必须足够大能包含两个字符串。
					lpString2：一个以NULL为终止符字符串指针，它将追加在由lpString1中指定。这个缓冲区必须足够大能包含两个字符串。
				返回值：
					若函数运行成功，返回值指向缓冲区；若失败，则返回值为NULL。
			*/
			lstrcat(lpszBuffer, (LPTSTR) achID); 
			printf("Contents of the mailslot: %s\n", lpszBuffer); 

			/*
				HeapFree是一个Windows API函数。它用来释放堆内存
				参数：
					hHeap：堆内存块释放。这个参数是HeapCreate或GetProcessHeap函数返回的句柄
					dwFlags：指定几个可控释放的内存块
					lpMem：被释放的内存块的指针。这HeapAlloc或HeapReAlloc函数返回的指针。如果这个指针为NULL,则为空
			*/
			HeapFree(GetProcessHeap(),0,lpszBuffer); 

			// 计算剩余的消息数
			fResult = GetMailslotInfo(hSlot,  // mailslot 句柄 
				(LPDWORD) NULL,     // 无最大消息限制
				&cbMessage,         // 下一条消息的大小 
				&cMessage,          // 消息的数量    
				(LPDWORD) NULL);    //无时限      

			if (!fResult) 
			{ 
				printf("GetMailslotInfo failed (%d)\n", GetLastError());
				return ; 
			} 
		} 
	}
	return ; 
}