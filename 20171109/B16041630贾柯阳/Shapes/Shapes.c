/* ************************************
*《精通Windows API》
* 示例代码
* Shapes.c
* 13.6 绘制图形
**************************************/
/* 头文件 */
#include <Windows.h>
#include "resource.h"
/* 全局变量 */
HDC hdcCompat; // handle to DC for bitmap 
POINT pt;      // 鼠标位置
RECT rcTarget; // 矩形外沿 
RECT rcClient; // 客户区RECT
// 绘制的状态
BOOL fSizeEllipse; 
BOOL fDrawEllipse;  
BOOL fDrawRectangle; 
BOOL fSizeRectangle; 
BOOL fSizeRoundRect;
BOOL fDrawRoundRect; 
// 圆角椭圆宽和高，
int nEllipseWidth;   
int nEllipseHeight; 

BOOL CALLBACK WndProc(HWND hwnd, UINT uMsg, WPARAM wParam, 
					  LPARAM lParam) 
{ 
	HDC hdc;          // DC 句柄 
	PAINTSTRUCT ps;   
	POINT ptClientUL; // 客户区左上角
	POINT ptClientLR; // 客户区右下角

	switch (uMsg) 
	{ 
	case WM_COMMAND: 
		switch (wParam)	// 菜单输入，设置需要绘制的图形
		{ 
		case ID_SHAPE_ELLIPSE: 
			fSizeEllipse = TRUE; 
			break;  
		case ID_SHAPE_RECTANGLE: 
			fSizeRectangle = TRUE; 
			break;  
		case ID_SHAPE_ROUNDRECT: 
			fSizeRoundRect = TRUE; 
			break; 

		default: 
			return DefWindowProc(hwnd, uMsg, wParam, 
				lParam); 
		} 
		break;  

	case WM_CREATE: 
		// 初始化圆角的长和宽
		nEllipseWidth = 20; 
		nEllipseHeight = 20;  
		return 0; 

	case WM_PAINT:// 绘制
		BeginPaint(hwnd, &ps); //BeginPaint函数为指定窗口进行绘图工作的准备，并用将和绘图有关的信息填充到一个PAINTSTRUCT结构中
		// 选择画刷
		SelectObject(ps.hdc, GetStockObject(GRAY_BRUSH)); //把一个对象(位图、画笔、画刷等)选入指定的设备描述表。新的对象代替同一类型的老对象。

		if (fDrawEllipse) // 如果有椭圆
		{
			// 调用Ellipse
			Ellipse(ps.hdc, rcTarget.left, rcTarget.top, 
				rcTarget.right, rcTarget.bottom); //构造椭圆对象
			fDrawEllipse = FALSE; 
			rcTarget.left = rcTarget.right = 0; 
			rcTarget.top = rcTarget.bottom = 0; 
		} 

		if (fDrawRectangle) // 如果有矩形
		{
			// 调用Rectangle
			Rectangle(ps.hdc, rcTarget.left, rcTarget.top, 
				rcTarget.right, rcTarget.bottom); 
			fDrawRectangle = FALSE; 
			rcTarget.left = rcTarget.right = 0; 
			rcTarget.top = rcTarget.bottom = 0; 
		} 

		if (fDrawRoundRect) // 如果有圆角矩形
		{
			// 调用RoundRect
			RoundRect(ps.hdc, rcTarget.left, rcTarget.top, 
				rcTarget.right, rcTarget.bottom, 
				nEllipseWidth, nEllipseHeight); 
			fDrawRectangle = FALSE; 
			rcTarget.left = rcTarget.right = 0; 
			rcTarget.top = rcTarget.bottom = 0; 
		} 

		EndPaint(hwnd, &ps); //EndPaint函数标记指定窗口的绘画过程结束；这个函数在每次调用BeginPaint函数之后被请求，但仅仅在绘画完成以后。
		break; 

	case WM_SIZE: // 大小改变
		GetClientRect(hwnd, &rcClient); 
		ptClientUL.x = rcClient.left; 
		ptClientUL.y = rcClient.top; 
		ptClientLR.x = rcClient.right; 
		ptClientLR.y = rcClient.bottom; 
		ClientToScreen(hwnd, &ptClientUL); 
		ClientToScreen(hwnd, &ptClientLR);
		// 设置RECT的大小和位置
		SetRect(&rcClient, ptClientUL.x, ptClientUL.y, 
			ptClientLR.x, ptClientLR.y); 
		return 0; 

	case WM_LBUTTONDOWN: 

		// 使用鼠标定义一个矩形范围
		ClipCursor(&rcClient); 

		// 鼠标的位置
		pt.x = (LONG) LOWORD(lParam); 
		pt.y = (LONG) HIWORD(lParam); 

		if (fDrawEllipse) 
			fSizeEllipse = TRUE; 

		return 0; 

	case WM_MOUSEMOVE: 

		if ((wParam && MK_LBUTTON) 
			&& (fSizeEllipse || fSizeRectangle 
			|| fSizeRoundRect)) 
		{  

			hdc = GetDC(hwnd); 
			// 目标RECT是否空，
			if (!IsRectEmpty(&rcTarget)) 
			{
				Rectangle(hdc, rcTarget.left, rcTarget.top, 
					rcTarget.right, rcTarget.bottom); 
			}

			// 根据鼠标移动和情况，得到需要绘制的范围 
			// 根据鼠标移动和终点和起点的相对位置，起点和终点所代表的矩形的角不同
			if ((pt.x < (LONG) LOWORD(lParam)) && 
				(pt.y > (LONG) HIWORD(lParam))) 
			{
				SetRect(&rcTarget, pt.x, HIWORD(lParam), 
					LOWORD(lParam), pt.y); 
			} 

			else if ((pt.x > (LONG) LOWORD(lParam)) && 
				(pt.y > (LONG) HIWORD(lParam))) 
			{
				SetRect(&rcTarget, LOWORD(lParam), 
					HIWORD(lParam), pt.x, pt.y); 
			}

			else if ((pt.x > (LONG) LOWORD(lParam)) && 
				(pt.y < (LONG) HIWORD(lParam))) 
			{
				SetRect(&rcTarget, LOWORD(lParam), pt.y, 
					pt.x, HIWORD(lParam)); 
			}
			else 
			{
				SetRect(&rcTarget, pt.x, pt.y, LOWORD(lParam), 
					HIWORD(lParam)); 
			}
			// 在鼠标移动的过程中，绘制一个矩形
			Rectangle(hdc, rcTarget.left, rcTarget.top, 
				rcTarget.right, rcTarget.bottom); 
			DeleteDC( hdc); //该函数删除指定的设备上下文环境
		} 
		return 0; 

	case WM_LBUTTONUP: 

		// 设置 
		if (fSizeEllipse) 
		{ 
			fSizeEllipse = FALSE; 
			fDrawEllipse = TRUE; 
		} 
		if (fSizeRectangle) 
		{ 
			fSizeRectangle = FALSE; 
			fDrawRectangle = TRUE; 
		} 
		if (fSizeRoundRect) 
		{ 
			fSizeRoundRect = FALSE; 
			fDrawRoundRect = TRUE; 
		} 
		// 是否有绘制的图形
		if (fDrawEllipse || fDrawRectangle || fDrawRoundRect) 
		{ 
			InvalidateRect(hwnd, &rcTarget, TRUE); 
			UpdateWindow(hwnd); // WM_PAINT
		} 

		// 释放鼠标 
		ClipCursor((LPRECT) NULL); 
		return 0; 

	case WM_DESTROY: 
		DeleteDC(hdcCompat); 
		PostQuitMessage(0); 
		break; 

	default: 
		return DefWindowProc(hwnd, uMsg, wParam, lParam); //DefWindowProc函数调用缺省的窗口过程来为应用程序没有处理的任何窗口消息提供缺省的处理。该函数确保每一个消息得到处理。
	} 
	return (LRESULT) NULL; 
} 

int WINAPI WinMain(
				   HINSTANCE hInstance,
				   HINSTANCE hPrevInstance,
				   LPSTR lpCmdLine,
				   int nCmdShow)
{
	MSG msg;
	HWND hWnd;
	WNDCLASS wc;

	wc.style = CS_OWNDC; 
	wc.lpfnWndProc = (WNDPROC)WndProc; 
	wc.cbClsExtra = 0; 
	wc.cbWndExtra = 0; 
	wc.hInstance = hInstance;
	wc.hIcon = NULL; 
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszMenuName = MAKEINTRESOURCE(IDR_MENU_SHAPE);
	wc.lpszClassName = "shape"; 

	if (!RegisterClass(&wc))
		return (FALSE);

	hWnd = CreateWindow(
		"shape", 
		"shape", 
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, 
		NULL, 
		NULL,
		hInstance, 
		NULL 
		);

	if (!hWnd)
		return (FALSE);

	ShowWindow(hWnd, nCmdShow);//该函数设置指定窗口的显示状态 
	UpdateWindow(hWnd); //意思是更新指定窗口的客户区

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);//将虚拟键消息转换为字符消息
		DispatchMessage(&msg);//分发一个消息给窗口程序
	}
	return (int)(msg.wParam);
	UNREFERENCED_PARAMETER(lpCmdLine); 
}