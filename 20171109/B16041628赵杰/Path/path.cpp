#include <Windows.h>
#include <math.h>
#include "resource.h"

CHOOSEFONT cf;          // 通用对话框字体结构体
LOGFONT lf;             // 逻辑字体结构体
HFONT hfont;            // 新逻辑字体句柄
HFONT hfontOld;         // 原来的逻辑字体句柄
HDC hdc;                // 设备描述表句柄
int nXStart, nYStart;   // 画坐标
RECT rc;                // 画窗口的结构体
SIZE sz;                // 接收文本区段的结构体
double aflSin[90];      // 0-90度sin值
double aflCos[90];      // 0-90度cos值
double flRadius,a;      // 圆的半径
int iMode;              // 剪裁方式
HRGN hrgn;              // 剪裁区域的句柄 
int i;
COLORREF  crOld;

LRESULT APIENTRY MainWndProc( 
							 HWND hwnd,                // 窗口句柄
							 UINT message,             // 消息值，由Windows.h头文件中的宏定义来标识
							 WPARAM wParam,            // 包含有关消息的附加信息,不同消息其值有所不同
							 LPARAM lParam)            // 包含有关消息的附加信息,不同消息其值有所不同

{ 

	PAINTSTRUCT ps; 

	switch (message) 
	{ 
	case WM_PAINT: 
		hdc = BeginPaint(hwnd, &ps); 
		EndPaint(hwnd, &ps); 
		break; 

	case WM_COMMAND:     // 通过参数，可以区分这个消息的来源是来自于控件，快捷键还是菜单。
		switch (wParam) 
		{ 
		case IDM_VANISH:  // 擦除客户区域
			hdc = GetDC(hwnd); 
			GetClientRect(hwnd, &rc); 
			FillRect(hdc, &rc, (HBRUSH)GetStockObject(WHITE_BRUSH)); 
			ReleaseDC(hwnd, hdc); 
			break; 

		case IDM_AND: // 将剪辑模式设置为 RGN_AND （两个源区域的公共部分）
			iMode = RGN_AND; 
			break; 

		case IDM_COPY: // 将剪辑模式设置为 RGN_COPY （1的全部，忽略2，复制）
			iMode = RGN_COPY; 
			break; 

		case IDM_DIFF: // 将剪辑模式设置为 RGN_DIFF （1不在2中的部分）
			iMode = RGN_DIFF; 
			break; 

		case IDM_OR: // 将剪辑模式设置为 RGN_OR （两个源区域的全部）
			iMode = RGN_OR; 
			break; 

		case IDM_XOR: // 将剪辑模式设置为 RGN_XOR  （两个源区域的全部，但除去公共部分）
			iMode = RGN_XOR; 
			break; 

		case IDM_CLIP_PATH: 
			{

				// 为窗口检索一个缓存的设备

				hdc = GetDC(hwnd); 

				// Use the font requested by the user in the 
				// Choose Font dialog box to create a logical
				// font, then select that font into the DC. 
				// 使用用户在选择字体对话框中请求的字体创建一个逻辑字体，然后在设备中选择该字体。
				hfont = CreateFontIndirect(cf.lpLogFont); 
				hfontOld = (HFONT)SelectObject(hdc, hfont); 


				POINT point[5]= {{0,200},{600,200},{100,600},{300,0},{500,600}};

				hrgn = CreatePolygonRgn(point, 5, WINDING);



				SelectClipRgn(hdc, hrgn); 


				LPSTR szOut = "Lorem ipsum dolor sit amet, consectetur \n"
					"adipisicing elit, sed do eiusmod tempor \n" 
					"incididunt ut labore et dolore magna \n" 
					"aliqua. Ut enim ad minim veniam, quis \n" 
					"nostrud exercitation ullamco laboris nisi \n" 
					"ut aliquip ex ea commodo consequat. Duis \n" 
					"aute irure dolor in reprehenderit in \n" 
					"voluptate velit esse cillum dolore eu \n" 
					"fugiat nulla pariatur. Excepteur sint \n" 
					"occaecat cupidatat non proident, sunt \n" 
					"in culpa qui officia deserunt mollit \n" 
					"anim id est laborum.\n"; 
				RECT rect;
				GetClientRect(hwnd,&rect);
				DrawText(hdc, szOut, lstrlen(szOut),&rect , DT_CENTER);
				BeginPath(hdc); 


				Ellipse(hdc,100,100,500,500);

				//TextOut(hdc, nXStart, nYStart, szOut, lstrlen(szOut)); 
				EndPath(hdc); 
				SelectClipPath(hdc, iMode); 

				// 计算0-90度sin值
				for (i = 0; i < 90; i++) 
				{ 
					aflSin[i] = sin( (((double)i) / 180.0) 
						* 3.14159); 
				} 

				// 计算0-90度cos值
				for (i = 0; i < 90; i++) 
				{ 
					aflCos[i] = cos( (((double)i) / 180.0) 
						* 3.14159); 
				} 

				// 设置半径的值

				flRadius = 1000; 

				// Draw the 90 rays extending from the 
				// radius to the edge of the circle. 
				// 画出从半径到圆周的90条射线。
				for (i = 0; i < 90; i++) 
				{ 
					MoveToEx(hdc, nXStart, nYStart, 
						(LPPOINT) NULL); 
					LineTo(hdc, nXStart + ((int) (flRadius 
						* aflCos[i])), 
						nYStart + ((int) (flRadius 
						* aflSin[i]))); 
				} 

				// Reselect the original font into the DC. 
				// 将原来的字体重新选择到设备中。
				SelectObject(hdc, hfontOld); 

				// Delete the user's font. 
				// 删除用户的字体
				DeleteObject(hfont); 

				// Release the DC. 
				// 释放设备
				ReleaseDC(hwnd, hdc); 

				UpdateWindow(hwnd);

				break; 
			}


		case IDM_FONT: 

			// Initialize necessary members. 
			// 初始化必要的成员
			cf.lStructSize = sizeof (CHOOSEFONT); 
			cf.hwndOwner = hwnd; 
			cf.lpLogFont = &lf; 
			cf.Flags = CF_SCREENFONTS | CF_EFFECTS; 
			cf.rgbColors = RGB(0, 255, 255); 
			cf.nFontType = SCREEN_FONTTYPE; 

			// Display the Font dialog box, allow the user 
			// to choose a font, and render text in the 
			// window with that selection. 
			// 显示字体对话框，允许用户选择字体，并在窗口中显示文本。
			if (ChooseFont(&cf)) 
			{ 
				hdc = GetDC(hwnd); 
				hfont = CreateFontIndirect(cf.lpLogFont); 
				hfontOld = (HFONT)SelectObject(hdc, hfont); 
				crOld = SetTextColor(hdc, cf.rgbColors); 
				//TextOut(hdc, nXStart, nYStart, 
				//    "Clip Path", 9); 
				SetTextColor(hdc, crOld); 
				SelectObject(hdc, hfontOld); 
				DeleteObject(hfont); 
				ReleaseDC(hwnd, hdc); 
			} 

			break; 

		default: 
			return DefWindowProc(hwnd, message, wParam, 
				lParam); 
		} 
		break; 

	case WM_DESTROY:    // 窗口正在被销毁
		PostQuitMessage(0); 
		break; 

	default: 
		return DefWindowProc(hwnd, message, wParam, lParam); 
	} 
	return 0; 
} 


int WINAPI WinMain(
				   HINSTANCE hInstance,
				   HINSTANCE hPrevInstance,
				   LPSTR lpCmdLine,
				   int nCmdShow)
{
	MSG msg;
	HWND hWnd;
	WNDCLASS wc;


	wc.style = CS_OWNDC; 
	wc.lpfnWndProc = (WNDPROC)MainWndProc; 
	wc.cbClsExtra = 0; 
	wc.cbWndExtra = 0; 
	wc.hInstance = hInstance;
	wc.hIcon = NULL; 
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszMenuName = MAKEINTRESOURCE(IDR_MENU_CLIP);
	wc.lpszClassName = "shape"; 

	if (!RegisterClass(&wc))
	{
		return (FALSE);
	}

	hWnd = CreateWindow(
		"shape", 
		"shape", 
		WS_OVERLAPPEDWINDOW,
		100, 100, 600, 650, 
		NULL, 
		NULL,
		hInstance, 
		NULL 
		);

	if (!hWnd)
	{
		return (FALSE);
	}

	ShowWindow(hWnd, nCmdShow); 
	UpdateWindow(hWnd); 

	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return (int)(msg.wParam);

	UNREFERENCED_PARAMETER(lpCmdLine); 
}