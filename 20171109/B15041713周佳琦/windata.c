/* ************************************
*《精通Windows API》
* 示例代码
* windata.c
* 2.1.1	常用的Windows数据类型
**************************************/

/* 头文件　*/
#include <windows.h>
#include <stdio.h>

/* ************************************
* 功能	Windows 数据类型演示
**************************************/
int WINAPI WinMain(
	HINSTANCE hInstance,//应用程序当前实例的句柄
	HINSTANCE hPrevInstance,//应用程序的先前实例的句柄
	LPSTR lpCmdLine,
	int nCmdShow//指明窗口如何显示
)//函数说明
 /*  WINAPI是对于Windows操作系统中可用的内核应用程序编程接口。在windef.h文件里的形式为 #define WINAPI  __stdcall （宏定义）。其中，__stdcall是函数调用约定，定义了函数参数入栈的顺序，按从右至左的顺序压参数入栈，由调用者把参数弹出栈以及产生函数修饰名的方法
 WinMain函数是所有Windows应用程序的入口，类似c语言中的main函数 其功能是完成一系列的定义和初始化，并产生消息循环.WinMain函数有三个基本部分组成：函数说明、初始化和消息循环
 WinMain函数实现以下功能：注册窗口类，建立窗口及执行其他必要的初始化工作；进入消息循环，根据从应用程序消息队列接受的消息，调用相应的处理过程；当消息循环检测到WM_QUIT消息时终止程序运行
 HINSTANCE:“句柄型”数据类型
 hPrevlnstance：对于同一个程序打开两次，出现两个窗口第一次打开的窗口就是先前实例的窗口。对于一个32位程序，该参数总为NULL。
 LPSTR是一种字符串数据类型，它被定义成是一个指向以NULL(‘\0’)结尾的32位ANSI字符数组指针。//根据环境配置，如果定义了UNICODE宏,则是LPCWSTR类型，否则是LPCSTR类型
 lpCmdLine：指向应用程序命令行的字符串的指针，不包括执行文件名。获得整个命令行，参看GetCommandLine。
 */
{
	//定义字符串
	LPSTR szString = "Windows data type, string.";

	//定义字符数组
	CHAR lpString[120];//要大于szString的长度

					   //定义DWORD类型的数据
	DWORD dwMax = 0xFFFFFFFF;
	DWORD dwOne = 0x1;

	//定义INT类型的数据
	INT iMax = 0xFFFFFFFF;//iMax实际的值是不是0xFFFFFFFF而是-1
	INT iOne = 0x1;

	//显示字符串
	MessageBox(NULL, szString, "LPSTR", MB_OK);

	//复制内存，将字符串复制到数组中（包括NULL结束符）
	CopyMemory(lpString, szString, lstrlen(szString) + 1);
	/*  CopyMemory函数是将一块内存的数据从一个位置复制到另一个位置。函数原型及参数如下:
	VOID CopyMemory(PVOID Destination, CONST VOID *Source, SIZE_T Length);
	Destination要复制内存块的目的地址。
	Source要复制内存块的源地址。
	Length指定要复制内存块的大小，单位为字节返回值该函数为VOID型，没有返回值。
	该程序中的CopyMemory(lpString, szString, lstrlen(szString) + 1); 表示将szString里的内容复制到lpString中去内存块的大小为lstrlen(szString) + 1
	*/
	//显示复制的字符串
	MessageBox(NULL, lpString, "CHAR[]", MB_OK);

	//比较DWORD并显示结果
	if (dwMax>dwOne)
	{
		MessageBox(NULL, "DWORD类型的数据 OxFFFFFFFF > 0x1", "DWORD", MB_OK);
	}
	//比较INT并显示结果
	if (iMax<iOne)
	{
		MessageBox(NULL, "INT类型的数据 OxFFFFFFFF < 0x1", "INT", MB_OK);
	}
	return 0;
}
/*  MessageBox函数指的是显示一个模态对话框。函数原型及参数如下：
MessageBox(HWND hWnd,LPCTSTR lpText,LPCTSTR lpCaption,UINT uType);
hWnd：此参数代表消息框拥有的窗口。如果为NULL，则消息框没有拥有窗口。
lpText：消息框的内容    lpCaption：消息框的标题。
uType：指定一个决定对话框的内容和行为的位标志集。此参数可以为下列标志组中标志的组合。指定下列标志中的一个来显示消息框中的按钮以及图标。
该程序中的MessageBox(NULL,szString,"LPSTR",MB_OK);是一个标题为LPSTR，内容为Windows data type, string. 和一个确定button的消息框且没有拥有窗口。

在这个程序中，使用了 4 种 Windows 数据类型，分别是 LPSTR、CHAR、DWORD 和 INT。
LPSTR 类型的数据是字符串，也就是字符指针，CHAR 是字符，DWORD 是 32 位的无符号
整数，INT 是 32 位有符号整数
这些 Windows 数据类型都是从标准 C 的数据类型经过类型重定义而来。
比如，INT的定义如typedef int INT，DWORD的定义如：typedef unsigned long DWORD，CHAR的定义如：typedef char CHAR

程序运行后会弹出 4 个对话框。 这说明dwMax>dwOne是成立的。iMax<iOne也是成立的。dwMax 与 iMax 的数值是一样的，dwOne 与 iOne 的数值也是一样的。但是比较结果不同，是因为二者的数据类型不一样。
*/
